#! /usr/bin/env bash

#-------------------------------------------------------------------------#
#                                  lb.sh                                  #
#                                                                         #
# Author: Nic H.                                                          #
# Date: 2015-May-28                                                       #
#-------------------------------------------------------------------------#

if [ "x$JAVA_HOME" == "x" ]; then
    export JAVA_HOME=$(readlink -f $(dirname $(readlink -f `which java`))/..)
fi

set -u
set -e

BB="$LOGICBLOX_HOME/bin/bloxbatch"
DB="TMP_DB"
IMP="tmp_import.import"
LB_IN="$1"
LB_LOGIC="$2"
LB_TIMEFILE="$3"
LB_MEMFILE="$4"
#export LB_PAGER_FORCE_START=1
#export LB_MEM_NOWARN=1

[ -f $MEMUSG ] || (echo "\$MEMUSG must point to the memusg script"; exit 1)

typeCounter(){
    python -c "import os
def account(fil, a, b, c, has):
    for line in fil.readlines():
        spl = line[1:-2].split('\",\"')
        a.add(spl[0])
        b.add(spl[1])
        if has:
            c.add(spl[2])
    fil.close()

var = set()
fie = set()
obj = set()
account(open(\"$1/Alloc.csv\", \"r\"), var, obj, None, False)
account(open(\"$1/Assign.csv\", \"r\"), var, var, None, False)
account(open(\"$1/Load.csv\", \"r\"), var, var, fie, True)
account(open(\"$1/Store.csv\", \"r\"), var, var, fie, True)
if os.path.exists(\"$1/Cast.csv\"):
    account(open(\"$1/Cast.csv\", \"r\"), var, var, None, False)

print \"lang:physical:capacity[\`VarRef] = %d.\" % ((len(var)*11)//10)
print \"lang:physical:capacity[\`FieldSignatureRef] = %d.\" % ((len(fie)*11)//10)
print \"lang:physical:capacity[\`HeapAllocationRef] = %d.\" % ((len(obj)*11)//10)
"
}

autoImport(){
    echo -n "fromFile,\"`readlink -f $1`\"" >> $5
    for i in $(seq 1 $3); do
        echo -n ",column:$i,$2:$i" >> $5
    done
    echo >> $5
    echo -n "toPredicate,$2" >> $5
    for i in $(seq 1 $4); do
        echo -n ",$2:$i" >> $5
    done
    echo >> $5
}

setupImport(){
    echo 'option,delimiter,","' > $IMP
    echo 'option,hasColumnNames,false' >> $IMP
    autoImport $1/Alloc.csv Alloc 2 2 $IMP
    autoImport $1/Assign.csv Assign 2 2 $IMP
    autoImport $1/Load.csv Load 3 3 $IMP
    autoImport $1/Store.csv Store 3 3 $IMP
    if [ -f "$1/Cast.csv" ]; then 
        autoImport $1/Cast.csv Cast 3 2 $IMP
    fi
}

echo "Setup $LB_LOGIC on $LB_IN"
setupImport $LB_IN
typeCounter $LB_IN | cat - types.logic > typesall.logic
echo "Creating DB"
$BB -db $DB -create -overwrite -blocks base 2>&1 # dump any initial errors to stdout fore easy reading, its a long story
$BB -db $DB -addBlock -file typesall.logic
$BB -db $DB -import $IMP
echo "Running"
$MEMUSG $LB_MEMFILE /usr/bin/time -f "%e" -o $LB_TIMEFILE $BB -db $DB -addBlock -file $LB_LOGIC
#$BB -db $DB -popCount Alloc,Assign,Cast,Load,Store,VarPointsTo
$BB -db $DB -popCount VarPointsTo
while [ ! -e $LB_MEMFILE ]; do
    sleep 1
done
python -c "print \"%dms %dMB\" % (1000.0 * `cat $LB_TIMEFILE`, `cat $LB_MEMFILE` / 1024.0)"
rm $IMP typesall.logic
killall BloxPagerDaemon

