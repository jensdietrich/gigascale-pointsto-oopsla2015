package au.edu.sydney.cflr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.RefinementListener;
import nz.ac.massey.gp2.pointsto.mock.NullComponent;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

import org.apache.log4j.Logger;

/**
 * An implementation of the worklist algorithm from Melski / Reps: Interconvertibility of a class of set constraints and context-free-language reachability. 
 * Theoretical Computer Science 248(1), (2000).
 * @author Nic Hollingum
 */
public class WorklistPointsTo extends AbstractPointsToReachability {

	private static Logger LOGGER = LogSystem.getLogger(WorklistPointsTo.class);

	// VPT -> -Alloc         <-- Seems that Jens reverses this according to the read order...how strange
	// VPT -> -Assign VPT
	// LPT_f -> -Load_f VPT
	// SPT_f -> Store_f VPT
	// Bridge -> LPT_f -SPT_f
	// VPT -> Bridge VPT

	public static final int VPT = 0;
	public static final int ALLOC = 1;
	public static final int ASSIGN = 2;
	public static final int BRIDGE = 3;
	public static final int NT_GREATEST_INDEX = BRIDGE;

	public static final int F_STORE = 0;
	public static final int F_LOAD = 1;
	public static final int F_VPSTORE = 2;
	public static final int F_VPLOAD = 3;

	private List<Map<P2Vertex, Set<P2Vertex>>> forwardsSet;
	private List<Map<P2Vertex, Set<P2Vertex>>> backwardsSet;
	private Map<String, Integer> fields;
	
	// Jens: added reset to support parameterised testing
	@Override
	public void reset() {
		if (forwardsSet!=null) forwardsSet.clear();
		if (backwardsSet!=null) backwardsSet.clear();
		if (fields!=null) fields.clear();
	}

	@Override
	protected void processInternal(P2Graph g, RefinementListener... listeners) {

		//initialise the representation
		long t1 = System.nanoTime();
		Component dummyComponent = null;
		forwardsSet = new ArrayList<>();
		backwardsSet = new ArrayList<>();
		fields = new HashMap<String, Integer>();
		for(P2Vertex v : g.getVertices()){

			//This is a hack because Jens uses component membership to prevent errors
			//Potentially, only endpoints of VPT edges should have a dummy component
			// Jens: replaced with dummy component
			if(dummyComponent == null) dummyComponent = NullComponent.SOLE_INSTANCE;
			v.setComponent(dummyComponent);

			while(forwardsSet.size() <= NT_GREATEST_INDEX){
				forwardsSet.add(new HashMap<P2Vertex, Set<P2Vertex>>());
				backwardsSet.add(new HashMap<P2Vertex, Set<P2Vertex>>());
			}
			for(P2Edge eOut : v.getOutEdges()){
				int id = -1;
				switch(eOut.getType()){
				case P2Edge.NEW :
					id = ALLOC;
					break;
				case P2Edge.ASSIGN :
					id = ASSIGN;
					break;
				case P2Edge.LOAD :
					id = getFieldLoad(((FieldAccessEdge)eOut).getField());
					break;
				case P2Edge.STORE :
					id = getFieldStore(((FieldAccessEdge)eOut).getField());
					break;
				default :
					throw new RuntimeException("Unrecognised input edge-type");
				}
				insertEdge(eOut.getStart(), eOut.getEnd(), id);
			}
		}
		long t2 = System.nanoTime();
		LOGGER.info("Read the graph into worklist format, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		long t3 = System.nanoTime();
		//Run the Points-To finder
		//it is safe to not add the input edges to the worklist, since VPT always depends on itself
		Stack<P2Vertex> newStarts = new Stack<>();
		Stack<P2Vertex> newEnds = new Stack<>();
		Stack<Integer> newIDs = new Stack<>();
		//initialise the VPT with reverse ALLOC
		for(P2Vertex v1 : forwardsSet.get(ALLOC).keySet()){
			for(P2Vertex v2 : forwardsSet.get(ALLOC).get(v1)){
				potentialEdge(v2, v1, VPT, newStarts, newEnds, newIDs);
			}
		}
		while(!newStarts.isEmpty()){
			P2Vertex start = newStarts.pop();
			P2Vertex end = newEnds.pop();
			int id = newIDs.pop();
			if(id == VPT){
				extendTo(forwardsSet.get(ASSIGN), start, end, VPT, newStarts, newEnds, newIDs);
				extendTo(backwardsSet.get(BRIDGE), start, end, VPT, newStarts, newEnds, newIDs);
				for(int f=0; f<fields.size(); f++){
					extendTo(forwardsSet.get(indexToField(f, F_LOAD)), start, end, indexToField(f, F_VPLOAD), newStarts, newEnds, newIDs);
					extendTo(backwardsSet.get(indexToField(f, F_STORE)), start, end, indexToField(f, F_VPSTORE), newStarts, newEnds, newIDs);
				}
			} else if (id == BRIDGE){
				extendFrom(start, end, forwardsSet.get(VPT), VPT, newStarts, newEnds, newIDs);
			} else if ((id - 1 - NT_GREATEST_INDEX)%4 == F_VPLOAD) {
				extendFrom(start, end, backwardsSet.get(id - F_VPLOAD + F_VPSTORE), BRIDGE, newStarts, newEnds, newIDs);
			} else {// if ((id - 1 - NT_GREATEST_INDEX)%4 == F_VPSTORE)   <---  This should always be true
				extendTo(backwardsSet.get(id - F_VPSTORE + F_VPLOAD), end, start, BRIDGE, newStarts, newEnds, newIDs);
			}
		}
		long t4 = System.nanoTime();
		LOGGER.info("Completed worklist iterations, this took " + MeasurementUtils.formatNanos(t3,t4) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		LOGGER.info("Computed points-to sets, this took " + MeasurementUtils.formatNanos(t1,t4) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
	}
	
	private void extendFrom(P2Vertex knownStart, P2Vertex midPoint, Map<P2Vertex, Set<P2Vertex>> partialSource, int createdEdge, Stack<P2Vertex> startDelta, Stack<P2Vertex> endDelta, Stack<Integer> idDelta){
		if(partialSource.containsKey(midPoint)) for(P2Vertex other : partialSource.get(midPoint)){
			potentialEdge(knownStart, other, createdEdge, startDelta, endDelta, idDelta);
		}
	}
	
	private void extendTo(Map<P2Vertex, Set<P2Vertex>> partialSource, P2Vertex midPoint, P2Vertex knownEnd, int createdEdge, Stack<P2Vertex> startDelta, Stack<P2Vertex> endDelta, Stack<Integer> idDelta){
		if(partialSource.containsKey(midPoint)) for(P2Vertex other : partialSource.get(midPoint)){
			potentialEdge(other, knownEnd, createdEdge, startDelta, endDelta, idDelta);
		}
	}

	private void potentialEdge(P2Vertex src, P2Vertex dst, int id, Stack<P2Vertex> startDelta, Stack<P2Vertex> endDelta, Stack<Integer> idDelta){
		if(!forwardsSet.get(id).containsKey(src) || !forwardsSet.get(id).get(src).contains(dst)){
			insertEdge(src, dst, id);
			startDelta.push(src);
			endDelta.push(dst);
			idDelta.push(id);
			//LOGGER.info("          EDGEMADE " + src + " - " + dst + " : " + id + "       " + startDelta + endDelta + idDelta);
		}
	}

	private void insertEdge(P2Vertex v1, P2Vertex v2, int labelID){
		if(!forwardsSet.get(labelID).containsKey(v1)) forwardsSet.get(labelID).put(v1, new HashSet<P2Vertex>());
		forwardsSet.get(labelID).get(v1).add(v2);
		if(!backwardsSet.get(labelID).containsKey(v2)) backwardsSet.get(labelID).put(v2, new HashSet<P2Vertex>());
		backwardsSet.get(labelID).get(v2).add(v1);
		//LOGGER.info("EDGE " + v1 + " - " + v2 + " : " + labelID);
	}
	
	private int indexToField(int fieldIdx, int fieldType){
		return NT_GREATEST_INDEX + 1 + fieldIdx*4 + fieldType;
	}

	private int getFieldStore(String field){
		if(!fields.containsKey(field)){
			fields.put(field, fields.size());
			for(int i=0; i<4; i++){
				forwardsSet.add(new HashMap<P2Vertex, Set<P2Vertex>>());
				backwardsSet.add(new HashMap<P2Vertex, Set<P2Vertex>>());
			}
		}
		return indexToField(fields.get(field), F_STORE);
	}

	private int getFieldLoad(String field){
		return getFieldStore(field) - F_STORE + F_LOAD;
	}

	@Override
	public Set<P2Vertex> getPointsToSet(P2Vertex v) {
		if(! forwardsSet.get(VPT).containsKey(v)) return new HashSet<P2Vertex>();
		return forwardsSet.get(VPT).get(v);
	}

	@Override
	public boolean pointsTo(P2Vertex var, P2Vertex heapObj) {
		return getPointsToSet(var).contains(heapObj);
	}

}
