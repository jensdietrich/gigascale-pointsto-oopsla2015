/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.util;

/**
 * Utility method related to measurements.
 * @author jens dietrich
 */
public class MeasurementUtils {
	
	private static final long MEGABYTE = 1024L * 1024L;
	
	public static String formatMem(long mem) {
		return "" + mem/MEGABYTE + "MB"; // careful: number formats might use , - this will interfere with CSV format !
	}
	
	public static String formatNanos(long t) {
		return "" + t/1000000 + "ms"; 
	}
	
	public static String formatNanos(long t1,long t2) {
		return "" + Math.abs(t1-t2)/1000000 + "ms"; 
	}
	
	public static long measureMem() throws InterruptedException {
		// this code is weak, the sleep might help the gc() to sprint into action, but there is no guarantee this will work 
		// this depends entirely on the JVM
//		System.gc(); 
//		Thread.sleep(500); 
		
		return (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory());
	}
	
	public static String measureAndFormatMem() {
		try {
			return formatMem(measureMem());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "<an exception occurred while measuring mem usage>";
	}
}
