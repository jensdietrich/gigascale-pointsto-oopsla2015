/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility to create matlab scripts creating box charts from result files.
 * 
 * @author jens dietrich
 */
public class BoxChartScriptCreator {
	
	public static final int COL_REACHABILITY_TIME = 16;
	public static final int COL_REACHABILITY_MEMORY = 17;
	public static final int COL_TEST_TIME = 18;
	
	public static void main(String[] args) throws Exception {
		
		
		createBoxChart("output/results.csv",COL_TEST_TIME) ;
	}

	public static void createBoxChart(String input, int column) throws Exception {

			File in = new File(input);
			BufferedReader r = new BufferedReader(new FileReader(in));
			
			
			String line = null;
			String key = null;
			boolean firstLine = true;
			List<String> labels = new ArrayList<>();
			int counter = -1;
			
			while ((line=r.readLine())!=null) {
				if (firstLine) firstLine=false;
				else { // skip first line with labels
					String[] tokens = line.split(",");
					String key2 = tokens[0];
					Integer value = getValue(tokens[column]);
					
					if (key2.equals(key)) {
						System.out.print(" ");
						System.out.print(value);
					}
					else {
						if (key!=null) System.out.println("];");
						key = key2;
						labels.add(key);
						System.out.print("dataset"+(++counter));
						System.out.print("=[");
						System.out.print(value);
					}
				}
			}
			System.out.println("];");
			r.close();
			
			// matrix 
			System.out.print("dataset=[");
			for (int i=0;i<=counter;i++) {
				if (i>0) System.out.print(";");
				System.out.print("dataset");
				System.out.print(i);
			}
			
			System.out.println("]");
			System.out.println("datasetx=transpose(dataset);");
			
			// boxplot command
			System.out.print("boxplot(datasetx,'labels',{");
			for (int i=0;i<labels.size();i++) {
				String next = labels.get(i);
				if (i>0) System.out.print(',');
				System.out.print('\'');
				System.out.print(normaliseLabel(next));
				System.out.print('\'');
			}
			System.out.println("});");
	}

	private static String normaliseLabel(String next) {
		return next.substring(next.lastIndexOf(".")+1);
	}

	private static Integer getValue(String v) {
		// remove units (ms, mb)
		StringBuffer b = new StringBuffer();
		for (char c:v.toCharArray()) {
			if (Character.isDigit(c)) {
				b.append(c);
			}
		}
		return Integer.parseInt(b.toString());
	}

}
