/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.util;

/**
 * Several reusable IO related utilities.
 * @author jens dietrich
 */
public class IOUtils {
	/**
	 * Fast-parse a 2 column csv line (avoid String.split as it has to compile a regex), tokens are in double quotes.
	 * @param line
	 * @param checkForComments if true, check whether lines start with # and if so, ignore them
	 * @return
	 */
	public static String[] parse2ColCSVLine(String line,boolean checkForComments) {
		if (checkForComments && line.startsWith("#")) return null;
		/*int dq1 = line.indexOf('\"');
		
		int dq2 = line.indexOf('\"',dq1+1);
		int comma = line.indexOf(",",dq2+1);
		int dq3 = line.indexOf('\"',comma+1);
		int dq4 = line.indexOf('\"',dq3+1);*/
		
		int mid = line.indexOf("\",\"");//this works for now :P
		int nxt = line.indexOf("\",\"", mid + 3);
		if(nxt == -1){
			return new String[] {line.substring(1, mid),line.substring(mid+3, line.length()-1)};
		} else {
			return new String[] {line.substring(1, mid),line.substring(mid+3, nxt)};
		}
	}
	
	/**
	 * Fast-parse a 3 column csv line (avoid String.split as it has to compile a regex), tokens are in double quotes.
	 * @param line
	 * @param checkForComments if true, check whether lines start with # and if so, ignore them
	 * @return
	 */
	public static String[] parse3ColCSVLine(String line,boolean checkForComments) {
		if (checkForComments && line.startsWith("#")) return null;
		/*int dq1 = line.indexOf('\"');
		int dq2 = line.indexOf('\"',dq1+1);
		
		int comma = line.indexOf(",",dq2+1);
		int dq3 = line.indexOf('\"',comma+1);
		int dq4 = line.indexOf('\"',dq3+1);	
		
		comma = line.indexOf(",",dq4+1);
		int dq5 = line.indexOf('\"',comma+1);
		int dq6 = line.indexOf('\"',dq5+1);	*/
		
		int mid = line.indexOf("\",\"");//this works for now :P
		int mid2 = line.indexOf("\",\"", mid+3);
		return new String[] {line.substring(1, mid),line.substring(mid+3, mid2), line.substring(mid2+3, line.length()-1)};
	}
}
