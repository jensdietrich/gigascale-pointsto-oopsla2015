/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.util;

import java.io.File;
import java.io.FilenameFilter;

public class Config {
	public static final String DATA_FOLDER = "data/relations";
	public static final String TEST_DATA_FOLDER = "data/testdata1";
	public static final String MAPPING_FOLDER = "data/relations-mapping";
	public static final String MAPPING = MAPPING_FOLDER+"/mapping.csv";
	
	public static File TEST_ALLOC_TABLE = new File(TEST_DATA_FOLDER+"/Alloc.csv"); 
	public static File TEST_ASSIGN_TABLE = new File(TEST_DATA_FOLDER+"/Assign.csv");
	
	public static File ALLOC_TABLE = new File(DATA_FOLDER+"/Alloc.csv"); 
	public static File ASSIGN_TABLE = new File(DATA_FOLDER+"/Assign.csv");
	public static File LOAD_TABLE = new File(DATA_FOLDER+"/Load.csv");
	public static File STORE_TABLE = new File(DATA_FOLDER+"/Store.csv");
	
	public static final FilenameFilter CSV_FILTER = new FilenameFilter() {
		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".csv");
		}
	};
	

}
