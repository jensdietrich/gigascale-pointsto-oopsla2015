/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */


package nz.ac.massey.gp2.diffprop;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.RefinementListener;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

/**
 * Implementation of the algorithm presented in 
 * Sridharan, Fink - The Complexity of Andersens Analysis in Practice. 
 * doi:10.1007/978-3-642-03237-0_15.
 * The implementation is a faithful implementation of the algorithm, whenever possible, we use the same 
 * identifiers for variables. 
 * <p>
 * The vertices and edges used are represented by separate (minimalistic) Vertex and Edge classes.
 * There is no explicit graph class, vertices now in and out edges, and edges know their source and sink (target). 
 * In order to map this to our framework, we have to map these types to the respective vertex and edge types used 
 * by the framework. For this purpose, and adapter bimap between P2Vertex and Vertex is generated. We do not expect that this 
 * has a major influence on performance, as at most |V| add operations are performed. Adapter lookup only happens at query time.
 * The implementation uses hash multi maps from the (google) guava library. This is a convenient and fast replacement for Map&lt;K,Set&lt;V&gt;&gt;
 * data structures.
 * The main algorithm is implemented in doAnalysis, and diffProp is an implementation of the procedure used by the algorithm.
 * There are comments referring to the respective lines in the figure from the paper in the code. 
 * <p>
 * NOTE THAT THIS ALGORITHM MIGHT PRODUCE FALSE NEGATIVES (MISSES POINTS_TO RECORDS).
 * For a comprehensive analysis, see issue 3 and the associated  {@link test.nz.ac.massey.gp2.pointsto.issues.TestIssue3 test case}.
 * @author jens dietrich
 */
public final class DiffPropPointsTo extends AbstractPointsToReachability {

	private static final Logger LOGGER = LogSystem.getLogger(DiffPropPointsTo.class);
	private static final boolean DEBUG = LOGGER.isDebugEnabled();
	
	@Override
	protected void processInternal(P2Graph g, RefinementListener... listeners) {
		doAnalysis(g);
	}

	private HashMultimap<Vertex,Vertex> pts = HashMultimap.create();
	private HashBiMap<P2Vertex,Vertex> adapter = HashBiMap.create();
	
	private void doAnalysis(P2Graph g) {
		
		LOGGER.info("Generating points-to index");
		long t1 = System.nanoTime();
		
		List<Vertex> workList = new LinkedList<>();
		HashMultimap<Vertex,Vertex> pt_deltas = HashMultimap.create();
		
		Multimap<String,FieldAccessEdge> storesByTarget = HashMultimap.create();
		Multimap<String,FieldAccessEdge> loadsBySource = HashMultimap.create();
		Map<String,Vertex> verticesByName = new HashMap<>();
		
		for (P2Vertex v:g.getVertices()) {
			for (P2Edge e:v.getOutEdges()) {
				if (e.getType()==P2Edge.LOAD) {
					loadsBySource.put(e.getStart().getName(), (FieldAccessEdge)e);
				}
				else if (e.getType()==P2Edge.STORE) {
					storesByTarget.put(e.getEnd().getName(), (FieldAccessEdge)e);
				}
			}
		}
		
		// graph representation:
		// note that y and o 's have different types (P2Vertex.NAME and P2Vertex.HEAP_OBJECT, respectively)
		
		// lines at the beginning refer to lines in figure 1 of the Sridharan/Fink paper
		
		// 1 for each statement i: x = new T() do 
		// 2 pt_delta (x) <- pt _delta (x) union {o_i}, o_i fresh
		// 3    add x to worklist	
		for (P2Vertex v:g.getVertices()) {
			if (v.getType()==P2Vertex.HEAP_OBJECT) {
				Vertex o = this.getOrAddVertexByName(adapter,verticesByName,v);
				// iterate over allocs
				for (P2Edge e:v.getOutEdges()) {
					Vertex x = this.getOrAddVertexByName(adapter,verticesByName,e.getEnd());
					workList.add(x);
					if (DEBUG) LOGGER.debug("add vertex to worklist: " + x);
					Set<Vertex> pt_delta = pt_deltas.get(x);
					pt_delta.add(o);
				}
			}
		// 4 for each statement x = y do
		// 5 	 add edge y -> x to G
		// do this faster in same loop
			else if (v.getType()==P2Vertex.NAME) {
				for (P2Edge e:v.getOutEdges()) {
					if (e.getType()==P2Edge.ASSIGN) {
						Vertex src = this.getOrAddVertexByName(adapter,verticesByName,e.getStart());
						Vertex target = this.getOrAddVertexByName(adapter,verticesByName,e.getEnd());
						Edge e2 = new Edge(src,target);
						src.addOutEdge(e2);
						target.addInEdge(e2);
						
						if (DEBUG) LOGGER.debug("add edge to graph: " + e2);
					}
				}
			}
		}
		
		// 6 while worklist != emptyset do
		while (!workList.isEmpty()) {
			// 7 remove n from worklist
			Vertex n = workList.remove(0);
			if (DEBUG) LOGGER.debug("remove from worklist: " + n + " , new worklist is " + workList);
			
			// 8 foreach edge n -> n2 do 
			Collection<Edge> outEdges = n.getOutEdges();
			for (Edge e:outEdges) {
			// 9 DiffProp(pt_delta (n), n2 )
				diffProp(pt_deltas.get(n),e.getTarget(),workList,pt_deltas,pts);
			}
			// 10 if n represents a local x
			if (!n.hasField()) {
				Vertex x = n;
				
				// 11 then for each statement x.f = y do
				for (FieldAccessEdge store:storesByTarget.get(n.getName())) {
					Vertex y = this.getOrAddVertexByName(adapter,verticesByName,store.getStart());
					// 12 for each o in pt _delta(n) do
					for (Vertex o:pt_deltas.get(x)) {
						// 13 if y -> o.f notin G
						Vertex of = this.getOrAddVertexByName(verticesByName,o.getName(),store.getField());
						Edge e = new Edge(y,of);
						if (!y.getOutEdges().contains(e)) {
							// 14 then add edge y -> o.f to G
							y.addOutEdge(e);
							of.addInEdge(e);
							if (DEBUG) LOGGER.debug("add edge to graph: " + e);
							// 15 DiffProp(pt  (y), o.f )
							diffProp(pts.get(y),of,workList,pt_deltas,pts);
						}
					}
				}
				
				// 16 for each statement y = x.f do
				for (FieldAccessEdge load:loadsBySource.get(n.getName())) {
					Vertex y = this.getOrAddVertexByName(adapter,verticesByName,load.getEnd());
					
					adapter.put(load.getEnd(), y);
					
					// 17 for each o in pt _delta(n) do
					
					// note that pt_deltas can be modified within this loop (in diffProp)
					// this might lead to a ConcurrentModificationException
					// we therefore have to copy the set
					// we use an ArrayList (for its fast add operation)
					Set<Vertex> pt_delta = pt_deltas.get(x);
					Collection<Vertex> pt_delta2 = new ArrayList<>();
					pt_delta2.addAll(pt_delta);
					for (Vertex o:pt_delta2) {
						// 18 if o.f -> y notin G
						Vertex of = this.getOrAddVertexByName(verticesByName,o.getName(),load.getField());
						Edge e = new Edge(of,y);
						if (!y.getInEdges().contains(e)) {
							// 19 then add edge o.f -> y  to G
							of.getOutEdges().add(e);
							y.getInEdges().add(e);
							if (DEBUG) LOGGER.debug("add edge to graph: " + e);
							// 20 DiffProp(pt  (o.f), y )
							diffProp(pts.get(of),y,workList,pt_deltas,pts);
						}
					}
				}	
			}
			
			// 21 pt(n) <- pt (n) union pt_delta(n)
			pts.get(n).addAll(pt_deltas.get(n));
			// 22 pt_delta(n) <- emptyset
			pt_deltas.get(n).clear();
		}
		long t2 = System.nanoTime();
		LOGGER.info("Generated index, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
	}

	private Vertex getOrAddVertexByName(HashBiMap<P2Vertex,Vertex> adapter,Map<String,Vertex> verticesByName,P2Vertex v_orig) {
		String name = v_orig.getName();
		Vertex v = verticesByName.get(name);
		if (v==null) {
			v = new Vertex(name);
			verticesByName.put(name,v);
			adapter.put(v_orig,v);
		}
		return v;
	}
	private Vertex getOrAddVertexByName(Map<String,Vertex> verticesByName,String name,String field) {
		String qname = name + "######" + field;
		Vertex v = verticesByName.get(qname);
		if (v==null) {
			v = new Vertex(name,field);
			verticesByName.put(qname,v);
			// these vertices will not be added to adapter, as they have no counterpart in original graph
		}
		return v;
	}
	
	private static void diffProp(Set<Vertex> srcSet,Vertex n,Collection<Vertex> workList,HashMultimap<Vertex,Vertex> pt_deltas,HashMultimap<Vertex,Vertex> pts) {
		
		if (DEBUG) {
			LOGGER.debug("diff prop " + srcSet + " to "  + n);
		}
		
		Set<Vertex> pt_delta = pt_deltas.get(n);
		Set<Vertex> pt = pts.get(n);
		int s = pt_delta.size(); // to check whether it changes
		
		// 1 pt_delta(n) <- pt_delta(n) union (srcSet - pt(n))
		for (Vertex v:srcSet) {
			if (!pt.contains(v)) {
				pt_delta.add(v);
			}
		}
		
		// 2 if pt _delta(n) changed then add n to worklist
		if (s!=pt_delta.size()) {
			// check LinkedHashSet 
			if (!workList.contains(n)) workList.add(n);
			if (DEBUG) {
				LOGGER.debug("pt_delta of " + n + " is " + pt_delta);
				LOGGER.debug("re-add to worklist: " + n);
			}
		}
		
		
	}
	
	@Override
	public Set<P2Vertex> getPointsToSet(P2Vertex var) {
		// note that using the adapter is cheap (hashmap lookups), and only influences
		// the query times, and not the index generation time ! 
		Vertex key = adapter.get(var);
		Set<Vertex> set = pts.get(key);
		Set<P2Vertex> set2 = new HashSet<>();
		for (Vertex v:set) {
			set2.add(adapter.inverse().get(v));
		}
		return set2;
	}

	@Override
	public boolean pointsTo(P2Vertex var, P2Vertex heapObj) {
		// note that using the adapter is cheap (hashmap lookups), and only influences
		// the query times, and not the index generation time !
		Vertex var2 = adapter.get(var);
		Vertex heapObj2 = adapter.get(heapObj);
		return pts.get(var2).contains(heapObj2);
	}

	@Override
	public void reset() {
		this.adapter.clear();
		this.pts.clear();
	}

}
