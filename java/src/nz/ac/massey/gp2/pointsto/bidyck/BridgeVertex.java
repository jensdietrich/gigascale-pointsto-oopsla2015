/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.bidyck;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Vertex;

import com.google.common.collect.HashMultimap;

/**
 * Virtual vertices that represent pseudo names (variables). 
 * Whenever components are traversed and an outgoing store-f edge (to a vertex in another component) is encountered:
 * <p>
 * v -store-f -&gt; v' [in c']
 * <p>
 * it is replaced by a new BV 
 * <p>
 * v -assign-&gt; pn
 * <p>
 * all "returning" loads (returning from any vertex v'' in c' to any vertex v''' in c (the component of v)) 
 * are represented as edges pn -assign-&gt; v'''
 * In other words, BVs "proxy" other components. The proxied components are referenced using representedComponent property.
 * @author jens dietrich
 */
public class BridgeVertex extends P2Vertex {

	private static final long serialVersionUID = -5774874776174959359L;
	private static int ID = 0;
	
	private String field = null;
	
	// this is the component represented by this vertex - the pivot of the respective Dyck rule
	private Component representedComponent = null;
	
	// associates the targets of the replaced store edges with their sources (sources are in the component of this bridge vertex, targets are in the represented component)
	private HashMultimap<P2Vertex, P2Vertex> replacedStoreEdges = HashMultimap.<P2Vertex,P2Vertex>create(); 
	// associates the sources of the replaced load edges with their targets (targets are in the component of this bridge vertex, sources are in the represented component)
	private HashMultimap<P2Vertex, P2Vertex> replacedLoadEdges = HashMultimap.<P2Vertex,P2Vertex>create();

	public BridgeVertex() {
		super();
		this.setType(NAME);
		synchronized (this) {
			this.setName("bridge-" + ID++ );
		}
	}
	
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Component getRepresentedComponent() {
		if (this.representedComponent==null) {
			return null;
		}
		else {
			// resolve proxies whenever this method is accessed
			if (this.representedComponent.isRedundant()) {
				this.representedComponent = this.representedComponent.getDelegate();
			}
			return this.representedComponent;
		}
	}

	public void setRepresentedComponent(Component c) {
		this.representedComponent = c;
	}

	@Override
	public String toString() {
		return "BridgeVertex [field=" + field + ", repr.component=" + representedComponent
				+ ", name=" + getName() + "]";
	}
	
	
	private Set<FieldAccessEdge> replacedStores = new HashSet<>();
	private Set<FieldAccessEdge> replacedLoads = new HashSet<>();
	
	public void addReplacedStoreEdge(FieldAccessEdge edge) {
		assert edge.getType()==P2Edge.STORE;
		// skip component checks, when components are merged, this is done before the proxies are set
//		assert edge.getStart().getComponent() == this.getComponent();
//		assert edge.getEnd().getComponent() == this.getRepresentedComponent();
		this.replacedStoreEdges.put(edge.getEnd(),edge.getStart());
		boolean added = this.replacedStores.add(edge);
		assert added;
	}

	public void addReplacedLoadEdge(FieldAccessEdge edge) {
		assert edge.getType()==P2Edge.LOAD;
		// skip component checks, when components are merged, this is done before the proxies are set
//		assert edge.getEnd().getComponent() == this.getComponent();
//		assert edge.getStart().getComponent() == this.getRepresentedComponent();
		this.replacedLoadEdges.put(edge.getStart(),edge.getEnd());
		boolean added = this.replacedLoads.add(edge);
		assert added;
	}
	
	public Collection<FieldAccessEdge> getReplacedStoreEdges() {
		return this.replacedStores;
	}
	public Collection<FieldAccessEdge> getReplacedLoadEdges() {
		return this.replacedLoads;
	}
	public Set<P2Vertex> getTargetsOfReplacedStoreEdges() {
		return this.replacedStoreEdges.keySet();
	}
	public Set<P2Vertex> getSourcesOfReplacedStoreEdges(P2Vertex target) {
		return this.replacedStoreEdges.get(target);
	}
	public Set<P2Vertex> getSourcesOfReplacedLoadEdges() {
		return this.replacedLoadEdges.keySet();
	}
	public Set<P2Vertex> getTargetsOfReplacedLoadEdges(P2Vertex source) {
		return this.replacedLoadEdges.get(source);
	}
	
}
