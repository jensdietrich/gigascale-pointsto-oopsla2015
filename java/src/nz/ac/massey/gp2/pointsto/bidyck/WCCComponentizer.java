/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.bidyck;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import nz.ac.massey.gp2.pointsto.Builder;
import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

import org.apache.log4j.Logger;

/**
 * Use DFS to compute Points2Sets for all heapobject nodes. Field info (load/store edges) is ignored.
 * Note that we compute the transitive closure of the alias relationship on the field-insensitive part of the graph.
 * <p>
 * Note:
 * We create pseudo heap objects (PHO) for incoming load nodes. A PHO knows the field of the load, and has a "new" edge linking it 
 * to the end of the load edge. Create PHOs only after HO s have been visited to minimize imprecision, and if load end nodes have not yet been visited.
 * Record statistics. 
 * <p>
 * If components discovered overlap, they are merged. 
 * 
 * @author jens dietrich
 */
public class WCCComponentizer implements Builder {
	
	private static Logger LOGGER = LogSystem.getLogger(WCCComponentizer.class);
	private static boolean TRACE_MODE = LOGGER.isTraceEnabled();
	
	/**
	 * Virtual vertices that represent pseudo heap objects (PHO). 
	 * A PHO-new-&gt;v vertex/edge replaces and incoming v'-load_f-&gt;v edge. 
	 * The intention is that v can point to a heap object stored in v'.f .
	 * @author jens dietrich
	 */
	static class PseudoHeapObjectVertex extends P2Vertex {

		private static final long serialVersionUID = 3719790162962793396L;

		private String field = null;
		private P2Vertex source = null;
		private P2Vertex target = null;
		
		public PseudoHeapObjectVertex(P2Vertex source,P2Vertex target,String field) {
			super();
			this.setType(HEAP_OBJECT);
			this.source = source;
			this.target = target;
			this.field = field;
			this.setName(source.getName()+" -> " + target.getName() + " // field="+field);
		}

		public String getField() {
			return field;
		}
		
		public P2Vertex getSource() {
			return source;
		}

		public P2Vertex getTarget() {
			return target;
		}
		
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("PseudoHeapObjectVertex [name=");
			builder.append(getName());
			builder.append("]");
			return builder.toString();
		}
	}

	
	@Override
	public void process(P2Graph graph) {
				
		LOGGER.info("Collecting components");
		long t1 = System.nanoTime();
		List<P2Vertex> agenda = new LinkedList<>();
		for (Iterator<P2Vertex> iter=graph.getVertices().iterator();iter.hasNext();) {
			P2Vertex v = iter.next();
			if (v.getType()==P2Vertex.HEAP_OBJECT) {
				agenda.add(v);
			}
		}	
		
		// to be merged info is managed in a graph
		while (!agenda.isEmpty()) {
			P2Vertex v = agenda.remove(0);
			if (TRACE_MODE) LOGGER.trace("start new traversal at " + v.getName());
			
			// check whether v has already been assigned to a component, this is possible for a PHO
			if (v.getComponent()==null) {
			
				// use v as "context/root vertex" for a new component
				Component component = new BiDyckComponent(v);
		
				v.setComponent(component);
				if (v instanceof PseudoHeapObjectVertex) {
					P2Vertex next = ((PseudoHeapObjectVertex) v).getTarget(); 
					// only traverse unassigned targets
					if (next.getComponent()==null) {
						visit(next,component,agenda);
					}
				}
				else {
					for (P2Edge e:v.getOutEdges()) {
						assert e.getType()==P2Edge.NEW;
						visit(e.getEnd(),component,agenda);
					}	
				}
			}
		}
		
		long t2 = System.nanoTime();
		LOGGER.info("Collected components, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		LOGGER.info("Adding edges to components");
		t1 = System.nanoTime();
		
		for (P2Vertex v:graph.getVertices()) {
			for (P2Edge e:v.getOutEdges()) {
				if (e.getType()==P2Edge.LOAD) {
					P2Vertex start = e.getStart();
					P2Vertex end = e.getEnd();
					if (start.getComponent()!=null && end.getComponent()!=null) {
						start.getComponent().addOutgoingLoadEdge((FieldAccessEdge)e);
					}
				}
				else if (e.getType()==P2Edge.STORE) {
					P2Vertex start = e.getStart();
					P2Vertex end = e.getEnd();
					if (start.getComponent()!=null && end.getComponent()!=null) {
						end.getComponent().addIncomingStoreEdge((FieldAccessEdge)e);
					}
				}
			}
		}
		
		t2 = System.nanoTime();
		LOGGER.info("Added edges to components, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
	}

	private void visit(P2Vertex v,Component component,  List<P2Vertex> agenda) {

		assert !component.isRedundant();
		Component component2 = v.getComponent();
		if (component2!=null) {
			// already visited !
			if (TRACE_MODE) LOGGER.trace("encountering already visited vertex " + v.getName());
			if (component2!=component) {
				component.merge(component2);
			}
			return;
		}
		else {
			v.setComponent(component);
		}
		
		if (TRACE_MODE) LOGGER.trace("visiting vertex " + v.getName());
		for (P2Edge e:v.getOutEdges()) {
			if (e.getType()==P2Edge.ASSIGN) {
				P2Vertex next = e.getEnd();
				visit(next,component,agenda);
			}
			else if (e.getType()==P2Edge.LOAD) {
				FieldAccessEdge fa = (FieldAccessEdge)e;
				if (e.getEnd().getComponent()==null) {
					PseudoHeapObjectVertex pho = new PseudoHeapObjectVertex(e.getStart(),e.getEnd(),fa.getField());
					agenda.add(pho);
				}
			}
		}

	}
	
	
}
