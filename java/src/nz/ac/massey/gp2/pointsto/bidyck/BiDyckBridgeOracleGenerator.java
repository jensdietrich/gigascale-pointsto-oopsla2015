/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */


package nz.ac.massey.gp2.pointsto.bidyck;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;

import nz.ac.massey.gp2.pointsto.BridgeEdge;
import nz.ac.massey.gp2.pointsto.BridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

/**
 * Generates an bridge oracle by building equivalence classes of variable vertices connected by 
 * assign edges (weakly connected components), which are then further merged through "folding" when matching stores-loads are encountered. 
 * When equivalence classes are folded, store sources are connected to load sinks with asserted bridge (match) edges that behave like assigns.
 * This follows the idea described in Zhang et al: Fast Algorithms for Dyck-CFL-Reachability with Applications to Alias Analysis. PLDI'13.
 * However, there are are some important differences: in order to obtain an effective implementation, we create bridge vertices first which are later resolved
 * into bridge edges.
 * @author jens dietrich
 */
public class BiDyckBridgeOracleGenerator implements BridgeOracleGenerator {
	private static Logger LOGGER = LogSystem.getLogger(BiDyckBridgeOracleGenerator.class);
	
	@Override
	public void process(P2Graph graph) {

		LOGGER.info("Detecting initial components");
		long t1 = System.nanoTime();
		WCCComponentizer builder1 = new WCCComponentizer();
		builder1.process(graph);
		long t2 = System.nanoTime();
		LOGGER.info("Detected initial components, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		LOGGER.info("Compacting component graph by applying Dyck rules");
		t1 = System.nanoTime();
		BiDyckComponentMerger biDyckComponentMerger = new BridgeVertexBasedDyckComponentMerger();
		biDyckComponentMerger.process(graph);
		t2 = System.nanoTime();
		LOGGER.info("Compacted component graph by applying Dyck rules, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		LOGGER.debug("Merging bridge vertices");
		t1 = System.nanoTime();
		mergeBridgeVertices(graph);
		t2 = System.nanoTime();
		LOGGER.debug("Merging bridge vertices done, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		
		LOGGER.debug("Adjusting components");
		t1 = System.nanoTime();
		adjustComponents(graph);
		t2 = System.nanoTime();
		LOGGER.debug("Adjusting components done, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		
		LOGGER.debug("Replacing bridge vertices by bridge edges");
		t1 = System.nanoTime();
		resolveBridgeVertices(graph);
		t2 = System.nanoTime();
		LOGGER.debug("Replacing bridge vertices by bridge edges done, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
	}
	
	/**
	 * During analysis, multiple bridges can be generated that have the same component, the same pivot and the same field. 
	 * This happens mainly when multiple bridge vertices are created, and later the respective components are merged
	 * by applying other rules. Such a scenario is described in issue TODO. 
	 * @param graph
	 */
	private void mergeBridgeVertices(P2Graph graph) {
		class BridgeKey {
			Component component = null;
			Component pivot = null;
			String field = null;
			public BridgeKey(Component component, Component pivot, String field) {
				super();
				this.component = component;
				this.pivot = pivot;
				this.field = field;
			}
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result
						+ ((component == null) ? 0 : component.hashCode());
				result = prime * result
						+ ((field == null) ? 0 : field.hashCode());
				result = prime * result
						+ ((pivot == null) ? 0 : pivot.hashCode());
				return result;
			}
			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				BridgeKey other = (BridgeKey) obj;
				if (component == null) {
					if (other.component != null)
						return false;
				} else if (!component.equals(other.component))
					return false;
				if (field == null) {
					if (other.field != null)
						return false;
				} else if (!field.equals(other.field))
					return false;
				if (pivot == null) {
					if (other.pivot != null)
						return false;
				} else if (!pivot.equals(other.pivot))
					return false;
				return true;
			}
			
		};
		Map<BridgeKey,BridgeVertex> bridges = new HashMap<>();
		Collection<BridgeVertex> toBeRemoved = new ArrayList<>();
		for(BridgeVertex bridge:graph.getBridges()) {
			BridgeKey key = new BridgeKey(bridge.getComponent(),bridge.getRepresentedComponent(),bridge.getField());
			BridgeVertex otherBridge = bridges.put(key, bridge);
			if (otherBridge!=null) {
				// merge bridges, create tmp collections to avoid concurrent modification exceptions
				Collection<P2Edge> edges = new ArrayList<>();
				edges.addAll(bridge.getInEdges());
				for (P2Edge e:edges) {
					otherBridge.addInEdge(e);
					bridge.removeInEdge(e);
					e.setEnd(otherBridge);
				}
				edges.clear();
				edges.addAll(bridge.getOutEdges());
				for (P2Edge e:edges) {
					otherBridge.addOutEdge(e);
					bridge.removeOutEdge(e);
					e.setStart(otherBridge);
				}
				Collection<FieldAccessEdge> loadStores = new ArrayList<>();
				loadStores.addAll(bridge.getReplacedLoadEdges());
				for (FieldAccessEdge e:loadStores) {
					otherBridge.addReplacedLoadEdge(e);
				}
				loadStores.clear();
				loadStores.addAll(bridge.getReplacedStoreEdges());
				for (FieldAccessEdge e:loadStores) {
					otherBridge.addReplacedStoreEdge(e);
				}
				toBeRemoved.add(bridge);
			}
		}
		for (BridgeVertex bridge:toBeRemoved) {
			graph.remove(bridge);
		}
		LOGGER.info("bridge vertices merged: "+toBeRemoved.size());
		
	}

	private void resolveBridgeVertices(P2Graph graph) {
		HashMultimap<Component, BridgeVertex> bridgeVerticesByComponent = HashMultimap.<Component,BridgeVertex>create();
		for (BridgeVertex bridge:graph.getBridges()) {
			bridgeVerticesByComponent.put(bridge.getComponent(), bridge);
		}
		
		replaceBridges(bridgeVerticesByComponent);
	}
	
	private void adjustComponents(P2Graph graph) {
		for (P2Vertex v1:graph.getVertices()) {
			for (P2Edge e:v1.getOutEdges()) {
				if (e.getType()==P2Edge.NEW || e.getType()==P2Edge.ASSIGN) {
					P2Vertex v2 = e.getEnd();
					if (v1.getComponent()!=null && v2.getComponent()!=null && v1.getComponent()!=v2.getComponent()) {
						v1.getComponent().merge(v2.getComponent());
					}
				}
			}
		}
	}
	
	// replace bridge vertices by bridge edges
		private HashMultimap<Component,BridgeEdge> replaceBridges(HashMultimap<Component, BridgeVertex> bridgesByComponent) {
			
			HashMultimap<Component,BridgeEdge> map = HashMultimap.create();
			int bridgeEdgeCount  = 0;
			int bridgeVertexCount  = 0;
			
			for (Component component:bridgesByComponent.keySet()) {
				for (BridgeVertex bridge:bridgesByComponent.get(component)) {
					bridgeVertexCount = bridgeVertexCount+1;
					Collection<FieldAccessEdge> storeEdges = bridge.getReplacedStoreEdges();
					Collection<FieldAccessEdge> loadEdges = bridge.getReplacedLoadEdges();
					
					// DEBUG CODE STARTS
					if (storeEdges.size()!=bridge.getInEdges().size()) {
						Set<P2Vertex> storeSources = new HashSet<>();
						for (P2Edge e:storeEdges) storeSources.add(e.getStart());
						Set<P2Vertex> assignSources = new HashSet<>();
						for (P2Edge e:bridge.getInEdges()) assignSources.add(e.getStart());
						System.out.println(storeSources);
						System.out.println(assignSources);
						System.out.println(".");
					}
					// DEBUG CODE ENDS
					
					assert storeEdges.size()==bridge.getInEdges().size();
					assert loadEdges.size()==bridge.getOutEdges().size();
					for (FieldAccessEdge store:storeEdges) {
						assert store.getType()==P2Edge.STORE;
						P2Vertex src = store.getStart();
						for (FieldAccessEdge load:loadEdges) {
							assert load.getType()==P2Edge.LOAD;
							P2Vertex target = load.getEnd();
							assert src.getComponent()==target.getComponent();
							BridgeEdge bridgeEdge = new BridgeEdge(src,target,store,load,bridge.getRepresentedComponent());
							src.addOutEdge(bridgeEdge);
							target.addInEdge(bridgeEdge);
							bridgeEdgeCount = bridgeEdgeCount+1;
							map.put(component,bridgeEdge);
						}
					}
					
					// disconnect bridge vertex - will GC it
					for (P2Edge e:bridge.getInEdges()) {
						e.getStart().removeOutEdge(e);
					}
					for (P2Edge e:bridge.getOutEdges()) {
						e.getEnd().removeInEdge(e);
					}
				}
			}
			
			return map;
		}

}
