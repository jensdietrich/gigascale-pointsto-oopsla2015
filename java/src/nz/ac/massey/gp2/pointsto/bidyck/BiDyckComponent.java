/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.bidyck;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.bidyck.DyckRule;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Component representation. Components are sets of vertices.
 * @author jens dietrich
 */

public final class BiDyckComponent  implements Component,Serializable {
	
	private static final long serialVersionUID = -1123070490202916352L;

	// the name is usually taken from a root
	private String name = null;
		
	// the roots are the heap object or pseudo heap object vertices
	private Collection<P2Vertex> roots = new ArrayList<>();
	
	private BiDyckComponent delegate = null;
	
	// map fields to representing bridge vertices
	private Map<String,BridgeVertex> bridges = new HashMap<>();
	
	// keys are fields
	private Multimap<String,FieldAccessEdge> outgoingLoadEdges = HashMultimap.create();
	private Multimap<String,FieldAccessEdge> incomingStoreEdges = HashMultimap.create();

	public boolean isRedundant() {
		return delegate!=null;
	}
	
	public BiDyckComponent(P2Vertex root) {
		super();
		assert (root.getType()==P2Vertex.HEAP_OBJECT);
		this.roots.add(root);
		this.name = root.getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("Component[name=");
		b.append(name);
		b.append("]");
		return b.toString();
	}
	
	public Collection<FieldAccessEdge> getIncomingStoreEdges() {
		return this.incomingStoreEdges.values();
	}
		
	public Collection<FieldAccessEdge> getIncomingStoreEdges(String field) {
		return this.incomingStoreEdges.get(field);
	}
	
	public void removeIncomingStoreEdges(String field) {
		assert delegate==null; //the component might have been merged by now
		this.incomingStoreEdges.removeAll(field);
	}
	
	public void removeOutgoingLoadEdges(String field) {
		assert delegate==null;
		this.outgoingLoadEdges.removeAll(field);
	}
	
	public Collection<FieldAccessEdge> getOutgoingLoadEdges() {
		return this.outgoingLoadEdges.values();
	}
	
	public Collection<FieldAccessEdge> getOutgoingLoadEdges(String field) {
		return this.outgoingLoadEdges.get(field);
	}
	
	/**
	 * Get the components of all vertices reachable by outgoing loads, excluding this component.
	 * @return
	 */
	public Set<BiDyckComponent> getComponentsReachableByLoads() {
		Set<BiDyckComponent> biDyckComponents = new HashSet<>();
		for (FieldAccessEdge e:getOutgoingLoadEdges()) {
			biDyckComponents.add((BiDyckComponent)e.getEnd().getComponent());
		}
		return biDyckComponents;
	}
	
	/**
	 * Get the components of all vertices that can reach this component by incoming stores, excluding this component.
	 * @return
	 */
	public Set<BiDyckComponent> getComponentsReachableByReversedStores() {
		Set<BiDyckComponent> biDyckComponents = new HashSet<>();
		for (FieldAccessEdge e:getIncomingStoreEdges()) {
			biDyckComponents.add((BiDyckComponent)e.getStart().getComponent());
		}
		return biDyckComponents;
	}
	
	
	public void addIncomingStoreEdge(FieldAccessEdge e) {
		assert delegate==null;
		assert e.getEnd().getComponent()==this;
		assert e.getType()==P2Edge.STORE;
		this.incomingStoreEdges.put(e.getField(), e);
	}
	
	
	public void addOutgoingLoadEdge(FieldAccessEdge e) {
		assert delegate==null;
		assert e.getStart().getComponent()==this;
		assert e.getType()==P2Edge.LOAD;
		this.outgoingLoadEdges.put(e.getField(), e);
	}
	
	private void clear() {
		this.roots = null;
		this.outgoingLoadEdges = null;
		this.incomingStoreEdges = null;	
	}
	
	public void addBridge(BridgeVertex v) {
		this.bridges.put(v.getField(),v);
	}
	
	boolean hasBridge(String field) {
		return getBridge(field)!=null;
	}
	
	BridgeVertex getBridge(String field) {
		return this.bridges.get(field);
	}
	/**
	 * Merge vertex into this vertex, move its members and leave it as a proxy. 
	 * Do not rewrite edges here as we do not have a reference to the graph ! 
	 * @param otherComponent the component to merge with
	 */
	public void merge(Component otherComponent) {
			
		assert delegate==null;
		
		assert otherComponent instanceof BiDyckComponent;
		
		if (this==otherComponent) return ;  // nothing to do
		
		BiDyckComponent otherComponent2 = (BiDyckComponent)otherComponent;
		assert otherComponent2.delegate==null;
		
		// merge bridges
		if (otherComponent2.bridges!=null && otherComponent2.bridges.size()>0) {
			for (Map.Entry<String,BridgeVertex> entry:otherComponent2.bridges.entrySet()) {
				String bridgeField = entry.getKey();
				BridgeVertex bridge = entry.getValue();
				BridgeVertex existingBridge = this.bridges.get(bridgeField);
				if (existingBridge==null) {
					// just add to map
					this.bridges.put(bridgeField,bridge);
				}
				else {
					// this is more complicated - merge bridges (making the old bridge redundant)
					Iterator<P2Edge> iter = bridge.getInEdges().iterator();
					while (iter.hasNext()) {
						P2Edge e = iter.next();
						assert e.getType()==P2Edge.ASSIGN;
						iter.remove();
						// existingBridge.addInEdge(e);
						e.setEnd(existingBridge);
					}
					iter = bridge.getOutEdges().iterator();
					while (iter.hasNext()) {
						P2Edge e = iter.next();
						assert e.getType()==P2Edge.ASSIGN;
						iter.remove();
						// existingBridge.addOutEdge(e);
						e.setStart(existingBridge);
					}
					
					for (FieldAccessEdge e: bridge.getReplacedLoadEdges()) {
						existingBridge.addReplacedLoadEdge(e);
					}
					for (FieldAccessEdge e: bridge.getReplacedStoreEdges()) {
						existingBridge.addReplacedStoreEdge(e);
					}
					
					// additional consistency checks
					assert existingBridge.getReplacedLoadEdges().size()==existingBridge.getOutEdges().size();
					assert existingBridge.getReplacedStoreEdges().size()==existingBridge.getInEdges().size();
				}

			}
		}
		
		// merge roots
		this.roots.addAll(otherComponent2.roots);

		otherComponent2.delegate = this;
		
		// rewire !
		// this is asymmetric: merge v2 INTO v1
		for (FieldAccessEdge e:otherComponent.getIncomingStoreEdges()) {
			// e.getEnd().setComponent(this);	// not necessary
			this.addIncomingStoreEdge(e);
		}
		for (FieldAccessEdge e:otherComponent.getOutgoingLoadEdges()) {
			// e.getStart().setComponent(this); // not necessary
			this.addOutgoingLoadEdge(e);
		}
		
		// clear outside loop to avoid ConcurrentModificationException
		otherComponent2.clear();
	}

	
	/**
	 * Returns a pair of matching incoming/outgoing edges that can be used to perform a merge operation.
	 * @return an edge, or null if this is not a merge node
	 */
	public DyckRule getDyckMergeRule() {
		assert delegate==null;
		
		BridgeVertex bridge = null;
		Collection<FieldAccessEdge> edges = null;
		Collection<FieldAccessEdge> matching = null;
		
		// try some simple checks to minimise looping 
		// this significantly improved performance !!
		boolean BE = this.bridges.isEmpty();
		boolean IE = this.incomingStoreEdges.isEmpty();
		if (BE&&IE) return null;
		if (BE||IE) {
			boolean OE = this.outgoingLoadEdges.isEmpty();
			if (BE&&OE) return null;
			if (IE&&OE) return null;
		}
				
		for (String field:this.outgoingLoadEdges.keys()) {
			bridge = this.bridges.get(field);
			matching = this.incomingStoreEdges.get(field);
			if (bridge!=null || !matching.isEmpty() ) {
				edges = this.outgoingLoadEdges.get(field);
				return new DyckRule(this,field,edges,matching,bridge);
			}
		}
		
		// try the reverse - there might be fields not yet checked
		for (String field:this.incomingStoreEdges.keys()) {
			bridge = this.bridges.get(field);	
			matching = this.outgoingLoadEdges.get(field);
			if (bridge!=null || !matching.isEmpty() ) {
				edges = this.incomingStoreEdges.get(field);
				return new DyckRule(this,field,matching,edges,bridge);
			}
		}
			
		return null;
	}
	
	public int getDegree() {
		return this.incomingStoreEdges.size()+this.outgoingLoadEdges.size();
	}

	public Collection<P2Vertex> getRoots() {
		return this.roots;
	}
	
	public BiDyckComponent getDelegate() {
		if (delegate!=null) {
			BiDyckComponent d = delegate.getDelegate();
			if (d!=delegate) {
				delegate=d;
			}
			return delegate;
		}
		return this;
	}

}
