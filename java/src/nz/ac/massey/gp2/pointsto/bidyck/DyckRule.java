/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.bidyck;

import java.util.Collection;
import java.util.HashSet;

import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;

/**
 * A Dyck rule consists of a pivot component and a field satisfying the following conditions:
 * <ol>
 * 		<li>there are store edges for the field ending in the vertex within the pivot
 * 		<li>there are load edges for the field starting from a vertex within the pivot
 * </ol>
 * @author jens dietrich
 */
public final class DyckRule {
		
	private Component pivot = null;
	private String field = null;
	private Collection<FieldAccessEdge> incomingStores = null;
	private Collection<FieldAccessEdge> outgoingLoads = null;
	private Collection<Component> sourceComponents = null;
	private Collection<Component> targetComponents = null;
	private Component mergeInto = null;
	private BridgeVertex bridge = null;
	
	public DyckRule(Component pivot, String field,Collection<FieldAccessEdge> outgoing,Collection<FieldAccessEdge> incoming,BridgeVertex bridge) {
		super();
		this.pivot = pivot;
		this.field = field;
		this.incomingStores = incoming;
		this.outgoingLoads = outgoing;
		this.bridge = bridge;
	}
	public Component getPivot() {
		return pivot;
	}

	public String getField() {
		return field;
	}

	public Collection<FieldAccessEdge> getIncomingStores() {
		return incomingStores;
	}

	public Collection<FieldAccessEdge> getOutgoingLoads() {
		return outgoingLoads;
	}
	
	public BridgeVertex getBridge() {
		return bridge;
	}
	
	public int getInDegree() {
		return this.getIncomingStores().size();
	}
	
	public int getOutDegree() {
		return this.getOutgoingLoads().size();
	}
	
	/**
	 * Get the source components.
	 * Uses lazy initialisation - this is important as the components associated with vertices may change when other
	 * rules are applied and components are merged, and old components become redundant.
	 * Therefore, this method should only be called when the rule is applied ! (not when it is queued). 
	 */
	public Collection<Component> getSourceComponents() {
		if (sourceComponents==null) {
			sourceComponents = new HashSet<>();
			for (FieldAccessEdge e:incomingStores) {
				Component component = e.getStart().getComponent();
				assert !component.isRedundant();
				sourceComponents.add(component);
			}
		}
		return sourceComponents;
	}
	/**
	 * Get the target components.
	 * Uses lazy initialisation - this is important as the components associated with vertices may change when other
	 * rules are applied and components are merged, and old components become redundant.
	 * Therefore, this method should only be called when the rule is applied ! (not when it is queued).
	 */
	public Collection<Component> getTargetComponents() {
		if (targetComponents==null) {
			targetComponents = new HashSet<>();
			for (FieldAccessEdge e:outgoingLoads) {
				Component component = e.getEnd().getComponent();
				assert !component.isRedundant();
				targetComponents.add(component);
			}
		}
		return targetComponents;
	}
	/**
	 * Get the component to merge into.
	 * Uses lazy initialisation - this is important as the components associated with vertices may change when other
	 * rules are applied and components are merged, and old components become redundant.
	 * Therefore, this method should only be called when the rule is applied ! (not when it is queued).
	 */
	public Component getMergeInto() {

		if (mergeInto==null) {
			// if there is already a bridge, merge into its componenr
			if (bridge!=null) {
				mergeInto = bridge.getComponent();
			}
			// compute component with highest degree - we will merge into this
			else {
				int degree = 0;
				for (Component component:getSourceComponents()) {
					if (component==pivot) {
						mergeInto = component;
						return mergeInto;
					}
					else if (mergeInto==null) {
						mergeInto = component;
						degree = mergeInto.getDegree();
					}
					else if (component.getDegree()>degree) {
						mergeInto = component;
						degree = mergeInto.getDegree();
					}
				}
				for (Component component:getTargetComponents()) {
					if (component==pivot) {
						mergeInto = component;
						return mergeInto;
					}
					else if (component.getDegree()>degree) {
						mergeInto = component;
						degree = mergeInto.getDegree();
					}
				}
			}
		}
		return mergeInto;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DyckRule [pivot=");
		builder.append(pivot);
		builder.append(", field=");
		builder.append(field);
		builder.append("]");
		return builder.toString();
	}
	

	
}
