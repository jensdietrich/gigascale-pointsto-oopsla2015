/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.bidyck;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

import org.apache.log4j.Logger;

/**
 * Abstract Dyck component merger. 
 * @author jens dietrich
 */
public abstract class AbstractDyckComponentMerger {
		
	private static Logger LOGGER = LogSystem.getLogger(AbstractDyckComponentMerger.class);
	private static boolean DEBUG_MODE = LOGGER.isDebugEnabled();
		
	public void process(P2Graph graph) {
		applyDyckRules(graph);
	}

	/**
	 * Merge two vertices in the aggregation graph, and reassign the component to all vertices of the component to be merged.
	 * This is asymmetric: c2 will be merged into c1, and c2 will become redundant.
	 * @param c1 component 1
	 * @param c2 component 2
	 */
	protected void mergeComponents(Component c1, Component c2) {
		assert !c1.isRedundant();
		assert !c2.isRedundant();	
		c1.merge(c2);
	}
	
	/**
	 * Apply all Dyck-rules. I.e., merge vertices that have an intermediate vertex, and are connected to it by matching 
	 * store and load edges (opening and closing brackets).  
	 * @param graph the graph
	 */
	private void applyDyckRules (P2Graph graph) {

		LOGGER.info("Applying Dyck rules");
		
		int count = 0;
		long t1 = System.nanoTime();
		
		Collection<Component> componentsWithDyckRules = new HashSet<>();
		Component component = null;
		for (P2Vertex v:graph.getVertices()) {
			// getComponent will remove proxies ! i.e., there will be no proxies in this set
			component = v.getComponent();
			if (component!=null) {
				componentsWithDyckRules.add(v.getComponent());
			}	
		}
		
		DyckRule rule = null;
		
		while ((rule=findDyckRule(componentsWithDyckRules))!=null) {
			applyDyckRule (graph,rule);			
			count = count+1;
			if (DEBUG_MODE && count%100==0) {
				LOGGER.debug("  "+count + "  Dyck rules applied, no of components that could have Dyck rules is " + componentsWithDyckRules.size());
			}			
		}
		long t2 = System.nanoTime();
		LOGGER.info("Applied " + count + " Dyck rules, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
	}

	/**
	 * Find a dyck rule in a set of components and return it. Return null if no rule can be found.
	 * Also maintain the rule set - remove components that cannot generated rules.
	 * @param componentsWithDyckRules components that might have Dyck rules
	 * @return a rule
	 */
	private DyckRule findDyckRule(Collection<Component> componentsWithDyckRules) {
		Iterator<Component> iter = componentsWithDyckRules.iterator();
		while (iter.hasNext()) {
			BiDyckComponent component = (BiDyckComponent)iter.next();
			if (component.isRedundant()) iter.remove();
			else {
				DyckRule rule = component.getDyckMergeRule();
				if (rule!=null) {
					return rule;
				}
			}
		}
		return null;
	}

	/**
	 * Apply a single Dyck-rule. I.e., merge vertices that have an intermediate vertex, and are connected to it by matching 
	 * store and load edges (opening and closing brackets).  
	 * @param graph the graph
	 * @param rule the rule to be applied
	 * @return the merged endpoint component
	 */
	protected abstract Component applyDyckRule (P2Graph graph,DyckRule rule) ; 

}
