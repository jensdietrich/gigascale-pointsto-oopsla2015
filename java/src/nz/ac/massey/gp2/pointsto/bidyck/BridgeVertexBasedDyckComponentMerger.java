/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.bidyck;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.util.LogSystem;

import org.apache.log4j.Logger;

/**
 * Algorithm to merge components when matching store load pairs are encountered,
 * using bridge vertices.
 * @author jens dietrich
 */
public class BridgeVertexBasedDyckComponentMerger extends AbstractDyckComponentMerger implements BiDyckComponentMerger {
		
	private static Logger LOGGER = LogSystem.getLogger(BridgeVertexBasedDyckComponentMerger.class);
	private static boolean TRACE_MODE = LOGGER.isTraceEnabled();
		
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Component applyDyckRule (P2Graph graph,DyckRule rule) {
					
		if (TRACE_MODE) {
			LOGGER.trace("Applying Dyck rule " + rule);
		}
		
		// merge endpoints
		String field = rule.getField();
		Component mergedComponent = rule.getMergeInto();
		
		// TODO: is this still necessary ?
		Set<Component> toBeMerged = new HashSet<>();
		toBeMerged.addAll(rule.getSourceComponents());
		toBeMerged.addAll(rule.getTargetComponents());
		for (Component component:toBeMerged) {
			mergeComponents(mergedComponent,component);
		}
		// end TODO
		
		// use existing bridge, or create new one
		BridgeVertex bridge = rule.getBridge();
		if (bridge==null) {
			bridge = new BridgeVertex();
			bridge.setComponent(mergedComponent);
			Component pivot = rule.getPivot().getDelegate();
			bridge.setRepresentedComponent(pivot);
			bridge.setField(field);
			// always use this expression to refer to pivot, as this will deproxify it if necessary
			((BiDyckComponent)bridge.getRepresentedComponent()).addBridge(bridge);
		}
		else {
			// merge components of existing in/out edges 
			for (P2Edge e:bridge.getInEdges()) {
				assert e.getType()==P2Edge.ASSIGN;
				mergeComponents(mergedComponent,e.getStart().getComponent());
			}
			for (P2Edge e:bridge.getOutEdges()) {
				assert e.getType()==P2Edge.ASSIGN;
				mergeComponents(mergedComponent,e.getEnd().getComponent());
			}
		}
		
		
		// wire up new bridge component, remove old load/store edges
		Collection<FieldAccessEdge> faEdges = new ArrayList<>();
		faEdges.addAll(rule.getIncomingStores()); // copy to avoid ConcurrentModificationException
		for (FieldAccessEdge e:faEdges) {
			P2Vertex start = e.getStart();
			P2Vertex end = e.getEnd();
			
			// remove old edge
			start.removeOutEdge(e);
			end.removeInEdge(e);
			
			// add new edge
			P2Edge e2 = new P2Edge(start,bridge);
			e2.setType(P2Edge.ASSIGN);
			start.addOutEdge(e2);
			bridge.addInEdge(e2);

			// merge components
			mergeComponents(mergedComponent,start.getComponent());
			mergeComponents(bridge.getRepresentedComponent(),end.getComponent());
			
			// register (TODO experimental API)
			bridge.addReplacedStoreEdge(e);
			assert start.getComponent()==mergedComponent;
		}
		faEdges.clear();
		faEdges.addAll(rule.getOutgoingLoads());
		for (FieldAccessEdge e:faEdges) {
			P2Vertex start = e.getStart();
			P2Vertex end = e.getEnd();
			
			// remove old edge
			start.removeOutEdge(e);
			end.removeInEdge(e);
			
			// add new edge
			P2Edge e2 = new P2Edge(bridge,end);
			e2.setType(P2Edge.ASSIGN);
			bridge.addOutEdge(e2);
			end.addInEdge(e2);
			
			mergeComponents(mergedComponent,end.getComponent());
			mergeComponents(bridge.getRepresentedComponent(),start.getComponent());
			
			// register (TODO experimental API)
			bridge.addReplacedLoadEdge(e);
			// end register

			assert end.getComponent()==mergedComponent;
		}
		
		// remove edges from pivot
		// important: remove after re-wiring as this are the same lists !!
		// this is not elegant but faster (than copying the lists)
		
		// the pivot itself could have been merged (if this is a rule where the pivot is also one of the end points)
		((BiDyckComponent)bridge.getRepresentedComponent()).removeIncomingStoreEdges(field);
		((BiDyckComponent)bridge.getRepresentedComponent()).removeOutgoingLoadEdges(field);
		
		
		// DEBUG CODE
		// check state for consistency

		assert !mergedComponent.isRedundant();
		for (P2Edge e:bridge.getInEdges()) {
			assert e.getStart().getComponent()==mergedComponent;
		}
		for (P2Edge e:bridge.getOutEdges()) {
			assert e.getEnd().getComponent()==mergedComponent;
		}
		assert bridge.getComponent()==mergedComponent;
		assert bridge.getReplacedLoadEdges().size()==bridge.getOutEdges().size();
		assert bridge.getReplacedStoreEdges().size()==bridge.getInEdges().size();
		
		return mergedComponent;
	}

}
