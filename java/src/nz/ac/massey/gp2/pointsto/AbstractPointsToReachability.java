/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto;

import java.util.*;


/**
 * This interface serves as the abstraction barrier between a points-to
 * analysis client (like the testing framework) and the points-to algorithm.
 * @author jens dietrich, nic hollingum
 */
public abstract class AbstractPointsToReachability {
	
	private P2Graph myGraph = null; 
	
	public final void process(P2Graph g,RefinementListener... listeners){
		myGraph = g;
		processInternal(g, listeners);
	}
	
	public final P2Graph getGraph() throws Exception{
		if(myGraph == null) throw new Exception("Current graph unset, please call \"process\" first");
		return myGraph;
	}
	
	protected abstract void processInternal(P2Graph g,RefinementListener... listeners);
	
	public abstract Set<P2Vertex> getPointsToSet(P2Vertex var);
	
	public abstract boolean pointsTo(P2Vertex var,P2Vertex heapObj);
	
	// added by jens to control lifecycle, e.g. for unit testing
	public abstract void reset() ;

}
