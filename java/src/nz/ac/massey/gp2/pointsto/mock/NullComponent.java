/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.mock;

import java.util.Collection;
import java.util.Collections;

import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Vertex;

/**
 * This is a trivial component that can be used if the graph is not componentised.  
 * This is a singleton.
 * @author jens dietrich
 */
public final class NullComponent implements Component {
	
	public static final NullComponent SOLE_INSTANCE = new NullComponent();
	
	private NullComponent() {
		super();
	}

	@Override
	public boolean isRedundant() {
		return false;
	}

	@Override
	public Component getDelegate() {
		return null;
	}

	@Override
	public String getName() {
		return "null component";
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FieldAccessEdge> getIncomingStoreEdges() {
		return Collections.EMPTY_SET;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FieldAccessEdge> getIncomingStoreEdges(String field) {
		return Collections.EMPTY_SET;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FieldAccessEdge> getOutgoingLoadEdges() {
		return Collections.EMPTY_SET;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FieldAccessEdge> getOutgoingLoadEdges(String field) {
		return Collections.EMPTY_SET;
	}

	@Override
	public void merge(Component otherComponent) {
		// nothing to do here
	}

	@Override
	public int getDegree() {
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<P2Vertex> getRoots() {
		return Collections.EMPTY_SET;
	}

	@Override
	public void addIncomingStoreEdge(FieldAccessEdge e) {
		// nothing to do here
	}

	@Override
	public void addOutgoingLoadEdge(FieldAccessEdge e) {
		// nothing to do here
	}

}
