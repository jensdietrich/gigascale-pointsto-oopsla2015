/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto;

import java.util.Map;

import nz.ac.massey.gp2.transitiveclosure.SuccessorSet;
import nz.ac.massey.gp2.transitiveclosure.SuccessorSetFactory;

/**
 * Interface for services that refine a reachability index. The purpose of refinement is to remove some or preferably all spurious edges
 * added when the bridge oracle was generated.
 * <p>
 * Refinement listeners can be used to observe and interrupt this process. Once complete, points-to information can be queried using the getPointsTo() method.
 * <p>
 * At the moment, we assume that the bridge oracle generator has created components, and has associated vertices with these components. 
 * This requirement can be satisfied by associating the NullComponent with all vertices.
 * 
 * @author jens dietrich
 */
public interface Refiner extends Builder {
	
	// maps components to maps associating var vertices with sets of heap object vertices they reference
	public Map<Component,Map<Integer,SuccessorSet<Integer>>> getPointsTo();
	public Map<Integer,P2Vertex> getVerticesById();

	/**
	 * This is used to set the mechanism used to represent points-to sets. 
	 * It is highly recommended to use a mechanism here that supports fast merging, and effective representation in memory.
	 * @param successorSetFactory the successor set factory used
	 */
	public void setSuccessorSetFactory(SuccessorSetFactory<Integer> successorSetFactory) ;
	
	public void addRefinementListener(RefinementListener listener);
	public void removeRefinementListener(RefinementListener listener);
	
	/**
	 * Indicates whether the refinement strategy used can remove all spurious edges / false positives.
	 * @return
	 */
	public boolean isPrecise();
	
	/**
	 * Reset any internal state.
	 */
	public abstract void reset();
}

