/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.util.IOUtils;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

/**
 * Access to generated contains oracles.
 * @author jens dietrich
 */
public class TestOracleLoader {
	
	private static Logger LOGGER = LogSystem.getLogger(TestOracleLoader.class);
	
	/**
	 * Loads test oracles from a file. The oracles are CSV records with two columns per row, representing
	 * two vertices. These names are resolved to vertices against a graph.
	 * @param graph the graph
	 * @param file a CSV file name
	 * @return a collection of oracles - tuples consist of two points, point 2 is reachable from point 2
	 * @throws Exception
	 */
	public static Collection<P2Vertex[]> loadTestOracles(P2Graph graph,String file) throws Exception {
		
		return loadTestOracles(graph,file,Integer.MAX_VALUE);
	}
	
	/**
	 * Loads test oracles from a file. The oracles are CSV records with two columns per row, representing
	 * two vertices. These names are resolved to vertices against a graph.
	 * @param graph the graph
	 * @param file a CSV file name
	 * @param maxSize the max number of oracles to load
	 * @return a collection of oracles - tuples consist of two points, point 2 is reachable from point 2
	 * @throws Exception
	 */
	public static Collection<P2Vertex[]> loadTestOracles(P2Graph graph,String file, int maxSize) throws Exception {
		
		LOGGER.info("Loading test oracles from " + file);
		long t1 = System.nanoTime();
		
		Map<String,P2Vertex> verticesByName = new HashMap<>();
		for (P2Vertex v:graph.getVertices()) {
			verticesByName.put(v.getName(),v);
		}
		
		Collection<P2Vertex[]> data = new ArrayList<>();
		int s = 0;
		String line=null;
		BufferedReader reader = new BufferedReader(new FileReader(file));

		while (s<maxSize && (line=reader.readLine()) != null) {
			String[] nextRecord = IOUtils.parse2ColCSVLine(line,true);
			if (nextRecord!=null) {
				P2Vertex[] nextData = new P2Vertex[] {
						verticesByName.get(nextRecord[0]),
						verticesByName.get(nextRecord[1])
				};
				data.add(nextData);
				if (s%1000==0) LOGGER.debug("test oracles loaded: " + s);
				s=s+1;
			}
		}
		reader.close();
		long t2 = System.nanoTime();
		LOGGER.info("Loaded " + data.size() + " test oracles, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
		return data;
	}
}
