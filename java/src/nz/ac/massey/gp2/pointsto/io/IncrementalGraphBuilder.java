/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.util.IOUtils;
import nz.ac.massey.gp2.util.LogSystem;

/**
 * Utility to build graphs from CVS input data.
 * @author jens dietrich
 */
public class IncrementalGraphBuilder {
	
	public static Logger LOGGER = LogSystem.getLogger(IncrementalGraphBuilder.class);
	private static boolean TRACE_MODE = LOGGER.isTraceEnabled();
	
	Map<String,P2Vertex> nodeIndex = new HashMap<String,P2Vertex>();

	private P2Graph graph = new P2Graph();
	
	public P2Graph getGraph() {
		return graph;
	}

	public IncrementalGraphBuilder addAllocData(File allocTable) throws Exception {
		
		if (!allocTable.exists()) {
			LOGGER.warn("File does not exist: " + allocTable.getAbsolutePath());
			return this;
		}
		
		int vertexCount1 = graph.getVertexCount(); 
		int edgeCount = 0;
		String line= null;
		BufferedReader reader = new BufferedReader(new FileReader(allocTable));
		while ((line=reader.readLine()) != null) {
			String[] nextRecord = IOUtils.parse2ColCSVLine(line,false);
			P2Vertex src = getOrAddNode(nodeIndex,nextRecord[0],P2Vertex.NAME);
			P2Vertex target = getOrAddNode(nodeIndex,nextRecord[1],P2Vertex.HEAP_OBJECT);
			// target can be null as we skip certain refs
			// see skipRefsToHeapObject
			if (src!=null && target!=null) {
				// reverse order if nodes from ALLOC
				addEdge(target,src,P2Edge.NEW);
				src.setAllocs(true);
				edgeCount=edgeCount+1;
			}
		}

		reader.close();
		
		int vertexCount2 = graph.getVertexCount(); 
		LOGGER.debug("Imported ALLOC records from " + allocTable.getAbsolutePath());
		LOGGER.debug("   - imported " + (vertexCount2-vertexCount1) + " vertices");
		LOGGER.debug("   - imported " + edgeCount + " edges");
		
		return this;
	}
		
	public IncrementalGraphBuilder addAssignData(File assignTable) throws Exception {
		
		if (!assignTable.exists()) {
			LOGGER.warn("File does not exist: " + assignTable.getAbsolutePath());
			return this;
		}
		
		int vertexCount1 = graph.getVertexCount(); 
		int edgeCount = 0;
		String line= null;	
		BufferedReader reader = new BufferedReader(new FileReader(assignTable));
		while ((line=reader.readLine()) != null) {
			String[] nextRecord = IOUtils.parse2ColCSVLine(line,false);
			P2Vertex src = getOrAddNode(nodeIndex,nextRecord[0],P2Vertex.NAME);
			P2Vertex target = getOrAddNode(nodeIndex,nextRecord[1],P2Vertex.NAME);
			// target can be null as we skip certain refs
			// see skipRefsToHeapObject
			if (src!=null && target!=null) {
				addEdge(src,target,P2Edge.ASSIGN);
				edgeCount=edgeCount+1;
			}
		}
		reader.close();
		
		int vertexCount2 = graph.getVertexCount(); 
		LOGGER.debug("Imported ASSIGN records from " + assignTable.getAbsolutePath());
		LOGGER.debug("   - imported " + (vertexCount2-vertexCount1) + " vertices");
		LOGGER.debug("   - imported " + edgeCount + " edges");
		
		return this;
		
		
	}
	public IncrementalGraphBuilder addLoadData(File loadTable) throws Exception {
		
		if (!loadTable.exists()) {
			LOGGER.warn("File does not exist: " + loadTable.getAbsolutePath());
			return this;
		}
		
		int vertexCount1 = graph.getVertexCount(); 
		int edgeCount = 0;
		String line= null;
		BufferedReader reader = new BufferedReader(new FileReader(loadTable));	
		while ((line=reader.readLine()) != null) {
			String[] nextRecord = IOUtils.parse3ColCSVLine(line,false);
			P2Vertex src = getOrAddNode(nodeIndex,nextRecord[0],P2Vertex.NAME);
			P2Vertex target = getOrAddNode(nodeIndex,nextRecord[1],P2Vertex.NAME);
			String fieldId = nextRecord[2];
			// target can be null as we skip certain refs
			// see skipRefsToHeapObject
			if (src!=null && target!=null) {
				addEdge(src,target,P2Edge.LOAD,fieldId);
				edgeCount = edgeCount+1;
			}
		}
		reader.close();
		
		int vertexCount2 = graph.getVertexCount(); 
		LOGGER.debug("Imported LOAD records from " + loadTable.getAbsolutePath());
		LOGGER.debug("   - imported " + (vertexCount2-vertexCount1) + " vertices");
		LOGGER.debug("   - imported " + edgeCount + " edges");
		
		return this;
	}
	
	public IncrementalGraphBuilder addStoreData(File storeTable) throws Exception {
		
		if (!storeTable.exists()) {
			LOGGER.warn("File does not exist: " + storeTable.getAbsolutePath());
			return this;
		}
		
		int vertexCount1 = graph.getVertexCount(); 
		int edgeCount = 0;
		String line= null;
		BufferedReader reader = new BufferedReader(new FileReader(storeTable));
		while ((line=reader.readLine()) != null) {
			String[] nextRecord = IOUtils.parse3ColCSVLine(line,false);
			P2Vertex src = getOrAddNode(nodeIndex,nextRecord[0],P2Vertex.NAME);
			P2Vertex target = getOrAddNode(nodeIndex,nextRecord[1],P2Vertex.NAME);
			String fieldId = nextRecord[2];
			// target can be null as we skip certain refs
			// see skipRefsToHeapObject
			if (src!=null && target!=null) {
				addEdge(src,target,P2Edge.STORE,fieldId);
				edgeCount = edgeCount+1;
			}
		}
		reader.close();

		int vertexCount2 = graph.getVertexCount(); 
		LOGGER.debug("Imported STORE records from " + storeTable.getAbsolutePath());
		LOGGER.debug("   - imported " + (vertexCount2-vertexCount1) + " vertices");
		LOGGER.debug("   - imported " + edgeCount + " edges");
		
		return this;
	}
		
	private P2Edge addEdge(P2Vertex src,P2Vertex target, byte type) {	
		if (TRACE_MODE && src==target) LOGGER.trace("Adding self-edge for " + src.getName() + " , type=" + src.getType());
		P2Edge e = new P2Edge(src,target);
		e.setType(type);
		src.addOutEdge(e);
		target.addInEdge(e);
		return e;
	}
	
	private P2Edge addEdge(P2Vertex src,P2Vertex target, byte type,String fieldId) {	
		if (TRACE_MODE && src==target) LOGGER.trace("Adding self-edge for " + src.getName() + " , type=" + src.getType());
		FieldAccessEdge e = new FieldAccessEdge(src,target);
		e.setType(type);
		e.setField(fieldId);
		src.addOutEdge(e);
		target.addInEdge(e);
		return e;
	}
	
	private static boolean skipRefsToHeapObject(String name) {
		return false; //name.equals("<null>");
	}

	private P2Vertex getOrAddNode(Map<String, P2Vertex> nodeIndex, String nodeName, byte nodeType) {
		if (nodeType==P2Vertex.HEAP_OBJECT && skipRefsToHeapObject(nodeName)) return null;
		
		P2Vertex node = nodeIndex.get(nodeName);
		if (node!=null && node.getType()!=nodeType) {
			LOGGER.warn("inconsistent node type: " + nodeName);
		}
		if (node==null) {
			node = new P2Vertex();
			node.setType(nodeType);
			node.setName(nodeName);
			nodeIndex.put(nodeName, node);
			graph.add(node);
		}
		return node;
	}
	

}
