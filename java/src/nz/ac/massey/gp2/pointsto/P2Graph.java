/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto;

import java.util.*;

import nz.ac.massey.gp2.pointsto.bidyck.BridgeVertex;

/**
 * Simple data structure to represent a graph.
 * We prefer this to JUNG graphs as
 * JUNG graphs do not permit multiple edges between the same source and target vertices.
 * @author jens dietrich
 */
public class P2Graph {
	private Collection<P2Vertex> vertices = new HashSet<>();
	public boolean add(P2Vertex v) {
		return this.vertices.add(v);
	}
	public boolean remove(P2Vertex v) {
		return this.vertices.remove(v);
	}
	public Collection<P2Vertex> getVertices() {
		return vertices;
	}
	public int getVertexCount() {
		return this.getVertices().size();
	}
	
	public Set<Component> getComponents() {
		Set<Component> components = new HashSet<>();
		for (P2Vertex v:this.getVertices()) {
			Component component = v.getComponent();
			if (component!=null) {
				components.add(component);
			}
		}
		return components;
	}
	
	public Set<BridgeVertex> getBridges() {
		Set<BridgeVertex> bridges = new HashSet<>();
		for (P2Vertex v:this.getVertices()) {
			for (P2Edge e:v.getOutEdges()) {
				P2Vertex v2 = e.getEnd();
				if (v2 instanceof BridgeVertex) {
					bridges.add((BridgeVertex) v2);
				}
			}
		}
		return bridges;
	}
	
	
//	public void printBridgeStats(Logger logger,org.apache.log4j.Level level) {
//		
//		logger.log(level,"Printing bridge stats, this is slow");
//		long t1 = System.nanoTime();
//		Set<BridgeVertex> bridges = this.getBridges();
//
//		double weightSum1 = 0.0;
//		int maxWeight1 = 0;
//		double weightSum2 = 0.0;
//		int maxWeight2 = 0;
//		
//		for (BridgeVertex bridge:bridges) {
//			int inDegree1 = bridge.getInEdges().size();
//			int outDegree1 = bridge.getOutEdges().size();
//			int weight1 = inDegree1 * outDegree1;
//			weightSum1 = weightSum1 + weight1;
//			maxWeight1 = Math.max(maxWeight1,weight1);
//			
//			int inDegree2 = bridge.getTargetsOfReplacedStoreEdges().size();
//			int outDegree2 = bridge.getSourcesOfReplacedLoadEdges().size();
//			int weight2 = inDegree2 * outDegree2;
//			weightSum2 = weightSum2 + weight2;
//			maxWeight2 = Math.max(maxWeight2,weight2);
//
//		}
//		
//		logger.log(level,"Bridges found:  " + bridges.size());
//		logger.log(level,"Weight sum:  " + weightSum1 + " (" + weightSum2 + ")");
//		logger.log(level,"Avg weight:  " + weightSum1/bridges.size() + " (" + weightSum2/bridges.size() + ")");
//		logger.log(level,"Max weight:  " + maxWeight1 + " (" + maxWeight2 + ")");
//		
//		long t2 = System.nanoTime();
//		logger.log(level,"Counted bridge stats, this took " + MeasurementUtils.formatNanos(t1,t2));
//	}
	
}
