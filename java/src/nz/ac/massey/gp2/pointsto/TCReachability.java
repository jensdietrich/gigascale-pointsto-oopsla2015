/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto;

import java.util.*;

import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;
import nz.ac.massey.gp2.transitiveclosure.SuccessorSet;
import nz.ac.massey.gp2.transitiveclosure.SuccessorSetFactory;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;


/**
 * Transitive-closure based reachability. 
 * Most work is delegated to a BridgeOracleGenerator and a Refiner,
 * both must be configured.
 * @author jens dietrich
 */
public class TCReachability extends AbstractPointsToReachability {

	private static Logger LOGGER = LogSystem.getLogger(AbstractPointsToReachability.class);

	private SuccessorSetFactory<Integer> successorSetFactory = new ConciseSuccessorSetFactory1();
	private Refiner refiner = null;
	private BridgeOracleGenerator bridgeOracleGenerator = null;
	
	private Map<Component,Map<Integer,SuccessorSet<Integer>>> pointsToMap = null;
	private Map<Integer,P2Vertex> verticesById = null;

	@Override
	public void reset() {
		this.pointsToMap.clear();
		this.verticesById.clear();
		this.refiner.reset();
	}
	/**
	 * Answers a single source - single target query for a pair of nodes.
	 * @param var a var node
	 * @param heapObj a heap object
	 * @return
	 */
	public boolean pointsTo(P2Vertex var,P2Vertex heapObj) {
		assert var.getType()==P2Vertex.NAME;
		assert heapObj.getType()==P2Vertex.HEAP_OBJECT;
		Component component = var.getComponent();
		if (component==null) return false;
		if (component!=heapObj.getComponent()) return false;
		Map<Integer,SuccessorSet<Integer>> map = this.pointsToMap.get(component);
		SuccessorSet<Integer> ss = map.get(var.getSIndex());
		return ss.contains(heapObj.getSIndex());
	}

	/**
	 * Answers a single source - single target alias query for a pair of nodes.
	 * @param var1
	 * @param var2
	 * @return
	 */
	public boolean mayAlias (P2Vertex var1, P2Vertex var2) {
		assert var1.getType()==P2Vertex.NAME;
		assert var2.getType()==P2Vertex.NAME;
		Component component = var1.getComponent();
		if (component!=null && component==var2.getComponent()) {			
			Map<Integer,SuccessorSet<Integer>> map = this.pointsToMap.get(component);
			SuccessorSet<Integer> h1 = map.get(var1.getSIndex());
			SuccessorSet<Integer> h2 = map.get(var2.getSIndex());
			return h1.intersects(h2);
		}
		else {
			return false;
		}
	}

	/**
	 * Get the pointsTo set for a variable vertex.
	 * @param var a variable node
	 * @return a set of heap object vertices, or null if the vertex has not been indexes (is "ungrounded") 
	 */
	public Set<P2Vertex> getPointsToSet(P2Vertex var) {
		assert var.getType()==P2Vertex.NAME;
		if (var.getSIndex()==-1) return null;
		Set<P2Vertex> reachable = new HashSet<>();
		Map<Integer,SuccessorSet<Integer>> map = this.pointsToMap.get(var.getComponent());
		for (Integer index:map.get(var.getSIndex())) {
			P2Vertex v = verticesById.get(index);
			assert v.getType()==P2Vertex.HEAP_OBJECT;
			reachable.add(v);
		}
		return reachable;
	}


	protected void processInternal(P2Graph graph,RefinementListener... listeners) {
		
		Preconditions.checkNotNull(this.refiner, "The refiner must be set, and cannot be null");
		Preconditions.checkNotNull(this.bridgeOracleGenerator, "The bridge pracle generator must be set, and cannot be null");
		Preconditions.checkNotNull(this.successorSetFactory, "The succesor set factory must be set, and cannot be null");
		
		long startTime = System.nanoTime();
	
		LOGGER.info("Bridge oracle generation");
		long t1 = System.nanoTime();
		bridgeOracleGenerator.process(graph);
		long t2 = System.nanoTime();
		LOGGER.info("Generated bridge oracles, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		LOGGER.info("Refinement");
		t1 = System.nanoTime();
		refiner.setSuccessorSetFactory(successorSetFactory);
		for (RefinementListener listener:listeners) {
			refiner.addRefinementListener(listener);
		}
		refiner.process(graph);
		this.pointsToMap = refiner.getPointsTo();
		this.verticesById = refiner.getVerticesById();

		t2 = System.nanoTime();
		LOGGER.info("Refinement done, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		long endTime = System.nanoTime();
		LOGGER.info("Overall reachability index generation took " + MeasurementUtils.formatNanos(startTime,endTime) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
	}

	public SuccessorSetFactory<Integer> getSuccessorSetFactory() {
		return successorSetFactory;
	}

	public void setSuccessorSetFactory(SuccessorSetFactory<Integer> successorSetFactory) {
		this.successorSetFactory = successorSetFactory;
	}

	public Refiner getRefiner() {
		return refiner;
	}

	public void setRefiner(Refiner refiner) {
		this.refiner = refiner;
	}

	public BridgeOracleGenerator getBridgeOracleGenerator() {
		return bridgeOracleGenerator;
	}

	public void setBridgeOracleGenerator(BridgeOracleGenerator bridgeOracleGenerator) {
		this.bridgeOracleGenerator = bridgeOracleGenerator;
	}


}
