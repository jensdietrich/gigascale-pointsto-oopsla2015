/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto;

import java.io.Serializable;

/**
 * Custom edge class.
 * @author jens dietrich
 */

public class P2Edge implements Serializable {

	private static final long serialVersionUID = 8084858521563114358L;
	protected P2Vertex start = null;
	protected P2Vertex end = null;
	
	public static final byte NEW = 0;
	public static final byte ASSIGN = 1;	
	public static final byte LOAD = 2;
	public static final byte STORE = 3;
	
	protected byte type = NEW;
	
	
	public P2Edge(P2Vertex start, P2Vertex end) {
		super();
		this.start = start;
		this.end = end;
	}
	
	public P2Vertex getStart() {
		return start;
	}
	public P2Vertex getEnd() {
		return end;
	}
	public void setStart(P2Vertex start) {
		if (this.start!=null) {
			start.removeOutEdge(this);
		}
		this.start = start;
		start.addOutEdge(this);
	}
	public void setEnd(P2Vertex end) {
		if (this.end!=null) {
			start.removeInEdge(this);
		}
		this.end = end;
		end.addInEdge(this);
	}
	
	@Override
	public String toString() {
		String t = null;
		if (type==ASSIGN) t = "assign";
		else if (type==NEW) t = "alloc";
		else if (type==LOAD) t = "load";
		else if (type==STORE) t = "store";
		else throw new IllegalStateException();
		
		return new StringBuffer() 			
			.append("P2Edge[")
			.append(this.start.getName())
			.append(" -> ")
			.append(this.end.getName())
			.append("] (type=")
			.append(t)
			.append(")")
			.toString();
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

}
