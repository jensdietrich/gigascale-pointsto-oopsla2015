/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto;

/**
 * Bridge edges represents edges inferred from matching store-load pairs.
 * @author jens dietrich
 */

public class BridgeEdge extends P2Edge {

	private static final long serialVersionUID = -8326274546423137492L;
	
	private FieldAccessEdge storeEdge = null;
	private FieldAccessEdge loadEdge = null;
	// this is the component represented by this vertex - the pivot of the respective Dyck rule
	private Component representedComponent = null;
	
	public BridgeEdge(P2Vertex start, P2Vertex end,FieldAccessEdge storeEdge, FieldAccessEdge loadEdge,Component representedComponent) {
		super(start,end);
		this.setType(P2Edge.ASSIGN);
		assert storeEdge.getType()==P2Edge.STORE;
		assert loadEdge.getType()==P2Edge.LOAD;
		assert storeEdge.getField().equals(loadEdge.getField());
		this.storeEdge = storeEdge;
		this.loadEdge = loadEdge;
		this.representedComponent = representedComponent;
		
	}
	@Override
	public String toString() {		
		return new StringBuffer() 			
			.append("BridgeEdge[")
			.append(this.start.getName())
			.append(" -> ")
			.append(this.end.getName())
			.append("]")
			.toString();
	}
	public P2Edge getStoreEdge() {
		return storeEdge;
	}
	public P2Edge getLoadEdge() {
		return loadEdge;
	}
	public Component getRepresentedComponent() {
		return representedComponent;
	}


}
