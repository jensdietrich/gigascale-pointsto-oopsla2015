/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto;

import java.io.Serializable;
import java.util.*;

/**
 * General vertex class. Vertices have references to incoming and outgoing edges. This is different to JUNG 2. 
 * The advantage is that lookup/navigation is faster.
 * @author jens dietrich
 */

public class P2Vertex implements Serializable {

	private static final long serialVersionUID = 6383085786558591800L;
	
	private int sindex = -1;
	
	// types
	public static final byte  NAME = 0;
	public static final byte  HEAP_OBJECT = 1;
	private byte type = NAME;
	
	private String name = null;
	private Collection<P2Edge> outEdges = new HashSet<>();
	private Collection<P2Edge> inEdges = new HashSet<>();
	private Component component = null;
	


	// cached info whether this is a vertex that is the source of an edge labelled with ALLOC
	private boolean allocs = false; 

	public P2Vertex() {
		super();
	}

	public int getSIndex() {
		return sindex;
	}
	
	public Collection<P2Edge> getOutEdges() {
		return outEdges;
	}
	public Collection<P2Edge> getInEdges() {
		return inEdges;
	}
	public void addInEdge(P2Edge e) {
		this.inEdges.add(e);
		// boolean added = this.inEdges.add(e);
		// assert added;  now with bridge vertex merging, the same edge can be re-added
	}
	public void addOutEdge(P2Edge e) {
		this.outEdges.add(e);
		// boolean added = this.outEdges.add(e);
		// assert added;  now with bridge vertex merging, the same edge can be re-added
	}
	public boolean removeInEdge(P2Edge e) {
		return this.inEdges.remove(e);
	}
	public boolean removeOutEdge(P2Edge e) {
		return this.outEdges.remove(e);
	}
	
	public void sortIncoming(Comparator<P2Edge> sortDef) {
		List<P2Edge> sorted = new ArrayList<P2Edge>();
		sorted.addAll(this.inEdges);
		Collections.sort(sorted,sortDef);
		inEdges = sorted;
	}
	
	public void sortOutgoing(Comparator<P2Edge> sortDef) {
		List<P2Edge> sorted = new ArrayList<P2Edge>();
		sorted.addAll(this.outEdges);
		Collections.sort(sorted,sortDef);
		outEdges = sorted;
	}
	
	public void sortEdges(Comparator<P2Edge> sortDef) {
		sortOutgoing(sortDef);
		sortIncoming(sortDef);
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAllocs() {
		return allocs;
	}

	public void setAllocs(boolean allocs) {
		this.allocs = allocs;
	}
	
	@Override
	public String toString() {
		String typeName = this.type==NAME ? "name" : (this.type==HEAP_OBJECT ? "heapobj" : "?");
		return "P2Vertex [" + name + ", type=" + typeName + "]";
	}
	
	public void setSIndex(int index) {
		this.sindex = index;
	}
	
	/**
	 * Get any incoming edge with a certain edge type, or null if there is no such edge.
	 * @param edgeType the edge type
	 * @return an edge or null
	 */
	public P2Edge getInEdge(byte edgeType) {
		for (P2Edge e:this.getInEdges()) {
			if (e.getType()==edgeType) return e;
		}
		return null;
	}
	
	/**
	 * Get any outgoing edge with a certain edge type, or null if there is no such edge.
	 * @param edgeType the edge type
	 * @return an edge or null
	 */
	public P2Edge getOutEdge(byte edgeType) {
		for (P2Edge e:this.getOutEdges()) {
			if (e.getType()==edgeType) return e;
		}
		return null;
	}
	
	public Component getComponent() {
		if (this.component==null) {
			return null;
		}
		else {
			// resolve proxies whenever this method is accessed
			if (this.component.isRedundant()) {
				this.component = this.component.getDelegate();
			}
			return this.component;
		}
	}
	


	public void setComponent(Component component) {
		this.component = component;
	}
}
