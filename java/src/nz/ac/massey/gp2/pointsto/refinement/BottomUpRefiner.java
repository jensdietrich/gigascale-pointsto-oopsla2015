/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.refinement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;

import nz.ac.massey.gp2.pointsto.BridgeEdge;
import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.RefinementListener;
import nz.ac.massey.gp2.pointsto.Refiner;
import nz.ac.massey.gp2.transitiveclosure.SuccessorSet;
import nz.ac.massey.gp2.transitiveclosure.SuccessorSetFactory;
import nz.ac.massey.gp2.util.Counter;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

/**
 * Refiner that looks for fixpoints to address self-supporting bridge edges.
 * All bridge edges are initially marked as unsafe (uncertified)
 * and then evaluated in order to detect self-supporting bridge edges. 
 * This is described in detail in the paper. 
 * @author jens dietrich
 */
public class BottomUpRefiner implements Refiner {
	
	private static Logger LOGGER = LogSystem.getLogger(BottomUpRefiner.class);
	private static boolean TRACE_MODE = LOGGER.isTraceEnabled();
	
	protected SuccessorSetFactory<Integer> successorSetFactory = null;
	protected Map<Component,Map<Integer,SuccessorSet<Integer>>> pointsToMap = null;
	protected Map<Integer,P2Vertex> verticesById = null; 
	protected List<RefinementListener> listeners = new ArrayList<>();
	
	public BottomUpRefiner() {
		super();
		this.pointsToMap = createNewPointsToMap();
		this.verticesById = createNewVerticesByIdMap();
	}
	
	@Override
	public boolean isPrecise(){
		return true;
	}
	
	@Override
	public void addRefinementListener(RefinementListener listener) {
		this.listeners.add(listener);
	}
	@Override
	public void removeRefinementListener(RefinementListener listener) {
		this.listeners.remove(listener);
	}
	
	/**
	 * Create a new points to to map. 
	 * If concurrency is supported, a ConcurrentMap should be used. Otherwise, a plain old HashMap will do.
	 * @return
	 */
	private Map<Component, Map<Integer, SuccessorSet<Integer>>> createNewPointsToMap() {
		return new HashMap<>();
	}
	
	/**
	 * Create a new vertices by id to map. 
	 * If concurrency is supported, a ConcurrentMap should be used. Otherwise, a plain old HashMap will do.
	 * @return
	 */
	private Map<Integer,P2Vertex> createNewVerticesByIdMap() {
		return new HashMap<>();
	}

	// maps components to maps associating var vertices with sets of heap object vertices they reference
	public Map<Component,Map<Integer,SuccessorSet<Integer>>> getPointsTo() {
		return pointsToMap;
	}
	
	@Override
	public Map<Integer, P2Vertex> getVerticesById() {
		return verticesById;
	}

	public void setSuccessorSetFactory(SuccessorSetFactory<Integer> successorSetFactory) {
		this.successorSetFactory = successorSetFactory;
	}

	@Override
	public void process(P2Graph graph) {

		Collection<BridgeEdge> uncertifiedBridgeEdges = new HashSet<>();
		for (P2Vertex v:graph.getVertices()) {
			for (P2Edge e:v.getOutEdges()) {
				if (e instanceof BridgeEdge) {
					uncertifiedBridgeEdges.add((BridgeEdge)e);
				}
			}
		}
		
		HashMultimap<Component, P2Vertex> verticesByComponent = HashMultimap.<Component,P2Vertex>create();
		for (P2Vertex vertex:graph.getVertices()) {
			// not that not all vertices might be assigned to a component when it is being preprocessed 
			if (vertex.getComponent()!=null) {
				verticesByComponent.put(vertex.getComponent(), vertex);
			}
		}
		
		// initialise pointsToMap 
		for (Component component:graph.getComponents()) {
			this.pointsToMap.put(component,new HashMap<Integer,SuccessorSet<Integer>>());
		}
				
		// build initial pointsToMap
		int iteration = 0;
		boolean carryOn = true;
		for (RefinementListener listener:listeners) listener.refinementStarted(graph,null,iteration);
		buildPoinstToIndex(graph,verticesByComponent,uncertifiedBridgeEdges);
		for (RefinementListener listener:listeners) carryOn=carryOn&listener.refinementDone(graph, Collections.<Component, Integer> emptyMap(),iteration);
		
		boolean done = false;
		while (!done) {
			iteration = iteration+1;
			LOGGER.info("Starting iteration " + iteration + ", uncertified bridge edges: " + uncertifiedBridgeEdges.size());
			
			Collection<BridgeEdge> uncertifiedBridgeEdges2 =  certify(graph,verticesByComponent,uncertifiedBridgeEdges);
			done = (uncertifiedBridgeEdges2.size()==0 || uncertifiedBridgeEdges2.size()==uncertifiedBridgeEdges.size());
			uncertifiedBridgeEdges = uncertifiedBridgeEdges2;
			this.resetAllIndices(graph);
			this.buildPoinstToIndex(graph, verticesByComponent,uncertifiedBridgeEdges);
		}
	}
	
	public boolean mayAlias (P2Vertex var1, P2Vertex var2) {
		assert var1.getType()==P2Vertex.NAME;
		assert var2.getType()==P2Vertex.NAME;
		// a vertex is not always alias of itself (must consider case when pointsTo set is empty)
		if (var1.getComponent()==var2.getComponent()) {
			Component component = var1.getComponent();
			Map<Integer,SuccessorSet<Integer>> map = this.pointsToMap.get(component);	
			SuccessorSet<Integer> h1 = map.get(var1.getSIndex());
			SuccessorSet<Integer> h2 = map.get(var2.getSIndex());
			return h1.intersects(h2);
		}
		else {
			return false;
		}
	}

	private Set<BridgeEdge> certify(P2Graph graph,HashMultimap<Component, P2Vertex> verticesByComponent,Collection<BridgeEdge> uncertifiedBridgeEdges) {
		Set<BridgeEdge> newUncertifiedBridgeEdges = new HashSet<>();
		for (BridgeEdge bridge:uncertifiedBridgeEdges) {
			P2Vertex storeTarget = bridge.getStoreEdge().getEnd();
			P2Vertex loadSource = bridge.getLoadEdge().getStart();
			if (!mayAlias(storeTarget,loadSource)) {
				newUncertifiedBridgeEdges.add(bridge);
			}
		}
		return newUncertifiedBridgeEdges;
	}
	
	// a serial strategy is hardcoded here, the reason is that this makes it easy to assign ids to heap objects in blocks,
	// improving the performance of the compressed bit maps
	// TODO evaluate whether it is worth parallelising this
	private void buildPoinstToIndex(P2Graph graph,HashMultimap<Component, P2Vertex> verticesByComponent,Collection<BridgeEdge> skipBridges) {
		LOGGER.debug("(Re-) building  points to map");
		long t1 = System.nanoTime();
			
		// note that we will ignore vertices not in component !
		int OFFSET = Math.max(10_000_000,graph.getVertexCount());
		Counter heapObjectId = new Counter(0);
		Counter varId = new Counter(2*OFFSET);
		
		for (Component component:verticesByComponent.keySet()) {
			buildPointsToIndex(graph,component, verticesByComponent.get(component),heapObjectId,varId,skipBridges);
		}
		long t2 = System.nanoTime();
		LOGGER.debug("(Re-) built initial points to map, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
	}
	
	/**
	 * Reset all ids and indices.
	 * @param graph
	 */
	private void resetAllIndices(P2Graph graph) {
		for (P2Vertex v:graph.getVertices()) {
			this.setIndex(v,-1);
		}
		for (Map<Integer,SuccessorSet<Integer>> map:this.pointsToMap.values()) {
			map.clear();
		}
		this.verticesById.clear();
		
	}
	
	private void buildPointsToIndex(P2Graph graph,Component component, Collection<P2Vertex> verticesInComponent,Counter heapObjectId ,Counter varId,Collection<BridgeEdge> skipBridges) {
		Stack<P2Vertex> stack = new Stack<>();
		Map<P2Vertex, Integer> lowlinks = new HashMap<>();
		List<P2Vertex> vertices = new ArrayList<>();
		List<P2Vertex> tmp = new ArrayList<>();
		for (P2Vertex v:verticesInComponent) {
			if (v.getType()==P2Vertex.NAME) {
				// start reverse traversal here
				if (v.getOutEdges().isEmpty()) {
					vertices.add(v);
				}
				else {
					tmp.add(v);
				}
			}
		}
		vertices.addAll(tmp); // necessary for some SCCs
		
		for (P2Vertex v:vertices) {
			if (v.getType()==P2Vertex.NAME) {
				if (TRACE_MODE) LOGGER.trace("start new traversal at " + v.getName());
				assert stack.isEmpty();
				if (getIndex(v)==-1) visitAndAssignId(graph,v,stack,lowlinks,heapObjectId,varId,skipBridges);
			}			
		}
	}
	
	private void visitAndAssignId(P2Graph graph,P2Vertex v,Stack<P2Vertex> stack,Map<P2Vertex, Integer> lowlinks,Counter heapObjectId,Counter varId,Collection<BridgeEdge> skipBridges) {
		
		if (TRACE_MODE) LOGGER.trace("visiting vertex " + v.getName());
		int id = varId.incrementAndGet();
		setIndex(v,id);
		verticesById.put(id,v);
		lowlinks.put(v, id);
		stack.push(v);
		SuccessorSet<Integer> points2Set = successorSetFactory.createNew();
		Map<Integer,SuccessorSet<Integer>> map = this.pointsToMap.get(v.getComponent());
		map.put(id,points2Set);
		
		// next generation
		Collection<P2Vertex> nextGeneration = new ArrayList<>();
		for (P2Edge e:v.getInEdges()) {
			P2Vertex src = e.getStart();
			// check for unassigned vertices, see /CFLReachability/data/testdata-fieldsensitive9 for an example
			if (e.getType()==P2Edge.ASSIGN && src.getComponent()!=null) {
				// skip uncertified bridge edges 
				assert src.getComponent()==v.getComponent();
				boolean skip = e instanceof BridgeEdge && skipBridges.contains(e);
				if (!skip) nextGeneration.add(src);
			}
			else if (e.getType()==P2Edge.NEW) {
				assert src.getComponent()==v.getComponent();
				assert src.getType()==P2Vertex.HEAP_OBJECT;
				int _id = this.getIndex(src);
				if (_id==-1) {
					_id = heapObjectId.incrementAndGet();
					setIndex(src,_id);
				}
				verticesById.put(_id,src);
				points2Set.add(_id);
			}
		}
			
		for (P2Vertex next:nextGeneration) {
			if (getIndex(next)==-1) { // not yet visited
				visitAndAssignId(graph, next,stack,lowlinks,heapObjectId,varId,skipBridges);
				lowlinks.put(v, Math.min(lowlinks.get(v), lowlinks.get(next)));
			} else if (stack.contains(next)) { // visited, but on stack
				lowlinks.put(v,Math.min(lowlinks.get(v), getIndex(next)));
			} 
			
			// merge sets (propagate)
			SuccessorSet<Integer> tmp = map.get(getIndex(next));
			if (tmp==null) LOGGER.warn("Encountered visited vertex without successor set");
			else points2Set.merge(tmp);
		}

		// build new component
		if (lowlinks.get(v)==getIndex(v)) {
			if (TRACE_MODE) LOGGER.trace("building new SCC");
			//Set<P2Vertex> component = new HashSet<>();
			P2Vertex v2;
			do {
				v2 = stack.pop();
				if (TRACE_MODE) LOGGER.trace("  building SCC - adding " + v2.getName());
				// all elements in the SCC have the same (identical!) successor set (as the root component)
				map.put(getIndex(v2),points2Set);
			} while (v2!=v);
		}
		
	}
	
	public int getIndex(P2Vertex v) {
		return v.getSIndex();
	}
	
	public void setIndex(P2Vertex v,int index) {
		v.setSIndex(index);
	}

	@Override
	public void reset() {
		this.pointsToMap.clear();
		this.listeners.clear();
		this.verticesById.clear();
	}
	
}
