/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.transitiveclosure;

import it.uniroma3.mat.extendedset.intset.ConciseSet;
import it.uniroma3.mat.extendedset.intset.IntSet;
import java.util.*;

/**
 * Successor set based on CONCISE sets.
 * @author jens dietrich
 */
public class ConciseSuccessorSet implements SuccessorSet<Integer> {
	
	public ConciseSuccessorSet(boolean simulateWAH) {
		super();
		set = new ConciseSet(simulateWAH);
	}

	private ConciseSet set = null;

	@Override
	public Iterator<Integer> iterator() {
		final IntSet.IntIterator iter = set.iterator();
		return new Iterator<Integer>() {

			@Override
			public boolean hasNext() {
				return iter.hasNext();
			}

			@Override
			public Integer next() {
				return iter.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public void merge(SuccessorSet<Integer> sset) {
		// set = set.union(((ConciseSuccessorSet)sset).set);
		
		// avoid cloning
		set.addAll(((ConciseSuccessorSet)sset).set);
	}
	
	@Override
	public void add(Integer v) {
		set.add(v);
	}

	@Override
	public boolean contains(Integer v) {
		return set.contains(v);
	}

	@Override
	public int size() {
		return set.size();
	}

	@Override
	public boolean intersects(SuccessorSet<Integer> sset) {
		if (set.isEmpty()) return false;
		ConciseSet cset = ((ConciseSuccessorSet)sset).set;
		if (cset.isEmpty()) return false;
		return set.containsAny(cset);
	}
}
