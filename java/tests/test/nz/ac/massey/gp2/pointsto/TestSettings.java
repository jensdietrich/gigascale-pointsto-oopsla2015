/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */


package test.nz.ac.massey.gp2.pointsto;

import java.util.ArrayList;
import java.util.Collection;

import au.edu.sydney.cflr.WorklistPointsTo;
import nz.ac.massey.gp2.diffprop.DiffPropPointsTo;
import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;

/**
 * Settings which algorithms to test are defined here.
 * These settings can be used in parameterized unit tests.
 * @author jens dietrich
 */
public class TestSettings {

	
	public static AbstractPointsToReachability[] getEnginesToTest() {
		
		AbstractPointsToReachability[] algs = new AbstractPointsToReachability[3];
		
		
		algs[0] = new WorklistPointsTo();
		
		algs[1] = new DiffPropPointsTo();
		
		TCReachability tcReachability = new TCReachability();
		tcReachability.setRefiner(new BottomUpRefiner());
		tcReachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
		tcReachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
		algs[2] = tcReachability;
		
		return algs;
	}
	
	/**
	 * Return all engines to be tested in a format suitable for junit parameterized testing.
	 * @return the engines to be tested
	 */
	public static Collection<Object[]> testEngines() {
		Collection<Object[]> engines = new ArrayList<>();
		for (AbstractPointsToReachability engine:getEnginesToTest()) {
			engines.add(new Object[]{engine});
		}
		return engines;
	}
	
	/**
	 * Return all engines to be tested in a format suitable for junit parameterized testing.
	 * @param runsPerEngine how many test runs per engine to perform
	 * @return the engines to be tested
	 */
	public static Collection<Object[]> testEngines(int runsPerEngine) {
		Collection<Object[]> engines = new ArrayList<>();
		for (AbstractPointsToReachability engine:getEnginesToTest()) {
			for (int i=0;i<runsPerEngine;i++) {
				engines.add(new Object[]{engine});
			}
		}
		return engines;
	}
	
}
