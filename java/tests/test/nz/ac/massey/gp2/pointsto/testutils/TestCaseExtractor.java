/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package test.nz.ac.massey.gp2.pointsto.testutils;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import nz.ac.massey.gp2.pointsto.BridgeEdge;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.bidyck.BridgeVertex;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;


/**
 * Script that can be used to reduce graphs by removing vertices to a smaller graph that still has some 
 * desirable property.
 * This can be used to extract useful test cases from behaviour observed on benchmark data sets.
 * @author jens dietrich
 */
public abstract class TestCaseExtractor {
	
	
	protected P2Graph graph = null;
	protected Map<String,String> renamings = new HashMap<>();
	// counter used when simpler names are generated while exporting
	private int varCounter = 1;
	private int hobjCounter = 1;
	private int fieldCounter = 1;
	
	/**
	 * If true, in each iteration vertices that are in assign chains will be removed.
	 * I.e., if there is a chain v1 -&gt; v2 -&gt; v3, then v2 will be removed, and a new edge v1 -&gt; v3 will be created.
	 * The method canRemoveVertexInAssignChain(P2Vertex) can be used to control this further. 
	 * @return
	 */
	public boolean shortenAssignChains() {
		return true;
	}
	public boolean canRemoveVertexInAssignChain(P2Vertex v) {
		return true;
	}
	
	public void extractTest() throws Exception {
		this.readInitialGraph();
		P2Graph g = graph;
		P2Graph clone = null;
		int count = 0;
		int RETRY = 100;
		int iteration = 0;
		
		// precondition: accept the original graph ! 
		boolean preCond = false;
		while (!preCond && count<RETRY) {
			if (count>0) System.out.println("   refinment failed, retry " + count);
			clone = clone(g);
			preCond = this.accept(clone);
			count = count+1;
		}
		
		if (!preCond) {
			throw new IllegalStateException("accept fails on original graph (without modifications)");
		}
		count = 0;
		while (count<RETRY && g.getVertexCount()>100) {
			clone=mutateAndRemoveSomeRandomVertices(g,0.01);
			if (this.accept(clone)) {
				g=clone;  // accept might have altered clone, e.g. by adding bridge edges/vertices
				count=0;
				System.out.println("refined test, graph size is " + g.getVertexCount() + " , refinement "+(iteration++));
			}
			else {
				count=count+1;
				System.out.println("   refinment failed, retry " + count);
			}
		}
		
		while (count<RETRY) {
			clone=mutateAndRemoveRandomVertex(g);
			if (this.accept(clone)) {
				g=clone;  // accept might have altered clone, e.g. by adding bridge edges/vertices
				count=0;
				System.out.println("refined test (2), graph size is " + g.getVertexCount());
			}
			else {
				count=count+1;
			}
		}
		this.graph = g;
		exportGraph();
	}
	
	public P2Graph mutateAndRemoveRandomVertex(P2Graph g) {
		List<P2Vertex> vertexList = new ArrayList<>();
		vertexList.addAll(g.getVertices());
		return mutateAndRemoveVertex(g,vertexList.get(new Random().nextInt(g.getVertexCount())));
	}
	
	public P2Graph mutateAndRemoveSomeRandomVertices(P2Graph g,double percentage) {
		List<P2Vertex> vertexList = new ArrayList<>();
		vertexList.addAll(g.getVertices());
		int removalSize = (int) (g.getVertexCount()*percentage);
		if (removalSize==0) return g;
		Collection<P2Vertex> removalSet = new HashSet<>();
		
		Random random = new Random();
		while (removalSet.size()<removalSize) {
			removalSet.add(vertexList.get(random.nextInt(g.getVertexCount())));
		}
		
		return mutate(g,removalSet);
	}
	
	public P2Graph mutateAndRemoveVertex(P2Graph g,P2Vertex vertexToBeRemoved) {
		Collection<P2Vertex> verticesToBeRemoved = new HashSet<>();
		verticesToBeRemoved.add(vertexToBeRemoved);
		return mutate(g,verticesToBeRemoved);
	}
	

	/**
	 * Deep clone a graph.
	 * @param g
	 * @return
	 */
	public P2Graph clone(P2Graph g) {
		return mutate(g,Collections.<P2Vertex> emptySet());	
	}
	/**
	 * Clone a graph, remove vertices and edges from the clone and test the result.
	 * Return the clone if it passes the test, and null otherwise.
	 * @param g
	 * @param verticesToBeRemoved
	 * @return
	 */
	public P2Graph mutate(P2Graph g,Collection<P2Vertex> verticesToBeRemoved) {
		P2Graph clone = new P2Graph();
		Map<P2Vertex,P2Vertex> cloneMap = new HashMap<>();
		for (P2Vertex v:g.getVertices()) {
			assert !(v instanceof BridgeVertex); // graphs should not have been processed
			if (!verticesToBeRemoved.contains(v)) {
				P2Vertex vClone = new P2Vertex();
				vClone.setType(v.getType());
				vClone.setName(v.getName());
				clone.add(vClone);
				cloneMap.put(v,vClone);
			}
		}
		for (P2Vertex v:g.getVertices()) {
			assert !(v instanceof BridgeVertex); // graphs should not have been processed
			for (P2Edge e:v.getOutEdges()) {
				if (!(verticesToBeRemoved.contains(e.getStart()) || verticesToBeRemoved.contains(e.getEnd()))) {
					assert !(e instanceof BridgeEdge); // graphs should not have been processed				
					P2Edge eClone = null;
					if (e instanceof FieldAccessEdge) {
						FieldAccessEdge fae = (FieldAccessEdge)e;
						FieldAccessEdge cloneFae = new FieldAccessEdge(cloneMap.get(e.getStart()),cloneMap.get(e.getEnd()));	
						cloneFae.setField(fae.getField());
						eClone = cloneFae;
					}
					else {
						eClone = new P2Edge(cloneMap.get(e.getStart()),cloneMap.get(e.getEnd()));
					}
					eClone.setType(e.getType());
					cloneMap.get(e.getStart()).addOutEdge(eClone);
					cloneMap.get(e.getEnd()).addInEdge(eClone);		
				}
			}
		}
		
		if (this.shortenAssignChains()) {
			Set<P2Vertex> toBeRemoved = new HashSet<>();
			for (P2Vertex v:clone.getVertices()) {
				if (this.canRemoveVertexInAssignChain(v) && v.getInEdges().size()==1 && v.getOutEdges().size()==1) {
					P2Edge in = v.getInEdges().iterator().next();
					P2Edge out = v.getOutEdges().iterator().next();
					if (in.getType()==P2Edge.ASSIGN && out.getType()==P2Edge.ASSIGN) {
						P2Vertex predeseccor = in.getStart();
						P2Vertex successor = out.getEnd();
						if (!toBeRemoved.contains(predeseccor) && !toBeRemoved.contains(successor)) {
							predeseccor.removeOutEdge(in);
							successor.removeInEdge(out);
							v.removeInEdge(in);
							v.removeOutEdge(out);
							P2Edge shortcut = new P2Edge(predeseccor,successor);
							shortcut.setType(P2Edge.ASSIGN);
							predeseccor.addOutEdge(shortcut);
							successor.addInEdge(shortcut);
							toBeRemoved.add(v);
						}
					}
				}
			}
			for (P2Vertex v:toBeRemoved) {
				clone.remove(v);
			}
			System.out.println("Shortended assignment chains, " + toBeRemoved.size() + " vertices removed");
		}
		
		return clone;
		
	}
	
	/**
	 * Read the initial graph from the folder specified by getInputDataFolder(). 
	 * @throws Exception
	 */
	public void readInitialGraph() throws Exception {
		graph = new IncrementalGraphBuilder()
				.addAllocData(new File(getInputDataFolder() + "Alloc.csv"))
				.addAssignData(new File(getInputDataFolder() + "Assign.csv"))
				.addLoadData(new File(getInputDataFolder() + "Load.csv"))
				.addStoreData(new File(getInputDataFolder() + "Store.csv"))
				.getGraph();
		}

	/**
	 * Export the reduced graph to a folder specified by getOutputDataFolder(), using the same naming conventions for files used in readInitialGraph()
	 * (although no Cast file will be created, as Cast records are currently interpreted as Assign records).  
	 * @throws Exception 
	 */
	void exportGraph() throws Exception {
		System.out.println("writing graph with " + graph.getVertexCount() + " vertices");
		File dir = new File(this.getOutputDataFolder());
		if (!dir.exists()) {
			dir.mkdirs();
			System.out.println("Output folder created: " + dir.getAbsolutePath());
		}
		System.out.println("Reduced graph will be exported to: " + dir.getAbsolutePath());
		
		exportEdges(P2Edge.NEW,new File(this.getOutputDataFolder()+"Alloc.csv"),false);
		exportEdges(P2Edge.ASSIGN,new File(this.getOutputDataFolder()+"Assign.csv"),false);
		exportEdges(P2Edge.LOAD,new File(this.getOutputDataFolder()+"Load.csv"),false);
		exportEdges(P2Edge.STORE,new File(this.getOutputDataFolder()+"Store.csv"),false);
		
		System.out.println("Simplified graph will be exported to: " + getRenamedOutputDataFolder());
		exportEdges(P2Edge.NEW,new File(this.getRenamedOutputDataFolder()+"Alloc.csv"),true);
		exportEdges(P2Edge.ASSIGN,new File(this.getRenamedOutputDataFolder()+"Assign.csv"),true);
		exportEdges(P2Edge.LOAD,new File(this.getRenamedOutputDataFolder()+"Load.csv"),true);
		exportEdges(P2Edge.STORE,new File(this.getRenamedOutputDataFolder()+"Store.csv"),true);
		
		exportMappings(new File(this.getRenamedOutputDataFolder()+"renamed.csv"));
	}
	
	private void exportMappings(File file) throws Exception {
		PrintWriter out = new PrintWriter(new FileWriter(file));
		for (Map.Entry<String,String> e:this.renamings.entrySet()) {
			out.print("\"");
			out.print(e.getValue());
			out.print("\",\"");
			out.print(e.getKey());
			out.println("\"");
		}
		out.close();
	}

	private void exportEdges(byte type, File file,boolean rename) throws Exception {
		PrintWriter out = new PrintWriter(new FileWriter(file));
		for (P2Vertex v:graph.getVertices()) {
			for (P2Edge e:v.getOutEdges()) {
				if (e.getType()==type) {
					if (type==P2Edge.NEW) {
						// reverse order - stored are ALLOCs
						out.print("\"");
						out.print(renameVertex(e.getEnd(),rename));
						out.print("\",\"");
						out.print(renameVertex(e.getStart(),rename));
						out.print("\"");
					}
					else {
						out.print("\"");
						out.print(renameVertex(e.getStart(),rename));
						out.print("\",\"");
						out.print(renameVertex(e.getEnd(),rename));
						out.print("\"");
					}
					if (e instanceof FieldAccessEdge) {
						FieldAccessEdge fae = (FieldAccessEdge)e;
						out.print(",\"");
						out.print(renameField(fae.getField(),rename));
						out.print("\"");
					}
					out.println();
				}
			}
		}	
		out.close();
		System.out.println("Edges exported to " + file.getAbsolutePath());
	}
	
	protected String renameVertex(P2Vertex v,boolean on) {
		String name = v.getName();
		byte type = v.getType();
		return renameVertex(name,type,on);
	}
	
	protected String renameField(String name,boolean on) {
		if (on) {
			String name2 = this.renamings.get(name);
			if (name2==null) {
				name2 = "f" + (fieldCounter++);
				this.renamings.put(name,name2);
			}
			return name2;
		}
		else {
			return name;
		}
	}
	
	
	protected String renameVertex(String name,byte type,boolean on) {
		if (on) {
			String name2 = this.renamings.get(name);
			if (name2==null) {
				String core = type==P2Vertex.NAME?"v":"hobj";
				name2 = core + (type==P2Vertex.NAME?this.varCounter++:this.hobjCounter++);
				this.renamings.put(name,name2);
			}
			return name2;
		}
		else{
			return name;
		}
	}

	/**
	 * The input data folder.
	 * @return
	 */
	public abstract String getInputDataFolder();
	/**
	 * The output data folder.
	 * @return
	 */
	public abstract String getOutputDataFolder();
	
	/**
	 * The output data folder for the renamed graph.
	 * @return
	 */
	public String getRenamedOutputDataFolder() {
		File f = new File(getOutputDataFolder()+"renamed/");
		if (!f.exists()) f.mkdirs();
		String n = f.getAbsolutePath();
		if (!n.endsWith("/")) n = n+"/";
		return n;
	}
	
	
	/**
	 * Whether the modified (reduced) graph still has the desirable properties.
	 * @param graph
	 * @return
	 */
	public abstract boolean accept(P2Graph graph);

}
