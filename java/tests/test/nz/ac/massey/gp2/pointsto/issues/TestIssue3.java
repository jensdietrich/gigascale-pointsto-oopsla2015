package test.nz.ac.massey.gp2.pointsto.issues;


import static org.junit.Assert.*;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import nz.ac.massey.gp2.diffprop.DiffPropPointsTo;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;
import org.junit.Assume;
import org.junit.Test;
import au.edu.sydney.cflr.WorklistPointsTo;

/**
 * Test that describes the unsoundness of the DifferencePropagation described here:
 * Sridharan, Fink - The Complexity of Andersens Analysis in Practice. 
 * For details, see readme.txt in the /testdata/testdata-issue3/ folder. 
 * Precondition (Assume.assumeThat()): a points-to query succeeds with TCReachability.
 * Assertion: the same query must succeed with DiffPropPointsTo.
 * <p>
 * Testdata, annotated with how diffprop computes points-to:
 * <p>
 * <img src="{@docRoot}/testdata/testdata-issue3/graph-diffprop.png" alt="copy testdata folder into javadoc root to see generated images">
 * <p>
 * Testdata, annotated with how tc (our algorithm) computes points-to:
 * <p>
 * <img src="{@docRoot}/testdata/testdata-issue3/graph-tc.png" alt="copy testdata folder into javadoc root to see generated images">
 * @author jens dietrich
 */


public class TestIssue3 {
	
	private static final String TEST_DATA = "testdata/testdata-issue3/";
	private static String[] FN = {
			"v8",
			"hobj3"
	};
	
	private static P2Graph loadGraph() throws Exception {
		return new IncrementalGraphBuilder()
			.addAllocData(new File(TEST_DATA + "Alloc.csv"))
			.addAssignData(new File(TEST_DATA + "Assign.csv"))
			.addAssignData(new File(TEST_DATA + "Cast.csv"))
			.addLoadData(new File(TEST_DATA + "Load.csv"))
			.addStoreData(new File(TEST_DATA + "Store.csv"))
			.getGraph();
	}

	@Test
	public void test() throws Exception {
		
		// 1 USE MELSKI-REPS WORKLIST ALGORITHM
		WorklistPointsTo WL = new WorklistPointsTo();		
		P2Graph graph1 = loadGraph(); 
		WL.process(graph1);
		// create map to lookup vertices by name
		Map<String,P2Vertex> verticesByName1 = new HashMap<>();
		for (P2Vertex v:graph1.getVertices()) {
			verticesByName1.put(v.getName(),v);
		}	
		P2Vertex var1 =  verticesByName1.get(FN[0]);
		P2Vertex heapObj1 = verticesByName1.get(FN[1]);
		boolean resultWL = WL.pointsTo(var1, heapObj1);
		System.out.println("reachability result using Melski/Reps CFLR worklist algorithm: " + resultWL);
		
		// 2 USE OUR TC ALGORITHM 
		TCReachability TC = new TCReachability();
		BottomUpRefiner refiner = new BottomUpRefiner();
		TC.setRefiner(refiner);
		TC.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
		TC.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
		P2Graph graph2 = loadGraph(); 
		TC.process(graph2);
		// create map to lookup vertices by name
		Map<String,P2Vertex> verticesByName2 = new HashMap<>();
		for (P2Vertex v:graph2.getVertices()) {
			verticesByName2.put(v.getName(),v);
		}	
		P2Vertex var2 =  verticesByName2.get(FN[0]);
		P2Vertex heapObj2 = verticesByName2.get(FN[1]);
		boolean resultTC = TC.pointsTo(var2, heapObj2);
		System.out.println("reachability result using TC algorithm: " + resultTC);
		
		
		// 3 USE OUR SRIDHARAN/FINK's DIFFERENCE PROPAGATION ALGORITHM 	
		DiffPropPointsTo DP = new DiffPropPointsTo();
		P2Graph graph3 = loadGraph();
		DP.process(graph3);
		Map<String,P2Vertex> verticesByName3 = new HashMap<>();
		for (P2Vertex v:graph3.getVertices()) {
			verticesByName3.put(v.getName(),v);
		}
		P2Vertex var3 =  verticesByName3.get(FN[0]);
		P2Vertex heapObj3 = verticesByName3.get(FN[1]);
		boolean resultDP = DP.pointsTo(var3, heapObj3);
		System.out.println("reachability result using Sridharan/Fink difference propagation algorithm: " + resultDP);
		
		// run test
		Assume.assumeTrue(resultWL);
		Assume.assumeTrue(resultTC);
		assertTrue(resultDP);
	}

}
