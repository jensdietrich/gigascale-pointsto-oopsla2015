/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package test.nz.ac.massey.gp2.pointsto.testutils;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collection;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.io.TestOracleLoader;
import nz.ac.massey.gp2.pointsto.refinement.TopDownRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;

/**
 * Extract a test case from a benchmark demonstrating that top down refinement 
 * can lead to false positives (over-approximation).
 * Try to minimise the size of the graph tested to make the test comprehensible. 
 * @author jens dietrich
 */
public class FPWithTopDownRefinerTestCaseExtractor extends TestCaseExtractor {

	private Multimap<String,String> oracleMap = null;
	
	// used to store the false positive that was used to evaluate the last mutation
	// [varName,heapObjName]
	private String[] FP = null;
	
	
	public static void main(String[] args) throws Exception {
		new FPWithTopDownRefinerTestCaseExtractor().extractTest();
	}
	@Override
	public String getInputDataFolder() {
		return "benchmarkdata/dacapo/bloat-def/";
	}

	@Override
	public String getOutputDataFolder() {
		return "benchmarkdata/generatedtests/bloat-tmp/";
	}
	
	public void readInitialGraph() throws Exception {
		super.readInitialGraph();
		Collection<P2Vertex[]> reachabilityOracles = TestOracleLoader.loadTestOracles(graph,getInputDataFolder()+"VarPointsTo.csv");
		oracleMap = HashMultimap.<String,String>create();
		for (P2Vertex[] oracle:reachabilityOracles) {
			P2Vertex heapObject = oracle[1];
			P2Vertex var = oracle[0];
			if (var!=null && heapObject!=null) oracleMap.put(var.getName(), heapObject.getName());
		}
	}
	

	@Override
	public boolean accept(P2Graph g) {
		TCReachability reachability = new TCReachability();
		TopDownRefiner tdRefiner = new TopDownRefiner();
		tdRefiner.setMaxRefinements(Integer.MAX_VALUE);
		reachability.setRefiner(tdRefiner);
		reachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
		reachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
		
		P2Graph clone = clone(g);// clone - reachability modifies graph !!
		reachability.process(clone); 
		
		for (P2Vertex var:clone.getVertices()) {
			if (var.getType()==P2Vertex.NAME && var.getComponent()!=null) {
				Collection<P2Vertex> reachable = reachability.getPointsToSet(var);
				for (P2Vertex hObj:reachable) {
					if (!oracleMap.get(var.getName()).contains(hObj.getName())) {
						// false positive detected ! 
						this.FP = new String[]{var.getName(),hObj.getName()};
						return true;
					}
				}
			}
		}
 		return false;	
	}
	
	/**
	 * Export the graph. 
	 * Override this function to also export the false positive record.
	 */
	@Override
	void exportGraph() throws Exception {
		
		super.exportGraph();
		
		File file = new File(this.getOutputDataFolder()+"FP.csv");
		PrintWriter out = new PrintWriter(new FileWriter(file));
		out.print("\"");
		out.print(this.renameVertex(FP[0],P2Vertex.NAME,false));
		out.print("\",\"");
		out.print(this.renameVertex(FP[1],P2Vertex.HEAP_OBJECT,false));
		out.println("\"");
		out.close();
		System.out.println("False positive exported to " + file.getAbsolutePath());
		
		file = new File(this.getRenamedOutputDataFolder()+"FP.csv");
		out = new PrintWriter(new FileWriter(file));
		out.print("\"");
		out.print(this.renameVertex(FP[0],P2Vertex.NAME,true));
		out.print("\",\"");
		out.print(this.renameVertex(FP[1],P2Vertex.HEAP_OBJECT,true));
		out.println("\"");
		out.close();
		System.out.println("Renamed false positive exported to " + file.getAbsolutePath());

	}


}
