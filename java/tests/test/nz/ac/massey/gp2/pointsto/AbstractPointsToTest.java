/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package test.nz.ac.massey.gp2.pointsto;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;

import org.junit.After;
import org.junit.Before;

/**
 * Abstract points-to test.
 * @author jens dietrich
 */

public abstract class AbstractPointsToTest extends AbstractTest {
	
	protected AbstractPointsToReachability reachability = null;
	
		
	public AbstractPointsToTest(AbstractPointsToReachability reachability) {
		super();
		this.reachability = reachability;
	}

	@Before
	public void setup() throws Exception {
		super.setup();
		
		// configure and compute reachability
//		TCReachability tcReachability = new TCReachability();
//		tcReachability.setRefiner(new BottomUpRefiner());
//		tcReachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
//		tcReachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
//		reachability = tcReachability;
		
//		reachability = new DiffPropPointsTo();
		
		reachability.process(graph);
		
		if (dumpInfo()) {
			dumpIndex();
			dumpComponents();
		}
	}
	
	@After
	public void tearDown() throws Exception {
		reachability.reset();
	}
	
	protected boolean dumpInfo() {
		return false;
	}
	
	protected P2Edge findEdge(P2Vertex from,P2Vertex to, String field, byte type) {
		for (P2Edge e:from.getOutEdges()) {
			if  (e.getType()==type && ((FieldAccessEdge)e).getField().equals(field) && e.getEnd()==to)  {
				System.out.println("edge found: " + e);
				return e;
			}
		}
		return null;
	}
	
	
	protected void testPointsTo(String v, String[] reachableHeapObjects) {
		P2Vertex var = getVertex(v);
		Set<P2Vertex> oracle = new HashSet<>();
		
		for (String v2:reachableHeapObjects) {
			oracle.add(getVertex(v2));
		}
		
		for (P2Vertex hobj:oracle) {
			assertTrue(""+var.getName()+" cannot reach " + hobj.getName(),reachability.pointsTo(var, hobj));
		}
		
		assertEquals(oracle.size(),reachability.getPointsToSet(var).size());		
		
	}
	
	protected void testPointsTo(String vName, String hobjName) {
		P2Vertex var = getVertex(vName);
		P2Vertex hobj = getVertex(hobjName);
		assertTrue(""+var.getName()+" cannot reach " + hobj.getName(),reachability.pointsTo(var, hobj));
	}
	
	protected void testPointsNotTo(String vName, String hobjName) {
		P2Vertex var = getVertex(vName);
		P2Vertex hobj = getVertex(hobjName);
		assertFalse(""+var.getName()+" can reach " + hobj.getName(),reachability.pointsTo(var, hobj));
	}
	
	protected void testNotIndexed (String v) {
		Set<P2Vertex> pointsTo = reachability.getPointsToSet(this.getVertex(v));
		// changed: some engines might produce an index that just associates unmapped 
		// variables with an empty pointsTo set
		assertTrue(pointsTo==null || pointsTo.isEmpty());
	}
	
	// also test completeness - i.e. whether this are all the vertices within the component
	protected void testSameComponent(String... memberNames) {
		P2Vertex v = this.getVertex(memberNames[0]);
		Component comp = v.getComponent();
		for (String name:memberNames) {
			v = this.getVertex(name);
			Component comp2 = v.getComponent();
			assertTrue(comp==comp2);
		}
	}
	
		
	protected void dumpIndex () {
		System.out.println();
		System.out.println("Reachability Index");
		for (P2Vertex v:graph.getVertices()) {
			try {
				dump(v,reachability.getPointsToSet(v));
			}
			catch (Exception x) {
				System.err.println("Error dumping reachability index");
				x.printStackTrace();
			}
		}
		System.out.println();
	}
	
	protected void dumpComponents () {
		System.out.println();
		System.out.println("Components");
		for (P2Vertex v:this.graph.getVertices()) {
			Component comp = v.getComponent();
			System.out.print("  ");
			System.out.print(v.getName());
			System.out.print(" -> ");
			System.out.println(comp==null?"null":comp);
		}
		System.out.println();
	}
	
	protected void dump(P2Vertex v,Set<P2Vertex> reachable) {
		
		System.out.print("  ");
		System.out.print(v.getName());
		
		if (reachable==null) {
			System.out.println("null");
		}
		else {
			System.out.print(" -> [");
			boolean first = true;
			for (P2Vertex v2:reachable) {
				if (first) first = false;
				else System.out.print(",");
				System.out.print(v2.getName());
			}
			System.out.println("]");
		}
		
	}

}
