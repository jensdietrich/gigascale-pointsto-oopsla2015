/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package test.nz.ac.massey.gp2.pointsto.testutils;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import nz.ac.massey.gp2.diffprop.DiffPropPointsTo;
import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator; 
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;

/**
 * Extract a test case from a benchmark demonstrating that an algorithm produces false negatives.
 * Try to minimise the size of the graph tested to make the test comprehensible. 
 * @author jens dietrich
 */

public class FNTestCaseExtractor extends TestCaseExtractor {

	
	// the false negative to look for [varName,heapObjName]
	private String[] FN = {
			"<org.apache.openejb.core.ivm.naming.NameNode: boolean hasChildren(org.apache.openejb.core.ivm.naming.NameNode)>/$r6",
			"org.apache.openejb.core.ivm.naming.NameNode.<init>/new org.apache.openejb.core.ivm.naming.NameNode/0"
	};
	
	@Override
	public boolean canRemoveVertexInAssignChain(P2Vertex v) {
		return !v.getName().equals(FN[0]);
	}
	
	@Override
	public String getInputDataFolder() {
		return "benchmarkdata/generatedtests/tradebeans/";
	}

	@Override
	public String getOutputDataFolder() {
		return "benchmarkdata/generatedtests/tradebeans2/";
	}
	
	public AbstractPointsToReachability getPreciseAlgorithm() {
		//		TCReachability reachability = new TCReachability();
		//		BottomUpRefiner refiner = new BottomUpRefiner();
		//		reachability.setRefiner(refiner);
		//		reachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
		//		reachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
		//		return reachability;
		// return new WorklistPointsTo();
		return new DiffPropPointsTo();
		

	}
	
	public AbstractPointsToReachability getImpreciseAlgorithm() {
		// return new DiffPropPointsTo();
		TCReachability reachability = new TCReachability();
		BottomUpRefiner refiner = new BottomUpRefiner();
		reachability.setRefiner(refiner);
		reachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
		reachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
		return reachability;
	}

	
	
	
	public static void main(String[] args) throws Exception {
		new FNTestCaseExtractor().extractTest();
	}

	
	

	@Override
	public boolean accept(P2Graph g) {
		
		// System.out.println("Trying with graph of size " + g.getVertexCount());
		
		AbstractPointsToReachability preciseReachability = getPreciseAlgorithm();
		P2Graph clone1 = clone(g); // clone - reachability modifies graph !!
		preciseReachability.process(clone1);
		
		AbstractPointsToReachability reachabilityWithFN = getImpreciseAlgorithm();
		P2Graph clone2 = clone(g);// clone - reachability modifies graph !!
		reachabilityWithFN.process(clone2);
		
		// create map to lookup vertices by name
		Map<String,P2Vertex> verticesByName1 = new HashMap<>();
		for (P2Vertex v:clone1.getVertices()) {
			verticesByName1.put(v.getName(),v);
		}
		Map<String,P2Vertex> verticesByName2 = new HashMap<>();
		for (P2Vertex v:clone2.getVertices()) {
			verticesByName2.put(v.getName(),v);
		}
		
		P2Vertex var1 =  verticesByName1.get(FN[0]);
		P2Vertex heapObj1 = verticesByName1.get(FN[1]);
		
		P2Vertex var2 =  verticesByName2.get(FN[0]);
		P2Vertex heapObj2 = verticesByName2.get(FN[1]);
		try {
			boolean successPrecise = preciseReachability.pointsTo(var1, heapObj1);
			boolean successInprecise = reachabilityWithFN.pointsTo(var2, heapObj2);
			return successPrecise && !successInprecise;// && !successInprecise;
		}
		catch (Exception x) {
			x.printStackTrace();
		}
		
		return false;
		
	}
	
	/**
	 * Export the graph. 
	 * Override this function to also export the false positive record.
	 */
	@Override
	void exportGraph() throws Exception {
		
		super.exportGraph();
		
		File file = new File(this.getOutputDataFolder()+"FN.csv");
		PrintWriter out = new PrintWriter(new FileWriter(file));
		out.print("\"");
		out.print(this.renameVertex(FN[0],P2Vertex.NAME,false));
		out.print("\",\"");
		out.print(this.renameVertex(FN[1],P2Vertex.HEAP_OBJECT,false));
		out.println("\"");
		out.close();
		System.out.println("False negatives exported to " + file.getAbsolutePath());
		
		file = new File(this.getRenamedOutputDataFolder()+"FN.csv");
		out = new PrintWriter(new FileWriter(file));
		out.print("\"");
		out.print(this.renameVertex(FN[0],P2Vertex.NAME,true));
		out.print("\",\"");
		out.print(this.renameVertex(FN[1],P2Vertex.HEAP_OBJECT,true));
		out.println("\"");
		out.close();
		System.out.println("Renamed false negative exported to " + file.getAbsolutePath());

	}




}
