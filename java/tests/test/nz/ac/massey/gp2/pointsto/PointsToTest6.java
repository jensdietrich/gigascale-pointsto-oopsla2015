/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package test.nz.ac.massey.gp2.pointsto;

import java.util.Collection;

import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * PointsTo test. Input data:
 * <img src="{@docRoot}/testdata/testdata-fieldsensitive6/testdata.png" alt="copy testdata folder into javadoc root to see generated images">
 * @author jens dietrich
 */

@RunWith(Parameterized.class)
public class PointsToTest6 extends AbstractPointsToTest{

	@Parameters
    public static Collection<Object[]> data() {
    	return TestSettings.testEngines();
    }
	
	public PointsToTest6(AbstractPointsToReachability reachability) {
		super(reachability);
	}

	@Override
	protected String getTestDataFolder() {
		return "testdata/testdata-fieldsensitive6/";
	}
	
	@Test
	public void testPointsTo1() throws Exception {	
		testPointsTo("v1",new String[]{"hobj"});
	}
	
	@Test
	public void testPointsTo2() throws Exception {	
		testPointsTo("v2",new String[]{"hobj"});
	}
	
	@Test
	public void testPointsTo3() throws Exception {	
		testPointsTo("v3",new String[]{"hobj"});
	}
	
	@Test
	public void testPointsTo4() throws Exception {	
		testPointsTo("v4",new String[]{"hobj"});
	}
	
	@Test
	public void testPointsTo5() throws Exception {	
		testPointsTo("v5",new String[]{"hobj"});
	}

}
