/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package test.nz.ac.massey.gp2.pointsto.testutils;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;

/**
 * Simple utility to convert graphs to dot files. These dot files can then be processed to pdf/png by graphviz, omnigraffl etc to 
 * document tests.
 * @author jens dietrich
 */
public class Graph2Dot {

	//public static final String TEST_DATA_FOLDER = "data/testdata-fieldsensitive7/";
	public static final String TEST_DATA_FOLDER = "testdata/testdata-fieldsensitive13/";
	
	public static void main(String[] args) throws Exception {
		P2Graph graph = new IncrementalGraphBuilder()
				.addAllocData(new File(TEST_DATA_FOLDER + "Alloc.csv"))
				.addAssignData(new File(TEST_DATA_FOLDER + "Assign.csv"))
				.addLoadData(new File(TEST_DATA_FOLDER + "Load.csv"))
				.addStoreData(new File(TEST_DATA_FOLDER + "Store.csv"))
				.getGraph();
		
		File dot = new File(TEST_DATA_FOLDER + "graph.dot");
		PrintWriter out = new PrintWriter(new FileWriter(dot));
		
		out.println("digraph graphname {");		
		out.println("   commentnode [fillcolor=lightyellow shape=box label=\"GRAPH GENERATED FROM TEST DATA\\ndata:  "+TEST_DATA_FOLDER+"\\ngreen edges - NEW\\nblack edges - ASSIGN\\nred egdes - STORE\\nblue edges - LOAD\"]");	
		for (P2Vertex v:graph.getVertices()) {
			for (P2Edge e:v.getOutEdges()) {
				if (e.getStart().getType()==P2Vertex.HEAP_OBJECT) {
					out.print("  ");
					out.print(e.getStart().getName());
					out.println(" [fillcolor=green]");
				}
				out.print("  ");
				out.print(e.getStart().getName());
				out.print(" -> ");
				out.print(e.getEnd().getName());
				out.print(" [");
				if (e.getType()==P2Edge.NEW) out.print("color=darkgreen fontcolor=darkgreen");
				else if (e.getType()==P2Edge.ASSIGN) out.print("color=black fontcolor=black");
				else if (e.getType()==P2Edge.STORE) out.print("label=store_" + ((FieldAccessEdge)e).getField() + " color=red fontcolor=red");
				else if (e.getType()==P2Edge.LOAD) out.print("label=load_" + ((FieldAccessEdge)e).getField() + " color=blue fontcolor=blue");
				out.println("];");
			}
		}
		out.println("}");
		
		out.close();
		
		System.out.println("Dot file written to " + dot.getAbsolutePath());
	}

}
