/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package test.nz.ac.massey.gp2.pointsto.issues;

import static org.junit.Assert.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;
import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import au.edu.sydney.cflr.WorklistPointsTo;


/**
 * Test that describes the unsoundness of the DifferencePropagation described here:
 * Sridharan, Fink - The Complexity of Andersen’s Analysis in Practice. 
 * For details, see readme.txt in the /testdata/testdata-issue4/ folder.  * Precondition: a points-to query succeeds with TCReachability.
 * Assertion: the same query must succeed with DiffPropPointsTo.
 * @author jens dietrich
 */

@RunWith(Parameterized.class)
public class TestIssue4 {
	
	private static final String TEST_DATA = "testdata/testdata-issue4/";
	private static String[] FN = {
			"<org.apache.openejb.core.ivm.naming.NameNode: boolean hasChildren(org.apache.openejb.core.ivm.naming.NameNode)>/$r6",
			"org.apache.openejb.core.ivm.naming.NameNode.<init>/new org.apache.openejb.core.ivm.naming.NameNode/0"
	};

	@Parameters
    public static Collection<Object[]> data() {
    	Collection<Object[]> coll = new ArrayList<>();
    	for (int i=0;i<20;i++) {
    		coll.add(new Object[]{});
    	}
    	return coll;
    }
    
	private static P2Graph loadGraph() throws Exception {
		return new IncrementalGraphBuilder()
			.addAllocData(new File(TEST_DATA + "Alloc.csv"))
			.addAssignData(new File(TEST_DATA + "Assign.csv"))
			.addAssignData(new File(TEST_DATA + "Cast.csv"))
			.addLoadData(new File(TEST_DATA + "Load.csv"))
			.addStoreData(new File(TEST_DATA + "Store.csv"))
			.getGraph();
	}

	@Test
	public void test() throws Exception {
		WorklistPointsTo preciseReachability = new WorklistPointsTo();
		P2Graph graph1 = loadGraph(); 
		System.out.println("Loaded graph1, vertex count: " + graph1.getVertexCount());
		preciseReachability.process(graph1);
		
		// create map to lookup vertices by name
		Map<String,P2Vertex> verticesByName1 = new HashMap<>();
		for (P2Vertex v:graph1.getVertices()) {
			verticesByName1.put(v.getName(),v);
		}
		
		P2Vertex var1 =  verticesByName1.get(FN[0]);
		P2Vertex heapObj1 = verticesByName1.get(FN[1]);
		boolean successPrecise = preciseReachability.pointsTo(var1, heapObj1);
		
		P2Graph graph2 = loadGraph();
		System.out.println("Loaded graph2, vertex count: " + graph2.getVertexCount());
		TCReachability reachabilityWithFN = new TCReachability();
		BottomUpRefiner refiner = new BottomUpRefiner();
		reachabilityWithFN.setRefiner(refiner);
		reachabilityWithFN.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
		reachabilityWithFN.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
		reachabilityWithFN.process(graph2);
		Map<String,P2Vertex> verticesByName2 = new HashMap<>();
		for (P2Vertex v:graph2.getVertices()) {
			verticesByName2.put(v.getName(),v);
		}
		P2Vertex var2 =  verticesByName2.get(FN[0]);
		P2Vertex heapObj2 = verticesByName2.get(FN[1]);
		boolean successInprecise = reachabilityWithFN.pointsTo(var2, heapObj2);
		
		System.out.println("reachability result using sound reachability: " + successPrecise);
		Assume.assumeTrue(successPrecise);
		assertTrue(successInprecise);
	}

}
