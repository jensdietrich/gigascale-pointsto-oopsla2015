/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import nz.ac.massey.gp2.util.LogSystem;
import org.apache.log4j.Logger;

/**
 * Utility to pick dataset to run a benchmark.
 * TODO: implement runtime parameter checks here. 
 * @author jens dietrich
 */
public class InitExperiment {
	private static Logger LOGGER = LogSystem.getLogger(InitExperiment.class);
	public static final String ROOT = "benchmarkdata";
	public static String getDataSet(String experimentName,String[] args) throws Exception {
		if (args.length>0) {
			LOGGER.info("run experiment with " + args[0]);
			String dataset = args[0];
			if (!dataset.endsWith("/")) dataset = dataset+"/";
			boolean valid = isValidDataSet(dataset);
			LOGGER.info("file is valid data set: " + valid);
			if (valid) {
				return dataset;
			}
		}

		
		if (!java.awt.GraphicsEnvironment.isHeadless()) {
			List<String> dataSets = new ArrayList<String>();
			String root = new File(ROOT).getAbsolutePath();
			for (File f:new File(ROOT).listFiles()) {
				if (isDataSet(f)) {
					dataSets.add(f.getAbsolutePath().replace(root,ROOT)+"/");
				}
				else if (f.isDirectory()) {
					// 2 -deep only (jython,dacapo)
					for (File f2:f.listFiles()) {
						if (isDataSet(f2)) {
							dataSets.add(f2.getAbsolutePath().replace(root,ROOT)+"/");
						}
					}
				}
			}
			
			Object[] options = dataSets.toArray();
			Object value = JOptionPane.showInputDialog(null, 
			                                           "Select Dataset", 
			                                            experimentName, 
			                                            JOptionPane.QUESTION_MESSAGE, 
			                                            null,
			                                            options, 
			                                            options[0]);
	
			int index = dataSets.indexOf(value);
			
			if (index==-1) System.exit(0);
			return dataSets.get(index);
		}
		else {
			throw new IllegalArgumentException ("Cannot find data set to analyse, start program with dataset parameter, example: benchmarkdata/dacapo2009/avrora/ (the path is relative to the folder containing this script)");
		}
	} 
	
	
	private static boolean isDataSet(File f) {
		// LOGGER.info("checking whether this is a valid dataset folder: " + f.getAbsolutePath());
		if (f.isDirectory()) {
			if (f.getName().contains("test")) return false;
			return 
					new File(f,"Alloc.csv").exists() &&
					new File(f,"Assign.csv").exists() &&
					new File(f,"Load.csv").exists() &&
					new File(f,"Store.csv").exists(); // &&
					//new File(f,"Alias.csv").exists() &&
					//new File(f,"VarPointsTo.csv").exists();
		}
		return false;
	}

	private static boolean isValidDataSet(String dataSetRelPath) {
		return isDataSet(new File(dataSetRelPath));
	}
}
