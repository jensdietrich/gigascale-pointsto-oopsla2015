/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks.util;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;

/**
 * Script to print the tree starting from a heap object vertex.
 * @author jens dietrich
 */
public class PrintTree {
	
	public static final String DATA_FOLDER = "data/openjdk/";
	public static final String START_VERTEX_NAME = "HALFWIDTH_KATAKANA";
	public static final String TARGET_VERTEX_NAME = "java.lang.Character$Subset.<init>(Ljava/lang/String;)V|@name";
	public static final int MAX_DEPTH = 4;
	public static final boolean REVERSE_EDGES = false;
	public static final String OUTPUT = DATA_FOLDER+"_tree.dot";
	
	public static void main (String[] args) throws Exception {
		P2Graph graph = new IncrementalGraphBuilder()
			.addAllocData(new File(DATA_FOLDER + "Alloc.csv"))
			.addAssignData(new File(DATA_FOLDER + "Assign.csv"))
			.addAssignData(new File(DATA_FOLDER + "Cast.csv"))
			.addLoadData(new File(DATA_FOLDER + "Load.csv"))
			.addStoreData(new File(DATA_FOLDER + "Store.csv"))
			.getGraph();
		
		// reverse assigns
		if (REVERSE_EDGES) {
			Collection<P2Edge> assigns = new HashSet<>();
			for (P2Vertex v:graph.getVertices()) {
				for (P2Edge e:v.getOutEdges()) {
					if (e.getType()==P2Edge.ASSIGN) {
						assigns.add(e);
					}
				}
			}
			for (P2Edge e:assigns) {
				P2Edge e2 = new P2Edge(e.getEnd(),e.getStart());
				e2.setType(P2Edge.ASSIGN);
				e.getEnd().addOutEdge(e2);
				e.getStart().addOutEdge(e2);
			}
		}
		
		P2Vertex v0 = null;
		for (P2Vertex v:graph.getVertices()) {
			if (v.getName().equals(START_VERTEX_NAME)) {
				v0=v;
				break;
			}
		}
		
		PrintWriter out = new PrintWriter(new FileWriter(OUTPUT));
		Map<P2Vertex,String> index = new HashMap<>();
		Set<P2Edge> visited = new HashSet<>();
		out.println("digraph graphname {");
		visit(v0,visited,index,0,out);
		out.println("}");
		out.close();
		
	}

	private static void visit(P2Vertex v, Set<P2Edge> visited,Map<P2Vertex, String> index, int depth,PrintWriter out) {
		if (depth>MAX_DEPTH) {
			System.err.println("MAX_DEPTH reached, stop here");
			return;
		}
		
		if (index.get(v)==null) {
			String id = v.getType()==P2Vertex.HEAP_OBJECT?"hobj":"v";
			id = id+index.size();
			index.put(v, id);
			
			System.out.println(""+id + "  " + v.getName());
			
			// highlight special nodes
			if (v.getName().equals(START_VERTEX_NAME) || v.getName().equals(TARGET_VERTEX_NAME)) {
				out.print("   ");
				out.print(id);
				out.println(" [fillcolor=lightyellow]");
			}
		}
		
		Collection<P2Edge> edges = new HashSet<>();
		edges.addAll(v.getOutEdges());
		
		for (P2Edge e:edges) {
			if (visited.add(e)) {
				P2Vertex end = e.getEnd();
				visit(end,visited,index,depth+1,out);		
				if (index.get(e.getEnd())!=null) {
					out.print("   ");
					out.print(index.get(e.getStart()));
					out.print("->");
					out.print(index.get(e.getEnd()));
					out.print("");
					if (e.getType()==P2Edge.NEW) out.print(" [label=new]");
					else if (e.getType()==P2Edge.LOAD) out.print(" [label=load_" + getFieldName(e)+"]");
					else if (e.getType()==P2Edge.STORE) out.print(" [label=store_" + getFieldName(e)+"]");
					out.println(";");
				}
			}
		}
	}
	private static String getFieldName(P2Edge e) {
		FieldAccessEdge fae =(FieldAccessEdge)e;
		String name = fae.getField();
		if (name.startsWith("org.python.core")) {
			name = name.substring("org.python.core".length());
		}
		return name;
	}
}
