package nz.ac.massey.gp2.pointsto.benchmarks;

import java.io.File;
import java.util.ArrayList;

import nz.ac.massey.gp2.diffprop.DiffPropPointsTo;
import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.benchmarks.util.InitExperiment;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.pointsto.refinement.TopDownRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;
import nz.ac.massey.gp2.util.LogSystem;

import org.apache.log4j.Logger;

import au.edu.sydney.cflr.WorklistPointsTo;

public class MeasureAll {
	
	private static Logger LOGGER = LogSystem.getLogger(MeasureAll.class);
	public static final int MAX_ORACLE_COUNT = 1_000_000; //100_000_000;
	public static final int PROGRESS_LOG_STEP_SIZE = 2_000_000;
	
	public static void main(String[] args) throws Exception{
		
		boolean graphSize = false;
		boolean oracleSize = false;
		boolean falsePositives = false;
		boolean falseNegatives = false;
		boolean singleSource = false;
		boolean dumpPointsTo = false;
		
		//it is an error not to provide Any arguments
		if(args.length < 1){
			usage();
			System.exit(0);
		}
		
		//process command line args
		ArrayList<String> benchmarkPaths = new ArrayList<>();
		ArrayList<AbstractPointsToReachability> reachabilityAlgs = new ArrayList<>();
		int i = 0;
		while(i < args.length){
			switch(args[i]){
				case "-h" :
					usage();
					System.exit(0);
				case "-gs" :
					graphSize = true;
					break;
				case "-os" :
					oracleSize = true;
					break;
				case "-fp" :
					falsePositives = true;
					break;
				case "-fn" :
					falseNegatives = true;
					break;
				case "-ss" :
					singleSource = true;
					break;
				case "-wl" :
					reachabilityAlgs.add(new WorklistPointsTo());
					break;
				case "-dp" :
					reachabilityAlgs.add(new DiffPropPointsTo());
					break;
				case "-bu" :
					TCReachability reachability = new TCReachability();
					reachability.setRefiner(new BottomUpRefiner());
					reachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
					reachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
					reachabilityAlgs.add(reachability);
					break;
				case "-td" :
					TCReachability tdAlg = new TCReachability();
					TopDownRefiner tdRefiner = new TopDownRefiner();
					
					tdAlg.setRefiner(tdRefiner);
					tdAlg.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
					tdAlg.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
					
					try{
						tdRefiner.setMaxRefinements(Integer.parseInt(args[i+1]));
						i++;
					} catch (Exception e){
						tdRefiner.setMaxRefinements(Integer.MAX_VALUE);
					}
					reachabilityAlgs.add(tdAlg);		
					break;
				case "-dump" :
					dumpPointsTo = true;
					break;
				default :
					if(new File(args[i]).isDirectory()) benchmarkPaths.add(args[i]);
					else{
						System.err.println("Error: benchmark path \"" + args[i] + "\" is invalid");
						System.exit(1);
					}
					break;
			}
			i++;
		}
		
		//Ensure there is at least one benchmark
		if(benchmarkPaths.size() < 1){
			benchmarkPaths.add(InitExperiment.getDataSet("All Measurement Experiments",args));
		}
		
		//Run the tests
		for(String dataset : benchmarkPaths){
			if(graphSize){
				LOGGER.info("TESTING Graph Size ON " + dataset);
				MeasureGraphSize.run(dataset, true);
			}
			if(oracleSize){
				LOGGER.info("TESTING Oracle Size ON " + dataset);
				MeasureSridharanBridgeOracleSize.run(dataset);
			}
			for(AbstractPointsToReachability abs : reachabilityAlgs){
				LOGGER.info("TESTING " + abs.getClass().getSimpleName() + " ON " + dataset);
				P2Graph graph = MeasureGraphSize.run(dataset, false);
				abs.process(graph);
				if(falsePositives) MeasureFalsePositives.run(abs, dataset);
				if(falseNegatives) MeasureFalseNegatives.run(abs, dataset);
				if(singleSource){
					MeasureSingleSource.run(abs, dumpPointsTo ? System.out : null);
					dumpPointsTo = false;
				}
				System.err.println();
			}
		}
		
	}
	
	public static void usage(){
		System.out.println("Usage:");
		System.out.println("    java MeasureAll [opts] [benchmark(s)]");
		System.out.println("");
		System.out.println("benchmarks:  0 or more paths to a benchmark folder");
		System.out.println("opts:");
		System.out.println("    -gs        Enable graph-size test");
		System.out.println("    -os        Count the sizes of the different potential oracles");
		System.out.println("    -fp        Enable false-positive test");
		System.out.println("    -fn        Enable false-negative test");
		System.out.println("    -ss        Enable single-source query time test");
		System.out.println("    -wl        Use worklist solver");
		System.out.println("    -dp        Use deference-propagation solver");
		System.out.println("    -bu        Use bottom-up refinement");
		System.out.println("    -td [num]  Use top-down refinement with [num] steps");
		System.out.println("    -dump      Dump the first generated points-to to stdout (use with -ss)");
	}
	
}
