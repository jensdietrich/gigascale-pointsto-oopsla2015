/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks.util;

import java.util.*;
import java.io.*;


/**
 * Utility to filter input files from the large jdk dataset datasets to facilitate debugging.
 * @author jens dietrich
 */
public class FilterInputFile {

	public static void main(String[] args) throws Exception {
		filter("data/openjdktmp/Assign.csv","data/openjdktmp/Assign-reduced.csv");
		filter("data/openjdktmp/Alloc.csv","data/openjdktmp/Alloc-reduced.csv");
	}
	
	public static void filter(String inputFile,String outputFile) throws Exception {
		List<String> lines = new ArrayList<>();
		int counter = 0;
		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		String line = null;
		while ((line=reader.readLine())!=null) {
			counter=counter+1;
			if (accept(line)) {
				lines.add(line);
			}
		}
		reader.close();
		
		PrintWriter out = new PrintWriter(new FileWriter(outputFile));
		for (String l:lines) {
			out.println(l);
		}
		out.close();
		
//		System.out.println("Line filter applied to " + inputFile + ", line count: " + counter + " -> " + lines.size());
//		System.out.println("Results written to " + outputFile);
	}
	
	
	private static boolean accept(String line) {
		return ! (
				line.contains("com.oracle.net.Sdp.<clinit>") ||
				line.contains("beans") ||
				line.contains("nio") ||
				line.contains("rmi") ||
				line.contains("sql") ||
				line.contains("corba") ||
				line.contains("CORBA") ||
				line.contains("xml") ||
				line.contains("XML") ||
				line.contains("security") || 
				line.contains("math") || 
				line.contains("swing") ||
				line.contains("com.sun.beans") ||
				line.contains("com.sun.image") ||
				line.contains("apache") ||
				line.contains("URL") ||
				line.contains("concurrent") ||
				line.contains("thread") ||
				line.contains("Class") || 
				line.contains("accessibility") ||
				line.contains("activation") ||
				line.contains("java.util") ||
				line.contains("jmx") || 
				line.contains("jndi") ||
				line.contains("reflect") || 
				line.contains("invoke") || 
				line.contains("management") ||
				line.contains("www") || 
				line.contains("io") || 
				line.contains("media") ||
				line.contains("print") ||
				line.contains("Socket") ||
				line.contains("naming") ||
				line.contains("model")
		);
	}

}
