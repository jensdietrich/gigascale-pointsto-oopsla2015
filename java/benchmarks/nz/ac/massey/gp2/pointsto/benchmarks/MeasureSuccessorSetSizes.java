/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks;

import java.io.File;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.benchmarks.util.InitExperiment;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;
import nz.ac.massey.gp2.util.LogSystem;

/**
 * Script that can be used to measure the size of the successor sets.
 * Output is on the console in CSV format, to facilitate copying results into
 * spreadsheets to analyse them.
 * Run with -Xmx4g -Xss32m  for openjdk dataset.
 * @author jens dietrich
 */
public class MeasureSuccessorSetSizes {
	
	private static Logger LOGGER = LogSystem.getLogger(MeasureSuccessorSetSizes.class);


	public static void main(String[] args) throws Exception {
		
		String experimentName = "Measure successor set sizes, using bidyck bridge oracle/concise successor sets/bottom-up-refinement";
		String dataset = InitExperiment.getDataSet(experimentName,args);
		
		P2Graph graph  = new IncrementalGraphBuilder()
				.addAllocData(new File(dataset + "Alloc.csv"))
				.addAssignData(new File(dataset + "Assign.csv"))
				.addAssignData(new File(dataset + "Cast.csv"))
				.addLoadData(new File(dataset + "Load.csv"))
				.addStoreData(new File(dataset + "Store.csv"))
				.getGraph();
		
		TCReachability reachability = new TCReachability();
		reachability.setRefiner(new BottomUpRefiner());
		reachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
		reachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
		reachability.process(graph);

		LOGGER.info("Measuring size of successor sets");
		


		int THRESHOLD = 10;
		
		// 1 measure size of successor sets
		int small = 0;
		int large = 0;
		for (P2Vertex var:graph.getVertices()) {
			if (var.getType()==P2Vertex.NAME) {
				Set<P2Vertex> pointsTo = reachability.getPointsToSet(var);
				int s = pointsTo==null?0:pointsTo.size();
				if (s>THRESHOLD) large = large+1;
				else small = small+1;
			}
		}
		// print CSV header
		System.out.println("program,small points to sets (threshold is " + THRESHOLD + "),small unique poinst to sets (threshold is " + THRESHOLD + ")");
		
		// print results as CSV
		System.out.print(new File(dataset).getName());
		System.out.print(",");
		System.out.print(getPercentage(small,large));
		System.out.print(",");
		
		// 2 measure size of sets (different, as sets in SCCs are only counted once)
		small = 0;
		large = 0;
		Set<Set<P2Vertex>> pointTos = new HashSet<>();
		for (P2Vertex var:graph.getVertices()) {
			if (var.getType()==P2Vertex.NAME) {
				Set<P2Vertex> pointsTo = reachability.getPointsToSet(var);
				if (pointsTo!=null && pointTos.add(pointsTo)) {
					int s = pointsTo==null?0:pointsTo.size();
					if (s>THRESHOLD) large = large+1;
					else small = small+1;
				}
			}
		}
		System.out.print(getPercentage(small,large));
		System.out.println();
		
	}

	private static String getPercentage(int small,int large) {
		double s1 = small;
		double t1 = small+large;
		// System.out.println("  crude value is " + (100*s1/t1));
		return new DecimalFormat("0.00").format(100*s1/t1);
	}

}
