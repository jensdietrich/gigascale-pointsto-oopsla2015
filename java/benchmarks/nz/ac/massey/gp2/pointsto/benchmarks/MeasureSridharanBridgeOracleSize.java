/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.benchmarks.util.InitExperiment;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.util.LogSystem;

/**
 * Measure size of bridge oracles using the strategy from the Sridharan et al OOSPLA'05 paper.
 * Run with -Xmx4g -Xss32m for openjdk data set. 
 * @author jens dietrich
 */
public class MeasureSridharanBridgeOracleSize {
		
	private static Logger LOGGER = LogSystem.getLogger(MeasureSridharanBridgeOracleSize.class);
	
	public static void main(String[] args) throws Exception {
		
		String experimentName = "Measure size of bridge oracles using the same-field strategy from the Sridharan et al paper";
		String dataset = InitExperiment.getDataSet(experimentName,args);
		
		LOGGER.info("Running experiment \"" + experimentName + "\" with dataset " + dataset);
		run(dataset);
	}	
	
	public static void run(String dataset) throws Exception{
		P2Graph graph  = new IncrementalGraphBuilder()
				.addAllocData(new File(dataset + "Alloc.csv"))
				.addAssignData(new File(dataset + "Assign.csv"))
				.addAssignData(new File(dataset + "Cast.csv"))
				.addLoadData(new File(dataset + "Load.csv"))
				.addStoreData(new File(dataset + "Store.csv"))
				.getGraph();
				
		int totalLoadDests = 0;
		Map<String,Integer> loads = new HashMap<>();
		for (P2Vertex v:graph.getVertices()) {
			for(P2Edge e:v.getInEdges()){
				if(e.getType() ==P2Edge.LOAD){
					totalLoadDests++;
					break;
				}
			}
			for (P2Edge e:v.getOutEdges()) {
				if (e.getType()==P2Edge.LOAD) {
					FieldAccessEdge ae = (FieldAccessEdge)e;
					String f = ae.getField();
					Integer c = loads.get(f);
					if (c==null) {
						c=1;
					}
					else {
						c=c+1;
					}
					loads.put(f, c);
				}
			}
		}
		
		long sf = 0;
		long totalStores = 0;
		
		for (P2Vertex v:graph.getVertices()) {
			for (P2Edge e:v.getOutEdges()) {
				boolean counted = false;
				if (e.getType()==P2Edge.STORE) {
					if(!counted){
						totalStores++;
						counted = true;
					}
					FieldAccessEdge ae = (FieldAccessEdge)e;
					String f = ae.getField();
					// find matching loads, algorithm will create match / bridge edges for each pair
					Integer c = loads.get(f);
					if (c!=null) {
						sf += c;
					}
				}
			}
		}
		
		LOGGER.info("Total-loads: " + totalLoadDests);
		LOGGER.info("Total-stores: " + totalStores);
		LOGGER.info("All-pairs: " + totalLoadDests * totalStores);
		LOGGER.info("Same-field: " + sf);

	}

}
