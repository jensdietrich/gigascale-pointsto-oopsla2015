/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks;

import java.io.File;
import java.util.Collection;

import org.apache.log4j.Logger;

import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.benchmarks.util.InitExperiment;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.pointsto.io.TestOracleLoader;
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Measure the over-approximation (false positives, and therefore precision) produced by 
 * TCReachability with the following plugins:
 * (1) nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator
 * (2) nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner
 * (3) nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1
 * This cannot be done with the openjdk dataset as this requires all oracles to be loaded into memory !
 * @author jens dietrich
 */
public class MeasurePointsToWithBottomUpRefinementFalsePositives {
		
	private static Logger LOGGER = LogSystem.getLogger(MeasurePointsToWithBottomUpRefinementFalsePositives.class);
	public static final int MAX_ORACLE_COUNT = 1_000_000; //100_000_000;
	
	public static void main(String[] args) throws Exception {
		
		String experimentName = "Measure false positives (precision), using bidyck bridge oracle/concise successor sets/bottom-up-refinement";
		String dataset = InitExperiment.getDataSet(experimentName,args);
		LOGGER.info("Running experiment \"" + experimentName + "\" with dataset " + dataset);
		
		P2Graph graph  = new IncrementalGraphBuilder()
				.addAllocData(new File(dataset + "Alloc.csv"))
				.addAssignData(new File(dataset + "Assign.csv"))
				.addAssignData(new File(dataset + "Cast.csv"))
				.addLoadData(new File(dataset + "Load.csv"))
				.addStoreData(new File(dataset + "Store.csv"))
				.getGraph();
		
		Collection<P2Vertex[]> reachabilityOracles = TestOracleLoader.loadTestOracles(graph,dataset+"VarPointsTo.csv",MAX_ORACLE_COUNT);
		
		TCReachability reachability = new TCReachability();
		reachability.setRefiner(new BottomUpRefiner());
		reachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
		reachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
		reachability.process(graph);

		int falsePositives = 0;
		int truePositives = 0;
		
		// put oracles in map
		Multimap<P2Vertex,P2Vertex> oracleMap = HashMultimap.<P2Vertex,P2Vertex>create();
		for (P2Vertex[] oracle:reachabilityOracles) {
			P2Vertex heapObject = oracle[1];
			P2Vertex var = oracle[0];
			oracleMap.put(var, heapObject);
		}
		
		// measure false positives
		long t1 = System.nanoTime();
		LOGGER.info("Testing for false positives");
		for (P2Vertex var:graph.getVertices()) {
			if (var.getType()==P2Vertex.NAME && var.getComponent()!=null) {
				Collection<P2Vertex> reachable = reachability.getPointsToSet(var);
				for (P2Vertex hObj:reachable) {
					if (!oracleMap.get(var).contains(hObj)) {
						falsePositives = falsePositives + 1;
					}
					else {
						truePositives = truePositives+1;
					}
				}
			}
		}

			
		long t2 = System.nanoTime();
		LOGGER.info("Tested for false positives, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
		LOGGER.info("true positives: " + truePositives);
		LOGGER.info("false positives: " + falsePositives);
		
		
		double TP = (double)truePositives;
		double FP = (double)falsePositives;
		
		LOGGER.info("precision: " + TP/(TP+FP));
		
		System.out.println();
		
	}

}
