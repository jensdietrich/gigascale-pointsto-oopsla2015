/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import nz.ac.massey.gp2.pointsto.FieldAccessEdge;
import nz.ac.massey.gp2.pointsto.P2Edge;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

/**
 * Script that can be used to measure the size of graphs.
 * Run with -Xmx3g (for openjdk)  
 * @author jens dietrich
 */
public class MeasureGraphSize {

	private static Logger LOGGER = LogSystem.getLogger(MeasureGraphSize.class);

	public static void main(String[] args) throws Exception {
		MeasureAll.main(ArrayUtils.addAll(args, "-gs"));
	}

	public static P2Graph run(String dataset, boolean verbose) throws Exception{

		long t1 = System.nanoTime();
		P2Graph graph  = new IncrementalGraphBuilder()
		.addAllocData(new File(dataset + "Alloc.csv"))
		.addAssignData(new File(dataset + "Assign.csv"))
		.addAssignData(new File(dataset + "Cast.csv"))
		.addLoadData(new File(dataset + "Load.csv"))
		.addStoreData(new File(dataset + "Store.csv"))
		.getGraph();
		
		if(!verbose){
			return graph;
		}
		
		long t2 = System.nanoTime();
		LOGGER.info("Read in the graph, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());

		int hobjCounter = 0;
		int varCounter = 0;
		int allocCounter = 0;
		int assignCounter = 0;
		int loadCounter = 0;
		int storeCounter = 0;
		int maxLoads = 0;
		int maxStores = 0;

		byte type = 0;
		
		Map<String,Integer> loadsByField = new HashMap<>();
		Map<String,Integer> storesByField = new HashMap<>();

		for (P2Vertex v:graph.getVertices()) {
			type = v.getType();
			if (type==P2Vertex.HEAP_OBJECT) hobjCounter=hobjCounter+1;
			else if (type==P2Vertex.NAME) varCounter=varCounter+1;
			else throw new IllegalStateException();

			for (P2Edge e:v.getOutEdges()) {
				type = e.getType();
				if (type==P2Edge.ASSIGN) assignCounter = assignCounter+1;
				else if (type==P2Edge.NEW) allocCounter = allocCounter+1;
				else if (type==P2Edge.LOAD){
					loadCounter = loadCounter+1;
					String field = ((FieldAccessEdge)e).getField();
					int curLoads = loadsByField.containsKey(field) ? loadsByField.get(field) + 1 : 1;
					maxLoads = Math.max(maxLoads, curLoads);
					loadsByField.put(field,curLoads);
				}
				else if (type==P2Edge.STORE){
					storeCounter = storeCounter+1;
					String field = ((FieldAccessEdge)e).getField();
					int curStores = storesByField.containsKey(field) ? storesByField.get(field) + 1 : 1;
					maxStores = Math.max(maxStores, curStores);
					storesByField.put(field,curStores);
				}
			}
		}

		LOGGER.info("hobjCounter :" + hobjCounter);
		LOGGER.info("varCounter :" + varCounter);
		LOGGER.info("|V| :" + (varCounter+hobjCounter));
		LOGGER.info("allocCounter :" + allocCounter);
		LOGGER.info("assignCounter :" + assignCounter);
		LOGGER.info("loadCounter :" + loadCounter);
		LOGGER.info("storeCounter :" + storeCounter);
		LOGGER.info("|E| :" + (allocCounter+assignCounter+loadCounter+storeCounter));
		LOGGER.info("Load Bound  : " + maxLoads);
		LOGGER.info("Store Bound : " + maxStores);
		// print latex line
		String SEP = " & ";
		System.err.println(dataset + SEP + (varCounter+hobjCounter) + SEP + allocCounter + SEP + assignCounter + SEP + loadCounter + SEP + storeCounter + SEP + (allocCounter+assignCounter+loadCounter+storeCounter));
		System.err.println();

		return graph;
	}

}
