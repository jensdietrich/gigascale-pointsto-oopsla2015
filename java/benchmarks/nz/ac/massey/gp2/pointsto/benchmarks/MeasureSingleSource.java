package nz.ac.massey.gp2.pointsto.benchmarks;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;
import nz.ac.massey.gp2.diffprop.DiffPropPointsTo;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

public class MeasureSingleSource {

	private static Logger LOGGER = LogSystem.getLogger(MeasureSingleSource.class);
	
	public static void main(String[] args) throws Exception{
		MeasureAll.main(ArrayUtils.addAll(args, "-td", "20", "-bu", "-ss"));
	}
	
	public static void run(AbstractPointsToReachability reachability, PrintStream dumpOS) throws Exception{
		
		P2Graph graph = reachability.getGraph();
		// collect all var vertices
		Collection<P2Vertex> vars = new ArrayList<>();
		for (P2Vertex v:graph.getVertices()) {
			if (v.getType()==P2Vertex.NAME && (v.getComponent()!=null || reachability instanceof DiffPropPointsTo)) vars.add(v);
		}
		
		// measure query performance
		LOGGER.info("Testing for point-to query performance");
		long t1 = System.nanoTime();
		long totalPointsToSetSizes = 0;
		int maxPointsToSetSizes = 0;
		Set<P2Vertex> pts = null;
		int iterCounter = 0;
		for (P2Vertex var:vars) {
			if (iterCounter%100_000==0) {
				LOGGER.info("Query process " + iterCounter + "/" + vars.size() + " , PointsTo records found: " + totalPointsToSetSizes);
			}
			pts = reachability.getPointsToSet(var);
			if(dumpOS != null) for(P2Vertex end : pts) dumpOS.println("\"" + var.getName() + "\",\"" + end.getName() + "\"");
			int s = pts.size();
			totalPointsToSetSizes = totalPointsToSetSizes+s;
			if (s>maxPointsToSetSizes) maxPointsToSetSizes=s;
			iterCounter = iterCounter+1;
		}

			
		long t2 = System.nanoTime();
		LOGGER.info("Tested " + vars.size() + " points-to queries, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		LOGGER.info("Total points-to set size: " + (totalPointsToSetSizes));
		LOGGER.info("Avg points-to set size: " + ((double)totalPointsToSetSizes)/((double)vars.size()));
		LOGGER.info("Max points-to set size: " + maxPointsToSetSizes);	
	}
	
}
