/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.apache.log4j.Logger;

import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.TCReachability;
import nz.ac.massey.gp2.pointsto.benchmarks.util.InitExperiment;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner;
import nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

/**
 * Script to measure single source all sinks points to set query performance
 * of TCReachability with the following plugins:
 * (1) nz.ac.massey.gp2.pointsto.bidyck.BiDyckBridgeOracleGenerator
 * (2) nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner
 * (3) nz.ac.massey.gp2.transitiveclosure.ConciseSuccessorSetFactory1
 * Run with -Xmx4g -Xss32m  for openjdk
 * TODO: check whether this can be reduced, investigate the impact of other options such as -server .
 * @author jens dietrich
 */
public class MeasurePointsToWithBottomUpRefinementSingleSourceQueryPerformance {
	
private static Logger LOGGER = LogSystem.getLogger(MeasurePointsToWithBottomUpRefinementSingleSourceQueryPerformance.class);

public static void main(String[] args) throws Exception {
	
	
	String experimentName = "Measure single source all sink query performance, using bidyck bridge oracle/concise successor sets/bottom-up-refinement";
	String dataset = InitExperiment.getDataSet(experimentName,args);
	
	LOGGER.info("Running experiment \"" + experimentName + "\" with dataset " + dataset);
	
	P2Graph graph  = new IncrementalGraphBuilder()
			.addAllocData(new File(dataset + "Alloc.csv"))
			.addAssignData(new File(dataset + "Assign.csv"))
			.addAssignData(new File(dataset + "Cast.csv"))
			.addLoadData(new File(dataset + "Load.csv"))
			.addStoreData(new File(dataset + "Store.csv"))
			.getGraph();
		
	TCReachability reachability = new TCReachability();
	reachability.setRefiner(new BottomUpRefiner());
	reachability.setSuccessorSetFactory(new ConciseSuccessorSetFactory1());
	reachability.setBridgeOracleGenerator(new BiDyckBridgeOracleGenerator());
	reachability.process(graph);

	// collect all var vertices
	Collection<P2Vertex> vars = new ArrayList<>();
	for (P2Vertex v:graph.getVertices()) {
		if (v.getType()==P2Vertex.NAME && v.getComponent()!=null) vars.add(v);
	}
	
	// measure query performance
	LOGGER.info("Testing for point-to query performance");
	long t1 = System.nanoTime();
	long totalPointsToSetSizes = 0;
	int maxPointsToSetSizes = 0;
	Set<P2Vertex> pts = null;
	int iterCounter = 0;
	for (P2Vertex var:vars) {
		if (iterCounter%100_000==0) {
			LOGGER.info("Query process " + iterCounter + "/" + vars.size() + " , PointsTo records found: " + totalPointsToSetSizes);
		}
		pts = reachability.getPointsToSet(var);
		int s = pts.size();
		totalPointsToSetSizes = totalPointsToSetSizes+s;
		if (s>maxPointsToSetSizes) maxPointsToSetSizes=s;
		iterCounter = iterCounter+1;
	}

		
	long t2 = System.nanoTime();
	LOGGER.info("Tested " + vars.size() + " points-to queries, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
	LOGGER.info("Total points-to set size: " + (totalPointsToSetSizes));
	LOGGER.info("Avg points-to set size: " + ((double)totalPointsToSetSizes)/((double)vars.size()));
	LOGGER.info("Max points-to set size: " + maxPointsToSetSizes);	
}

}
