/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.benchmarks;

import java.io.File;
import java.util.Collection;

import org.apache.log4j.Logger;

import nz.ac.massey.gp2.diffprop.DiffPropPointsTo;
import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.benchmarks.util.InitExperiment;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.pointsto.io.TestOracleLoader;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

/**
 * Script that can be used to find false negatives produced by
 * the difference propagation algorithm.
 * This script is also used to measure single-source/single-sink query performance.
 * Run with -Xmx4g -Xss32m  for openjdk dataset.
 * @author jens dietrich
 */
public class MeasurePointsToWithDiffPropFalseNegatives {
	
	public static final int MAX_ORACLE_COUNT = 10_000_000; //100_000_000;
	
	private static Logger LOGGER = LogSystem.getLogger(MeasurePointsToWithDiffPropFalseNegatives.class);
	
	public static final int PROGRESS_LOG_STEP_SIZE = 10_000_000;

	public static void main(String[] args) throws Exception {
		
		Thread.sleep(3000);
		
		String experimentName = "Measure false negatives (completeness), using bidyck bridge oracle/concise successor sets/bottom-up-refinement";
		String dataset = InitExperiment.getDataSet(experimentName,args);
		LOGGER.info("Running experiment \"" + experimentName + "\" with dataset " + dataset);
		
		P2Graph graph  = new IncrementalGraphBuilder()
				.addAllocData(new File(dataset + "Alloc.csv"))
				.addAssignData(new File(dataset + "Assign.csv"))
				.addAssignData(new File(dataset + "Cast.csv"))
				.addLoadData(new File(dataset + "Load.csv"))
				.addStoreData(new File(dataset + "Store.csv"))
				.getGraph();
		
		Collection<P2Vertex[]> reachabilityOracles = TestOracleLoader.loadTestOracles(graph,dataset+"VarPointsTo.csv",MAX_ORACLE_COUNT);
	
		AbstractPointsToReachability reachability = new DiffPropPointsTo();
		
		long t1 = System.nanoTime();
		reachability.process(graph);
		long t2 = System.nanoTime();

		int falseNegatives = 0;
		int crashed = 0;
				
		int counter = 0;
		t1 = System.nanoTime();
		LOGGER.info("Testing for false negatives");
		
		for (P2Vertex[] oracle:reachabilityOracles) {
			counter = counter+1;
			if (counter%PROGRESS_LOG_STEP_SIZE==0) System.out.println("Measuring approximation (FN): " + counter + " / " + MAX_ORACLE_COUNT + ", FN=" + falseNegatives );
			P2Vertex heapObject = oracle[1];
			P2Vertex var = oracle[0];
			try {
				if (!reachability.pointsTo(var, heapObject)) {
					LOGGER.warn("False negative found: ");
					LOGGER.warn("   var: 		" + var);
					LOGGER.warn("   heapObject: " + heapObject);
					falseNegatives = falseNegatives+1;
				}
			}
			catch (Exception x) {
				crashed = crashed+1;
				falseNegatives = falseNegatives+1;
			}
		} 
		t2 = System.nanoTime();
		LOGGER.info("Tested for false negatives, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
		LOGGER.info("oracles investigated: " + reachabilityOracles.size());
		LOGGER.info("false negatives: " + falseNegatives);
		LOGGER.info("reachability crashed: " + crashed);

		System.out.println();
		
	}


}
