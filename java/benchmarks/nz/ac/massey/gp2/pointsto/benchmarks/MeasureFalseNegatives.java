package nz.ac.massey.gp2.pointsto.benchmarks;

import java.util.Collection;

import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.io.TestOracleLoader;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

public class MeasureFalseNegatives {
	
	private static Logger LOGGER = LogSystem.getLogger(MeasureFalseNegatives.class);
	public static final int PROGRESS_LOG_STEP_SIZE = MeasureAll.PROGRESS_LOG_STEP_SIZE;
	public static final int MAX_ORACLE_COUNT = MeasureAll.MAX_ORACLE_COUNT; //100_000_000;

	public static void main(String[] args) throws Exception {
		MeasureAll.main(ArrayUtils.addAll(args, "-td", "20", "-bu", "-fn"));
	}
	
	/**
	 * @param apt The reachability index to query
	 * @param dataset The path to the dataset being run
	 * @throws Exception
	 */
	public static void run(AbstractPointsToReachability apt, String dataset) throws Exception{
		
		P2Graph graph = apt.getGraph();
		Collection<P2Vertex[]> reachabilityOracles = TestOracleLoader.loadTestOracles(graph,dataset+"VarPointsTo.csv",MAX_ORACLE_COUNT);

		int falseNegatives = 0;
		int crashed = 0;
				
		int counter = 0;
		long t1 = System.nanoTime();
		LOGGER.info("Testing for false negatives");
		
		for (P2Vertex[] oracle:reachabilityOracles) {
			counter = counter+1;
			if (counter%PROGRESS_LOG_STEP_SIZE==0) LOGGER.info("Measuring approximation (FN): " + counter + " / " + MAX_ORACLE_COUNT + ", FN=" + falseNegatives );
			P2Vertex heapObject = oracle[1];
			P2Vertex var = oracle[0];
			try {
				if (!apt.pointsTo(var, heapObject)) {
					LOGGER.warn("False negative found: ");
					LOGGER.warn("   var: 		" + var);
					LOGGER.warn("   heapObject: " + heapObject);
					LOGGER.warn("   var comp: 		" + var.getComponent());
					LOGGER.warn("   heapObject comp:" + heapObject.getComponent());
					falseNegatives = falseNegatives+1;
				}
			}
			catch (Exception x) {
				crashed = crashed+1;
				falseNegatives = falseNegatives+1;
			}
		} 
		long t2 = System.nanoTime();
		LOGGER.info("Tested for false negatives, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
		LOGGER.info("oracles investigated: " + reachabilityOracles.size());
		LOGGER.info("false negatives: " + falseNegatives);
		LOGGER.info("reachability crashed: " + crashed);
	}
}
