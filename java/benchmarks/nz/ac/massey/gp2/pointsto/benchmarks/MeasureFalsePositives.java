package nz.ac.massey.gp2.pointsto.benchmarks;

import java.util.Collection;

import nz.ac.massey.gp2.pointsto.AbstractPointsToReachability;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.io.TestOracleLoader;
import nz.ac.massey.gp2.util.LogSystem;
import nz.ac.massey.gp2.util.MeasurementUtils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class MeasureFalsePositives {
	
	private static Logger LOGGER = LogSystem.getLogger(MeasureFalsePositives.class);
	public static final int MAX_ORACLE_COUNT = MeasureAll.MAX_ORACLE_COUNT;
	
	public static void main(String[] args) throws Exception{
		MeasureAll.main(ArrayUtils.addAll(args, "-td", "20", "-bu", "-fp"));
	}
	
	public static void run(AbstractPointsToReachability apt, String dataset) throws Exception{
		
		P2Graph graph = apt.getGraph();
		Collection<P2Vertex[]> reachabilityOracles = TestOracleLoader.loadTestOracles(graph,dataset+"VarPointsTo.csv",MAX_ORACLE_COUNT);

		int falsePositives = 0;
		int truePositives = 0;
		
		// put oracles in map
		Multimap<P2Vertex,P2Vertex> oracleMap = HashMultimap.<P2Vertex,P2Vertex>create();
		for (P2Vertex[] oracle:reachabilityOracles) {
			P2Vertex heapObject = oracle[1];
			P2Vertex var = oracle[0];
			oracleMap.put(var, heapObject);
		}
		
		// measure false positives
		long t1 = System.nanoTime();
		LOGGER.info("Testing for false positives");
		for (P2Vertex var:graph.getVertices()) {
			if (var.getType()==P2Vertex.NAME && var.getComponent()!=null) {
				Collection<P2Vertex> reachable = apt.getPointsToSet(var);
				for (P2Vertex hObj:reachable) {
					if (!oracleMap.get(var).contains(hObj)) {
						falsePositives = falsePositives + 1;
					}
					else {
						truePositives = truePositives+1;
					}
				}
			}
		}

			
		long t2 = System.nanoTime();
		LOGGER.info("Tested for false positives, this took " + MeasurementUtils.formatNanos(t1,t2) + ", memory usage is " + MeasurementUtils.measureAndFormatMem());
		
		LOGGER.info("true positives: " + truePositives);
		LOGGER.info("false positives: " + falsePositives);
		
		
		double TP = (double)truePositives;
		double FP = (double)falsePositives;
		
		LOGGER.info("precision: " + TP/(TP+FP));
		
	}
	
}
