/* 
 *  The Universal Permissive License (UPL) 
 *  Copyright (c) 2014 Jens Dietrich
 * 
 *  Subject to the condition set forth below, permission is hereby granted, free 
 *  of charge and under any and all patent and copyright rights owned or freely 
 *  licensable by each licensor hereunder, whether an original author or 
 *  another licensor, to any person obtaining a copy of this software, associated 
 *  documentation and/or data (collectively the "Software"), to deal in current 
 *  and future versions of both 
 *  (a) the Software, and 
 *  (b) any piece of software and/or hardware listed in the 
 *  LARGER_WORKS.TXT file included with the Software (each a “Larger Work”
 *  to which the Software is contributed by such licensors), 
 *  without restriction, including without limitation the rights to make, use, sell, 
 *  offer for sale, import, export, have made, have sold, copy, create derivative 
 *  works of, display, perform, distribute, and sublicense the Software and the 
 *  Larger Work(s) on either these or other terms. 
 *  This license is subject to the following condition: 
 *  The above copyright notice and either this complete permission notice or at 
 *  a minimum a reference to the UPL shall be included in all copies or 
 *  substantial portions of the Software.
 *   
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 *  OTHER DEALINGS IN THE SOFTWARE.
 */

package nz.ac.massey.gp2.pointsto.spikes;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import nz.ac.massey.gp2.pointsto.Component;
import nz.ac.massey.gp2.pointsto.P2Graph;
import nz.ac.massey.gp2.pointsto.P2Vertex;
import nz.ac.massey.gp2.pointsto.benchmarks.util.InitExperiment;
import nz.ac.massey.gp2.pointsto.bidyck.BiDyckComponent;
import nz.ac.massey.gp2.pointsto.bidyck.WCCComponentizer;
import nz.ac.massey.gp2.pointsto.io.IncrementalGraphBuilder;
import nz.ac.massey.gp2.util.LogSystem;

/**
 * This is a spike to measure the size of the component graph.
 * here we are interested to run componentisation first, and the to build a graph that
 * has these components as vertices. Finally, we are interested in teh size of the weakly connected components
 * (of components) in this graph. 
 * Run with -Xmx4g -Xss32m  for openjdk dataset.
 * @author jens dietrich
 */
public class MeasureMetaComponents {
	
	
	private static Logger LOGGER = LogSystem.getLogger(MeasureMetaComponents.class);
	
	public static final int PROGRESS_LOG_STEP_SIZE = 10_000_000;

	public static void main(String[] args) throws Exception {
		
		Thread.sleep(3000);
		
		String experimentName = "Measure MeataComponent Size";
		String dataset = InitExperiment.getDataSet(experimentName,args);
		LOGGER.info("Running experiment \"" + experimentName + "\" with dataset " + dataset);
		
		
		P2Graph graph  = new IncrementalGraphBuilder()
				.addAllocData(new File(dataset + "Alloc.csv"))
				.addAssignData(new File(dataset + "Assign.csv"))
				.addAssignData(new File(dataset + "Cast.csv"))
				.addLoadData(new File(dataset + "Load.csv"))
				.addStoreData(new File(dataset + "Store.csv"))
				.getGraph();
		
		WCCComponentizer builder1 = new WCCComponentizer();
		builder1.process(graph);
		
		DirectedGraph<Component,String> componentGraph = new DirectedSparseGraph<>();
		
		for (Component component:graph.getComponents()) {
			componentGraph.addVertex(component);
		}
		
		int c=0;
		for (Component component:graph.getComponents()) {
			BiDyckComponent bdComponent = (BiDyckComponent)component;
			for (Component next:bdComponent.getComponentsReachableByLoads()) {
				componentGraph.addEdge("LOAD"+(c++), component, next);
			}
			for (Component next:bdComponent.getComponentsReachableByReversedStores()) {
				componentGraph.addEdge("STORE"+(c++),next,component);
			}
		}
		
		WeakComponentClusterer<Component,String> clusterer = new WeakComponentClusterer<>();
		Set<Set<Component>> wccs = clusterer.transform(componentGraph);
		
		LOGGER.info("vertex count: " + graph.getVertexCount());
		LOGGER.info("component graph vertex count: " + componentGraph.getVertexCount());
		LOGGER.info("component graph edge count: " + componentGraph.getEdgeCount());
		LOGGER.info("component graph wccs count: " + wccs.size());
		
		int maxSize = 0;
		for (Set<Component> wcc:wccs) {
			maxSize = Math.max(maxSize,wcc.size());
		}
		LOGGER.info("component graph wccs max size (components): " + maxSize);

		// count vertices in components within wccs 
		Map<Component,Integer> componentSizes = new HashMap<>();
		for (Component component:graph.getComponents()) {
			componentSizes.put(component,0);
		}
		for (P2Vertex v:graph.getVertices()) {
			if (v.getComponent()!=null) {
				int size = componentSizes.get(v.getComponent());
				componentSizes.put(v.getComponent(),size+1);
			}
		}
		maxSize = 0;
		for (Set<Component> wcc:wccs) {
			int s = 0;
			for (Component component:wcc) {
				s = s + componentSizes.get(component);
			}
			maxSize = Math.max(maxSize,s);
		}
		LOGGER.info("component graph wccs max size (vertices): " + maxSize);
		
		System.out.println();
		
	}


}
