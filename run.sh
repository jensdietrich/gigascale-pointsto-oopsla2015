#! /usr/bin/env bash

#-------------------------------------------------------------------------#
#                                run.sh                                   #
#                                                                         #
# Author: Nic H.                                                          #
# Date: 2015-Jun-01                                                       #
#                                                                         #
# Run all performance experiments for the Gigascale 'points-to' analysis  #
# engine. Output will be recorded to a CSV file and printed at the end of #
# execution.                                                              #
#                                                                         #
#   Commands:                                                             #
#     -h        Display this help message                                 #
#     -i <dir>  Read input problems from <dir> (Use multiple times)       #
#     -o <file> output the result of the analysis to <file>               #
#   Options:                                                              #
#     -w        Exclude the worklist implementation                       #
#     -d        Exclude the difference-propagation implementation         #
#     -l        Exclude the logicblox implementation                      #
#     -t        Exclude the transitive-closure implementation             #
#     -b        Exclude bridge-finder comparison                          #
#     -s        Exclude benchmark statistical analysis                    #
#     -r        Exclude refinement/precision experimentation              #
#-------------------------------------------------------------------------#

DATASETS=""
OUT="ALL.csv" #`basename $0 | sed 's/.sh$/.csv/'`
OMNI_LOG="LOG.log"
MYDIR=`dirname $0`
TMP="TMP_FILE_93846547"
LOG4J="log4j.properties"
JAVA_OPTS="-d64 -Xmx32m -Xmx5g"

export MEMUSG=`readlink -f $MYDIR/memusg`

#
# Print the header of this file as usage information
# Where the header is defined as '#' at the beginning and end of a line
#
function usage(){
    grep "^#.*#$" $0
}

#
# Echo the args inside a pretty box
#
function box(){
    echo "$@" | sed 's/^\(.*\)$/  \1  \n| \1 |\n  \1  /' | sed '/^ /s/./=/g' | tee -a $OMNI_LOG
}

#
# Append the contents of $1 $2 to $3, then delete $1 and $2
#
function append_time_mem(){
    if [ -e $1 -a -e $2 ]; then
        MEM_MB=$(python -c "print \"%d\" % (int(`cat $2`) / 1024)")
        echo -n ",`cat $1`,$MEM_MB" >> $3
        rm $1 $2
    else
        echo "Time and memory data missing" | tee -a $OMNI_LOG
        echo -n ",-1,-1" >> $3
    fi
}

#
# Universal template for the java implementations
#    java_experiment <problem> <experiment> [time-outout-file] [memory-output-file] [log-time-grep]
#
function java_experiment(){
    cp $MYDIR/java/$LOG4J $MYDIR/ # We use Log4j, copy the config
    local LOGFI=`grep "output=" $MYDIR/$LOG4J | head -n 1 | sed 's/.*=//'` # Determine where the Log4j logger will be placing files
    local CPATH="$MYDIR/java/build/classes/core:$MYDIR/java/build/classes/benchmarks:$MYDIR/java/build/classes/tests:$MYDIR/java/lib/*"
    # When we intend to record time/memory, we must provide mem/time arguments
    if [ "x$3" != "x" ]; then
        $MEMUSG $4 /usr/bin/time -f "%e" -o $3 java $JAVA_OPTS -cp "$CPATH" nz.ac.massey.gp2.pointsto.benchmarks.$2 $1/
        local JAVA_SUCCESS="$?"
        while [ ! -e $4 ]; do
            sleep 1 # wait for the memory log to be written (it happens in a different process)
        done
        if [ $JAVA_SUCCESS == 0 ]; then
            # When a 5th argument is given, it means use a line in the log for accurate time data
            if [ "x$5" != "x" ]; then
                grep "$5" $LOGFI | sed 's/^.*took /print /' | sed 's/ms.*$/\/1000.0/' | python > $3
            fi
        else
            rm -rf $3 $4
        fi
    else # otherwise we are ignoring mem/time info
        java $JAVA_OPTS -cp "$CPATH" nz.ac.massey.gp2.pointsto.benchmarks.$2 $1/
    fi
    cat $LOGFI >> $OMNI_LOG # add this executions logs to the main log file
    rm $MYDIR/$LOG4J
    rm $LOGFI
}

#
# Run the worklist experiment on $1, appending the time,mem to $2
#
function work_list(){
    box WL $1
    java_experiment $1 "MeasureAll -wl" "${TMP}T" "${TMP}M" "Computed points-to sets, this took"
    append_time_mem "${TMP}T" "${TMP}M" $2
}

#
# Run the difference-propagation experiment on $1, appending the time,mem to $2
#
function diff_prop(){
    box DP $1
    java_experiment $1 "MeasureAll -dp" "${TMP}T" "${TMP}M" "Generated index, this took"
    append_time_mem "${TMP}T" "${TMP}M" $2
}

#
# Run the logicblox experiment on $1, appending the time,mem to $2
#
function logic_blox(){
    box LB $1
    local LB_T=`readlink -f ${TMP}T`
    local LB_M=`readlink -f ${TMP}M`
    local LB_IN=`readlink -f $1`
    (cd lb; ./lb.sh $LB_IN vpt.logic $LB_T $LB_M) | tee -a $OMNI_LOG
    append_time_mem "${TMP}T" "${TMP}M" $2
}

#
# Run the transitive-closure experiment on $1, appending the time,mem to $2
#
function trans_close(){
    box TC $1
    java_experiment $1 "MeasureAll -bu" "${TMP}T" "${TMP}M" "Overall reachability index generation took"
    append_time_mem "${TMP}T" "${TMP}M" $2
}

#
# Experiments to compare refinement strategies
#
function refinement_exp(){
    box Refinement $1
    java_experiment $1 "MeasureAll -td 0 -bu -td 20 -fp" 2>&1 | tee $TMP
    local PRECISIONS=`grep "MeasureFalse.*precision" $TMP | sed 's/.*: //' | paste -d"," - - - | cut -d"," -f 1,3`
    local TD_ITER=`grep  "TopDownRefiner" $TMP | sed 's/.*iteration //' | sed 's/,.*//' | sort -n | tail -n 1`
    local BU_ITER=`grep  "BottomUpRefiner" $TMP | sed 's/.*iteration //' | sed 's/,.*//' | sort -n | tail -n 1`
    echo -n ",$PRECISIONS,$BU_ITER,$TD_ITER" >> $2
    rm $TMP
}

#
# Experiments to print problem statistics
#
function statistics_exp(){
    box Statistics $1
    java_experiment $1 "MeasureAll -gs -bu -ss" 2>&1 | tee $TMP
    local VER=`grep "|V|" $TMP | sed 's/.*://'`
    local EDG=`grep "|E|" $TMP | sed 's/.*://'`
    local VPT=`grep "Total points-to set size" $TMP | sed 's/.*: //'`
    local AVG=`python -c "print \"%0.3f\" % (float($VPT) / $VER)"`
    local MAX=`grep "Max points-to set size" $TMP | sed 's/.*: //'`
    local LPF=`grep "Load Bound" $TMP | sed 's/.*: //'`
    local SPF=`grep "Store Bound" $TMP | sed 's/.*: //'`
    echo -n ",$VER,$EDG,$VPT,$AVG,$MAX,$LPF,$SPF" >> $2
    rm $TMP
}

#
# Experiments to count the various bridges found by different analyses
#
function bridge_count(){
    box Bridge-counts $1
    java_experiment $1 "MeasureAll -os -bu" 2>&1 | tee $TMP
    local AP=`grep "All-pairs:" $TMP | sed 's/.*All-pairs: //'`
    local SF=`grep "Same-field:" $TMP | sed 's/.*Same-field: //'`
    local DY=`grep  "BottomUpRefiner.*iteration" $TMP | sed 's/.*: //' | sort -n | tail -n 1`
    local FB=`grep  "BottomUpRefiner.*iteration" $TMP | sed 's/.*: //' | sort -n | head -n 1`
    echo -n ",$AP,$SF,$DY,$(( $DY -  $FB))" >> $2
    rm $TMP
}

#
# BEGIN MAIN SCRIPT EXECUTION
#   Confirm the script is being run from the current directory
#   Parse the command line options
#
if [ $(readlink -f `dirname $0`) != `pwd` ]; then
    echo "Only execute this script from the current directory!" >&2
    usage
    exit 1
fi
while getopts "hi:o:wdltrsb" opt; do
    case $opt in
        h)
            usage
            exit 0
            ;;
        i)
            DATASETS="$DATASETS $OPTARG"
            USER_DEFINED_DATASETS="True"
            ;;
        o)
            OUT="$OPTARG"
            ;;
        w)
            echo "Excluding worklist"
            EX_WL="True"
            ;;
        d)
            echo "Excluding difference-propagation"
            EX_DP="True"
            ;;
        l)
            echo "Excluding logicblox"
            EX_LB="True"
            ;;
        t)
            echo "Excluding transitive-closure"
            EX_TC="True"
            ;;
        r)
            echo "Excluding refinement experimentation"
            EX_RF="True"
            ;;
        s)
            echo "Excluding statistical experimentation"
            EX_ST="True"
            ;;
        b)
            echo "Excluding bridge-finder comparison"
            EX_BR="True"
            ;;
        \?)
            usage
            exit 1
            ;;
    esac
done
[ -n "$DATASETS" ] || DATASETS="datasets/"
box Execution begins `date`

# Compile the java framework if necessary
(cd $MYDIR/java; ant compile) 2>&1 | tee -a $OMNI_LOG

# Write the CSV header
echo -n "benchmark" > $OUT
[ -n "$EX_TC" ] || echo -n ",TC-time,TC-mem" >> $OUT
[ -n "$EX_LB" ] || echo -n ",LB-time,LB-mem" >> $OUT
[ -n "$EX_DP" ] || echo -n ",DP-time,DP-mem" >> $OUT
[ -n "$EX_WL" ] || echo -n ",WL-time,WL-mem" >> $OUT
[ -n "$EX_ST" ] || echo -n ",v,e,vpt,avg,max,load/f,store/f" >> $OUT
[ -n "$EX_BR" ] || echo -n ",bridge-all,bridge-same,bridge-dyck,bridge-true" >> $OUT
[ -n "$EX_RF" ] || echo -n ",precision-init,precision-final,refinement-bu,refinement-td" >> $OUT
echo >> $OUT

# Run each problem through all its algorithms/analyses in sequence
PROBLEMS=`find $DATASETS -name Assign.csv | xargs dirname 2>/dev/null | sort -ur `
for PROB in $PROBLEMS; do
    if [ -z "$USER_DEFINED_DATASETS" ] && [[ $PROB =~ /\. ]]; then
            continue # Skip problems in hidden directories unless the user has forced their evaluation with -i
    fi
    echo -n $(basename `readlink -f $PROB`) >> $OUT
    [ -n "$EX_TC" ] || trans_close    $PROB $OUT
    [ -n "$EX_LB" ] || logic_blox     $PROB $OUT
    [ -n "$EX_DP" ] || diff_prop      $PROB $OUT
    [ -n "$EX_WL" ] || work_list      $PROB $OUT
    [ -n "$EX_ST" ] || statistics_exp $PROB $OUT
    [ -n "$EX_BR" ] || bridge_count   $PROB $OUT
    [ -n "$EX_RF" ] || refinement_exp $PROB $OUT
    echo >> $OUT
done

box Execution completed `date`

# Pretty print the output
column -ts"," $OUT
