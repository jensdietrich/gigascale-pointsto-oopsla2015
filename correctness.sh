#! /usr/bin/env bash

#-------------------------------------------------------------------------#
#                             correctness.sh                              #
#                                                                         #
# Author: Nic H.                                                          #
# Date: 2015-Jul-08                                                       #
#                                                                         #
# Checks for consistency of a given implementation against the oracle (or #
# if the oracle does not exist, against a nominated alternative).         #
#                                                                         #
# Logicblox output is from the -print option, the output requires some    #
# formatting to keep it consistent with our oracles                       #
#                                                                         #
# Usage: correctness.sh                                                   #
#   correctness.sh [options] <implementation> <dataset>                   #
#                                                                         #
# Implementations:                                                        #
#   wl, dp, lb, tc                                                        #
#                                                                         #
# Options:                                                                #
#   -h       Display this help message                                    #
#   -g       Generate relation files without performing the diff          #
#   -l FILE  Use FILE instead of the default logiql file (lb/vpt.logic)   #
#            for the Logicblox implementation                             #
#   -o IMP   Treat the implementation IMP as providing the oracle         #
#                                                                         #
#-------------------------------------------------------------------------#

IWL="wl"
IDP="dp"
ILB="lb"
ITC="tc"
MYDIR=`dirname $0`
LB_LOGIC="vpt.logic"
export MEMUSG=`readlink -f $MYDIR/memusg`

function usage(){
    grep "^#.*#$" $0
}

function source_oracle(){
    local OFI=""
    if [ "X$1" == "X" ]; then
        OFI="$2/VarPointsTo.csv"
    else
        OFI=$(source_exec "$1" "$2")
        [ -n "$OFI" ] || exit 1
    fi
    if [ -f $OFI ] && [ -e $OFI ]; then
        echo "The oracle is located at: $OFI" >&2
        echo $OFI
    else
        echo "Cannot find oracle file: $OFI" >&2
    fi
}

function source_exec(){
    local RET=$(tempfile -p ${1}- -s .vpt)
    if [ $1 == $ILB ]; then
        # Dump the logicblox relations to the $RET file
        echo "Generating LB relations from logic file: $LB_LOGIC" >&2
        local LB_TIME=$(tempfile)
        local LB_MEMO=$(tempfile)
        local LB_DATA=$(readlink -f $2)
        (cd lb; ./lb.sh $LB_DATA $LB_LOGIC $LB_TIME $LB_MEMO) 1>&2
        [ $? == 0 ] || exit 1
        $LOGICBLOX_HOME/bin/bloxbatch -db $MYDIR/lb/TMP_DB -print VarPointsTo | egrep "^ *\[[0-9]+\].*, \[[0-9]+\]" | sed 's/"//g' | sed 's/^ *\[[0-9]*\]/"/' | sed 's/, \[[0-9]*\]/","/' | sed 's/$/"/' | sed 's/"\(.*\)","\(.*\)"/"\2","\1"/' | sort -u > $RET
        rm $LB_TIME $LB_MEMO
    elif [ $1 == $IWL ] || [ $1 == $IDP ] || [ $1 == $ITC ]; then
        # Dump the relations according to a java implementation
        local IMPL="$1"
        if [ $1 == $ITC ]; then
            IMPL="bu"
        fi
        local CPATH="$MYDIR/java/build/classes/core:$MYDIR/java/build/classes/benchmarks:$MYDIR/java/build/classes/tests:$MYDIR/java/lib/*"
        cp java/log4j.properties log4j.properties
        java $JAVA_OPTS -cp "$CPATH" nz.ac.massey.gp2.pointsto.benchmarks.MeasureAll -$IMPL -ss -dump $2/ | sort -u > $RET
        rm log4j.properties most_recent_execution.log
    else
        rm $RET
        echo "Invalid implementation: $1" >&2
    fi
    echo "$1 relations dumped to: $RET" >&2
    echo $RET
}

#
# BEGIN SCRIPT
#  check we are running from the current directory
#
if [ $(readlink -f `dirname $0`) != `pwd` ]; then
    echo "Only execute this script from the current directory!" >&2
    usage
    exit 1
fi
ORAC=""
DIFFING="true"
while getopts "hgo:l:" opt; do
    case $opt in
        h)
            usage
            exit 0
            ;;
        g)
            DIFFING="false"
            ;;
        o)
            ORAC=$OPTARG
            ;;
        l)
            LB_LOGIC=$(readlink -f $OPTARG)
            ;;
        \?)
            usage
            exit 1
            ;;
    esac
done
shift $(($OPTIND -1))

#Make sure the correct number of arguments are given
if [ $# != 2 ]; then
    usage
    exit 2
fi

ORAC_FI=$(source_oracle "$ORAC" "$2")
[ -n "$ORAC_FI" ] || exit 3
EXEC_FI=$(source_exec $1 $2)
[ -n "$EXEC_FI" ] || exit 4

if [ $DIFFING == "true" ]; then
    echo
    echo "diff $ORAC_FI $EXEC_FI"
    echo "====================================="
    diff $ORAC_FI $EXEC_FI
    echo "====================================="
    echo "If nothing was printed in the diff, the evaluation is correct"
fi
