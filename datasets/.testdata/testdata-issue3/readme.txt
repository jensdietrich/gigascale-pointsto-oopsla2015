This test case describes an issue we discovered with the difference propagation algorithm presented in 
Sridharan, Fink - The Complexity of Andersen’s Analysis in Practice.
and implemented in nz.ac.massey.gp2.diffprop.DiffPropPointsTo (DiffProp for short)

we compare this with nz.ac.massey.gp2.pointsto.TCReachability using nz.ac.massey.gp2.pointsto.refinement.BottomUpRefiner (TC for short).

The scenario has a unique feature: a self-load edge - this turns out to be important. Note that we have extracted this example from xalan, by first
using the utility test.nz.ac.massey.gp2.pointsto.testutils.FNWithDiffPropPointsToTestCaseExtractor, followed by some manual refinement 
to make the test as simple and comprehensible as possible. 
 
v8 should point to hobj3, and TC computes this. There are two bridge edges created in two iterations, for details see 
graph-tc.png in this folder. 

On the other hand, DiffProp does not compute this result, i.e., it yields a false negative. The auxiliary vertices and edges created by 
DiffProp are shown in graph-diffprop.png. The algorithm performs the following steps: 

(1) a vertex hobj2.f is created that acts as a "bridge" from v4 to v7 (similar to TC)
(2) when v7 is processed and removed from the worklist, an edge is generated to represent the self-load edge: hobj1.f->v7 , and the pointsto set 
of hobj1.f ({hobj3}) is propagated to v7 and added to pt_delta(v7) -  this is the partial pointsto set of v7 "yet to be propagated". And indeed, v7 is added back to the worklist
because it has become dirty (has information that must be propagated) [lines 16-20 of the algorithm]
(3) however, at the end of the iteration immediately after the loads are processed, the pt_delta(v7) is copied to the final points-to set and then flashed [lines 21-22 of the algorithm]
(4) therefore, when v7 is processed again (as it was added back the worklist), there is nothing to propagate (pt_delta(v7) is now empty), and therefore hobj3 is never propagated to v8

Note that the self-load is essential for this behaviour !  
