#! /usr/bin/env bash

#-------------------------------------------------------------------------#
#                               checksum.sh                               #
#                                                                         #
# Author: Nic H.                                                          #
# Date: 2015-Mar-13                                                       #
#-------------------------------------------------------------------------#

if [ $(readlink -f .) != $(readlink -f $(dirname $0)) ]; then
    echo "Please run checksum.sh from its own directory (i.e. ./checksum.sh)"
    exit 1
fi


find . -iname "*.csv" | sort | xargs sha256sum | diff - checksums
echo "Checksum complete.  If nothing is printed you're good."
